#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
from XAMPPplotting.Utils import CreateRatioHistos, setupBaseParser

from ClusterSubmission.Utils import CreateDirectory, id_generator
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.FileStructureHandler import GetStructure
import ROOT


def drawComparison(Options, analysis, region, ff_vars=[], fake_defs=[], bonus_str=""):
    HistToDraw = []
    ratios = []

    Colors = [
        0,
        ROOT.TColor.GetColor(102, 252, 245),
        ROOT.TColor.GetColor(111, 224, 5),
        ROOT.TColor.GetColor(162, 145, 94),
        ROOT.TColor.GetColor(225, 230, 22),
        ROOT.TColor.GetColor(210, 104, 23),
        ROOT.TColor.GetColor(245, 82, 253),
        ROOT.TColor.GetColor(20, 54, 208),
    ]

    DefaultSet = None
    for i, var in enumerate(ff_vars):
        FullHistoSet = CreateHistoSets(Options, analysis, region, var, UseData=False)
        if not FullHistoSet or not FullHistoSet[0].CheckHistoSet():
            continue

        if not DefaultSet: DefaultSet = FullHistoSet[0].GetSummedBackground()
        else:
            HistToDraw += [FullHistoSet[0].GetSummedBackground()]
            HistToDraw[-1].SetLineColor(Colors[i])
            HistToDraw[-1].SetFillColor(Colors[i])
            HistToDraw[-1].SetTitle(fake_defs[i])
    PosY2 = 0.9
    PosY1 = 0.6
    if Options.doLogY: bonus_str += "_LogY"
    canvasName = "FakeBreakDown_%s_%s_%s%s" % (analysis, region, ff_vars[0], bonus_str)

    pu = PlotUtils(status=Options.label, size=24, lumi=Options.lumi)

    pu.Prepare2PadCanvas(canvasName, 800, 600, Options.quadCanvas)
    pu.GetTopPad().cd()
    if Options.doLogY: pu.GetTopPad().SetLogy()

    pu.CreateLegend(Options.LegendXCoords[0], Options.LegendYCoords[0], Options.LegendXCoords[1], Options.LegendYCoords[1])

    ### Draw the histograms
    ymin, ymax = pu.GetFancyAxisRanges(HistToDraw + [DefaultSet], Options)
    pu.drawStyling(DefaultSet, ymin, ymax, RemoveLabel=not Options.noRatio, TopPad=not Options.noRatio)

    stack = Stack(HistToDraw)
    stack.Draw("histSAME")
    for H in HistToDraw:
        pu.AddToLegend(H)
    pu.drawSumBg(DefaultSet)
    pu.DrawLegend()

    labelY = 0.83
    if Options.noRatio: labelY = 0.88
    pu.DrawPlotLabels(0.195, labelY, region, analysis, Options.noATLAS)

    ### Draw the ratio to WZ ZZ
    pu.GetTopPad().RedrawAxis()
    pu.GetBottomPad().cd()
    pu.GetBottomPad().SetGridy()

    ratios = [Stack(CreateRatioHistos(HistToDraw, DefaultSet))]

    pu.drawRatioStyling(HistToDraw[0], 0, 1.2, "Contribution")

    for r in ratios:
        r.Draw("histSAME")
    pu.saveHisto("%s/%s" % (Options.outputDir, canvasName), Options.OutFileType)


def make_ff_vars(trig_def="Base", prong="Incl", lep_kind="Incl", observable=""):
    origs = [
        "Incl",
        "Real",
        "Quark",
        #"HF", "LF",
        "Gluon",
        #"Elec", "Muon",
        "Leptonic",
        "PU"
    ]
    return [
        "TauFF_def%s_orig%s_etaIncl_prong%s_rangeIncl_lepton%s_var%s" % (trig_def, kind, prong, lep_kind, observable) for kind in origs
    ], origs


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='This script produces MC only histograms from tree files. For more help type \"python MCPlots.py -h\"',
        prog='TauFF_Contributions',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.set_defaults(outputDir="TauFF_Composition")
    PlottingOptions = parser.parse_args()
    CreateDirectory(PlottingOptions.outputDir, False)

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = GetStructure(PlottingOptions)

    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)

    Observables = [
        "TauPt",
        "TauAbsEta",
        "TauWidth",
        #"TauMetDPhi",
        #"TauTrkJetWidth",
        #"TauTrkJetWidthTauVtx",
        #"TauZ0SinTheta",
        "TauD0sig",
        "TauD0",
    ]
    prongs = ["Incl", "1P", "3P"]
    light_lep = ["Incl", "Real"]
    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            if not "PreSel" in region: continue
            for trig in ["Base", "LepTrigger", "TauTrigger"]:
                for lep in light_lep:
                    for var in Observables:
                        for p in prongs:
                            ff_vars, defs = make_ff_vars(prong=p, observable=var, lep_kind=lep, trig_def=trig)
                            drawComparison(PlottingOptions, ana, region, ff_vars=ff_vars, fake_defs=defs, bonus_str="")
