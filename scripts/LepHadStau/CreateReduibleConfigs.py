#!/env/python
from ClusterSubmission.Utils import ResolvePath, WriteList, CreateDirectory
import os
List_DIR = ResolvePath("XAMPPplotting/InputConf/LepHadStau/")

BkgFiles = os.listdir(List_DIR + "Background/")
for Bkg in BkgFiles:
    if ".conf" not in Bkg: continue
    New_InCfg = ["Import XAMPPplotting/InputConf/LepHadStau/Background/%s" % (Bkg), "SampleName DataDriven", "define useDataDrivenEstimate"]
    WriteList(New_InCfg, "%s/Reducible/DD_%s" % (List_DIR, Bkg))

DataFiles = os.listdir("%s/Data/" % (List_DIR))
for Smp in DataFiles:
    New_InCfg = ["Import XAMPPplotting/InputConf/LepHadStau/Data/%s" % (Smp), "SampleName DataDriven", "define useDataDrivenEstimate"]
    WriteList(New_InCfg, "%s/Reducible/DD_%s" % (List_DIR, Smp))
