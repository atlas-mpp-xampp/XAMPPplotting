import os, sys, math
import argparse
from pprint import pprint


def CreateArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--ResumminoIn", help="Directory where the input files from Resummino are located")
    parser.add_argument("--OutFile", help="The outfile", default="xSections.txt")
    parser.add_argument('--JobOptionsDir', help="Directory of the files containing the 1-line JO", required=True)
    parser.add_argument("--texFile", help="The outfile", default="tables.tex")
    parser.add_argument("--EvGenDir", help="Where is the output of the EVGEN files", default="/ptmp/mpp/junggjo9/MC16_Simulation/EVNT/")
    return parser


def MassFromFileName(Name):
    Mass = Name.split("_")[3].replace("p", ".")
    Angle = Name.split("_")[-1].replace(".txt", "")
    return float(Mass), int(Angle)


def ReadInResumminoFile(Location):
    FileContent = {}
    Read = open(Location, "r")
    print "Open resummino file ", Location
    for Line in Read:
        Line = Line.strip()
        if "nan" in Line or Line.startswith("#"): continue
        print Line
        FinalState = int(Line.split()[0])
        Central = float(Line.split()[1])
        CalcError = float(Line.split()[2])
        if Central == 0.: FileContent[FinalState] = (0., 1.)
        else:
            Error = (CalcError / Central)**2
            PDFUncert = [float(E) for E in Line.split("|")[-1].split()]
            for i in range(0, len(PDFUncert), 2):
                if i + 1 < len(PDFUncert): Error += max(PDFUncert[i]**2, PDFUncert[i + 1]**2)
                else: Error *= PDFUncert[i]**2

            Error = math.sqrt(Error)
            #Cross-ections are saved in fb -> divide by 1.e3
            FileContent[FinalState] = (Central / 1.e3, Error)

    return FileContent


def GetResumminoDictionary(Input):
    Dict = {}
    if os.path.isdir(Input):
        for F in os.listdir(Input):
            if not F.endswith(".txt"): continue
            Mass, Angle = MassFromFileName(F)
            try:
                Dict[(Mass, Angle)].update(ReadInResumminoFile("%s/%s" % (Input, F)))
            except:
                Dict[(Mass, Angle)] = ReadInResumminoFile("%s/%s" % (Input, F))
    return Dict


def GetDSIDs(Options):
    DSIDs = {}
    for File in sorted(os.listdir(Options.JobOptionsDir)):
        if not File.endswith(".py"):
            continue
        ##All JOS have the form
        #MC15.XXXXXX.<Gen>_<FineTune>_<Model>_<NLSP>
        DS = int(File[5:11])
        try:
            NLSPMass = float(File.split("_")[3].replace("p", "."))
        except:
            continue
        Angle = int(File.split("_")[-1].replace(".py", ""))

        DSIDs[DS] = (NLSPMass, Angle, File[5:-3])
    return DSIDs


def WriteXsectionFile(File, Samples, DB):
    OutFile = open(File, "w")
    for DSID in sorted(Samples.iterkeys()):
        Model = Samples[DSID]
        Mass = Model[0]
        Angle = Model[1]
        Description = Model[2]
        if (Mass, Angle) not in DB.iterkeys():
            print "WARNING: There was some strange noise out there", Description
            continue
        OutFile.write("### %s\n" % (Description))
        for FinalState, Values in DB[(Mass, Angle)].iteritems():
            xs = Values[0]
            err = Values[1]
            Line = "%i   %i   %.10f   1.  1.  %.10f \n" % (DSID, FinalState, xs, err)
            OutFile.write(Line)
    OutFile.close()


def writeLatexTables(DB, texfile):
    ResortedDB = {}
    for mass_angle, values in DB.iteritems():
        if not mass_angle[0] in ResortedDB.iterkeys(): ResortedDB[mass_angle[0]] = {}

        ResortedDB[mass_angle[0]][mass_angle[1]] = {}
        for fs, xSecErr in values.iteritems():
            ResortedDB[mass_angle[0]][mass_angle[1]][fs] = (xSecErr[0] * 1.e3, xSecErr[0] * 1.e3 * xSecErr[1])

    Tex = ""
    for mass in ResortedDB.iterkeys():
        Header = """
\\begin{table}
    \\begin{tabular}{c | r@{$\\pm$}l r@{$\\pm$}l r@{$\\pm$}l| r@{$\\pm$}l}
      $\\theta_{\\tilde{\\tau}}$ & 
      \\multicolumn{2}{c}{$\\sigma_{\\tilde{\\tau}_{1}\\tilde{\\tau}_{1}}$ [fb]} & 
      \\multicolumn{2}{c}{$\\sigma_{\\tilde{\\tau}_{2}\\tilde{\\tau}_{2}}$ [fb]} & 
      \\multicolumn{2}{c}{$\\sigma_{\\tilde{\\tau}_{1}\\tilde{\\tau}_{2}}$ [fb]} & 
      \\multicolumn{2}{c}{$\\sigma_{\\text{total}}$ [fb]}\\\ \\hline
    """
        Footer = """
    \end{tabular}
    \caption{$\\tilde{\\tau}$-production cross-section split into the three production modes for a $\\tilde{\\tau}$-mass of %.f~GeV at NLL-NLO calculated by the Resummino package}
\end{table} \n\n\n
    """ % (mass)
        xSecValues = ResortedDB[mass]
        for angle in sorted(xSecValues.iterkeys()):
            if angle > 0 and angle % 50 == 0: Tex += Footer
            if angle % 50 == 0: Tex += Header
            prod_modes = xSecValues[angle]
            Tot_xSec = sum(p[0] for p in prod_modes.itervalues())
            Tot_xSecErr = math.sqrt(sum(p[1]**2 for p in prod_modes.itervalues()))
            Tex += " %d  &  %.3f  & %.3f &  %.3f  & %.3f &  %.3f  & %.3f &  %.3f  & %.3f \\\\ \n" % (
                angle, prod_modes[206][0], prod_modes[206][1], prod_modes[207][0], prod_modes[207][1], prod_modes[208][0],
                prod_modes[208][1], Tot_xSec, Tot_xSecErr)
        Tex += Footer

    myfile = open(texfile, 'w')
    myfile.write(Tex)
    myfile.close()


def extractFilter(EvGenDir):
    #### Subdirectories are named like <DSID>. phys_process
    Filters = {}
    if not os.path.isdir(EvGenDir):
        print "ERROR: %s is not a directory" % (EvGenDir)
        return None
    for Sample in sorted(os.listdir(EvGenDir)):
        DSID = int(Sample.split(".")[0])
        LogFiles = ["%s/%s/%s" % (EvGenDir, Sample, L) for L in os.listdir("%s/%s" % (EvGenDir, Sample)) if L.endswith(".log")]
        N = 0
        AvgEffi = 0.
        for Log in LogFiles:
            GoodFile = False
            FileEffi = 0.
            with open(Log) as L:
                for i, line in enumerate(reversed(L.readlines())):
                    ### Look in the first few lines whether the run was good or not
                    if not GoodFile and i < 10:
                        if "an unknown exception occurred" in line: break
                        if "successful run" in line: GoodFile = True
                    elif not GoodFile: break
                    if "MetaData: GenFiltEff = " in line:
                        FileEffi = float(line.split()[-1])
                        break
            if GoodFile:
                N += 1
                AvgEffi += FileEffi

        AvgEffi = AvgEffi / N if N > 0 else 0.
        Filters[DSID] = AvgEffi
        print "Found %f filter efficiency for %s" % (AvgEffi, Sample)

    return Filters


if __name__ == '__main__':
    Options = CreateArgumentParser().parse_args()
    efficiencies = extractFilter(Options.EvGenDir)
    exit(1)
    xSecDB = GetResumminoDictionary(Options.ResumminoIn)
    writeLatexTables(xSecDB, Options.texFile)
    DSIDs = GetDSIDs(Options)
    WriteXsectionFile(Options.OutFile, DSIDs, xSecDB)
