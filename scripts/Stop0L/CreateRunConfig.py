import os
import sys
import commands
from pprint import pprint

# This scripts creates Run Config files from a list of systematics and selections
#
# Usage: python scripts/CreateRunConfigs.py

listOfSystematics = [
    "Nominal",
    "EG_RESOLUTION_ALL__1down",
    "EG_RESOLUTION_ALL__1up",
    "EG_SCALE_ALL__1down",
    "EG_SCALE_ALL__1up",
    "JET_GroupedNP_1__1down",
    "JET_GroupedNP_1__1up",
    "JET_GroupedNP_2__1down",
    "JET_GroupedNP_2__1up",
    "JET_GroupedNP_3__1down",
    "JET_GroupedNP_3__1up",
    "JET_JER_SINGLE_NP__1up",
    "MET_SoftTrk_ResoPara",
    "MET_SoftTrk_ResoPerp",
    "MET_SoftTrk_ScaleDown",
    "MET_SoftTrk_ScaleUp",
    "MUONS_ID__1down",
    "MUONS_ID__1up",
    "MUONS_MS__1down",
    "MUONS_MS__1up",
    "JET_RelativeNonClosure_AFII__1down",
    "JET_RelativeNonClosure_AFII__1up",
    #                 "TAUS_TRUEHADTAU_SME_TES_TOTAL__1down", "TAUS_TRUEHADTAU_SME_TES_TOTAL__1up",
]

RegionsAndSelections = {
    "2LCR": ["Stop0L_2LCR_SF_3J1B", "Stop0L_2LCR_SF_4J1B", "Stop0L_2LCR_OF_3J1B", "Stop0L_2LCR_OF_4J1B"],
    "1LCR": [
        "Stop0L_1LCR_Wjet", "Stop0L_1LCR_3J1B", "Stop0L_1LCR_4J1B", "Stop0L_1LCR_5J1B", "Stop0L_1LCR_3J2B", "Stop0L_1LCR_4J2B",
        "Stop0L_1LCR_5J2B"
    ]
}

analysis = "Stop0L"

mainRegion = "1LCR"

for sys in listOfSystematics:
    fname = analysis + '_' + mainRegion + '_' + sys + '.conf'
    with open(fname, "w") as fout:
        fout.write("# Analysis setup for %s_%s\n\n" % (analysis, mainRegion))
        fout.write("Analysis %s \n\n" % (analysis))
        fout.write("ProbeMatch %s MediumMuons\n\n" % (sys))
        for selection in RegionsAndSelections[mainRegion]:
            fout.write("ChargeProducts %s\n" % (selection))
        fout.write("\n#WriteCutFlow \n\n")
        fout.write("#SysVars dRUp dRDown \n")
