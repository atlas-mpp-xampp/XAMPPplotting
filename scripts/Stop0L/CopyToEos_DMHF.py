#!/usr/bin/env python

import os
import sys
import commands
import mimetypes

if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description="Script for creating MC input configs",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', dest='indir', help='Folder with files', required=True, nargs='+')
    parser.add_argument('-o',
                        '--output',
                        dest='outdir',
                        help='Output directory to put file list(s) into',
                        default='/eos/atlas/user/n/nkoehler/DMHF/HistFitter/170312_Base_2_4_28_ST_08_54/')
    options = parser.parse_args()

    # for permissions go to lxplus and do: eos attr -r set user.acl='egroup:atlas-phys-susy-directstop-zeroLep:rx,g:zp:!d' eos/atlas/user/n/nkoehler/Stop0L/xyz

    destination = 'root://eosatlas.cern.ch//' + options.outdir + '/'
    print 'EOS destination to which files are copied: %s' % destination

    files = os.listdir(options.indir[0])

    if len(files) == 0:
        print 'No files found, exiting...'
        sys.exit(1)

    TMP_PATH = '/ptmp/mpp/niko/Cluster/Batch_tmp/2017-02-06/test/'
    os.system("mkdir -p %s" % TMP_PATH)

    for ifile in files:
        if os.path.exists("%s/%s" % (TMP_PATH, ifile)): os.system("rm %s/%s" % (TMP_PATH, ifile))
        haddStr = "hadd %s/%s" % (TMP_PATH, ifile)
        for inputdir in options.indir:
            if os.path.exists("%s/%s" % (inputdir, ifile)):
                haddStr += " %s/%s" % (inputdir, ifile)
        if haddStr == "hadd %s/%s" % (TMP_PATH, ifile):
            print 'no files to hadd, exiting...'
            sys.exit(1)
        print haddStr

        os.system(haddStr)

        os.system("xrdcp -f %s/%s %s/%s " % (TMP_PATH, ifile, destination, ifile))

        os.system("rm %s/%s" % (TMP_PATH, ifile))
