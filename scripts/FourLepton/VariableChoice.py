#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
from XAMPPplotting.Utils import setupBaseParser, ExtractMasses
from ClusterSubmission.Utils import IsROOTFile, CreateDirectory
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.SignificancePlots import CreateSiggiHistos
from XAMPPplotting.FileStructureHandler import GetStructure
from XAMPPplotting.PlotLabels import GetLabelKey

FourLep_RegionLabels = {
    "4LnoZ": "4L0T, Z-veto",
    "3L1TnoZ": "3L1T, Z-veto",
    "2L2TnoZ": "2L2T, Z-veto",
    "4LZZ": "4L0T, ZZ selection",
    "4LZZ_bveto": "4L0T, ZZ selection, b-veto",
}


def modelString(smp_name):

    model_key = GetLabelKey(smp_name.split("_"))
    nlsp, lsp = ExtractMasses(smp_name)
    rpv_coupling = ""
    if smp_name.find("LLEi33") != -1: rpv_coupling = "i33"
    elif smp_name.find("LLE12k") != -1: rpv_coupling = "12k"

    if model_key == "Wino_LLE":
        return "#tilde{W}^{#pm}#tilde{W}^{0}, m_{#tilde{W}}=%d GeV, m_{#tilde{#chi}^{0}_{1}}=%d GeV, #lambda_{%s}#neq0" % (nlsp, lsp,
                                                                                                                           rpv_coupling)

    elif model_key == "VVz_LLE":
        return "#tilde{W}^{#pm}#tilde{W}^{0}, m_{#tilde{W}}=%d GeV, m_{#tilde{#chi}^{0}_{1}}=%d GeV, #lambda_{%s}#neq0, #tilde{#chi}^{0}_{2}#rightarrow#tilde{#chi}^{0}_{1}Z" % (
            nlsp, lsp, rpv_coupling)
    elif model_key == "VVh_LLE":
        return "#tilde{W}^{#pm}#tilde{W}^{0}, m_{#tilde{W}}=%d GeV, m_{#tilde{#chi}^{0}_{1}}=%d GeV, #lambda_{%s}#neq0, #tilde{#chi}^{0}_{2}#rightarrow#tilde{#chi}^{0}_{1}h" % (
            nlsp, lsp, rpv_coupling)

    elif model_key == "LV_LLE":
        return "#tilde{l}^{#pm}#tilde{#nu}^{0}, m_{#tilde{l}/#tilde{#nu}}=%d GeV, m_{#tilde{#chi}^{0}_{1}}=%d GeV, #lambda_{%s}#neq0" % (
            nlsp, lsp, rpv_coupling)

    elif model_key == "GG_LLE":
        return "#tilde{g}#tilde{g}, m_{#tilde{g}}=%d GeV, m_{#tilde{#chi}^{0}_{1}}=%d GeV, #lambda_{%s}#neq0" % (nlsp, lsp, rpv_coupling)
    elif model_key == "GGM_Higgsino":
        return "GGM higgsino, m_{#tilde{H}}=%d GeV, BR(#tilde{#chi}^{1}_{0}#rightarrowZ#tilde{G})=%d%%" % (nlsp, lsp)
        return "#tilde{H}_{u}^{#pm/0}#tilde{H}_{d}^{#pm/0}, m_{#tilde{H}}=%d GeV, BR(#tilde{#chi}^{1}_{0}#rightarrowZ#tilde{G})=%d%%" % (
            nlsp, lsp)

    return smp_name


def plotVariables(Options, loaded_histos=[], smp_name="", bonus_str=""):
    if Options.doLogY: bonus_str += "_LogY"
    canvasName = "DiscriminatingVariable_%s_%s%s" % (smp_name, loaded_histos[0].GetRegion(), bonus_str)

    pu = PlotUtils(status=Options.label, size=24, lumi=Options.lumi)

    pu.Prepare1PadCanvas(canvasName, 800, 600, Options.quadCanvas)
    pu.GetCanvas().cd()
    if Options.doLogY: pu.GetCanvas().SetLogy()

    pu.CreateLegend(0.58, 0.75, 0.965, 0.94)
    pu.AddToLegend(loaded_histos)

    ymin = 0
    ymax = loaded_histos[0].GetMaximum() * 1.6
    if ymax < 0.5: return False
    Styling = loaded_histos[0].GetHistogram().Clone("styling")
    Styling.GetXaxis().SetTitle("Discriminating variable")
    max_vis_bin = max([l.GetMinimumBin(1.e-3 * ymax) for l in loaded_histos])
    if max_vis_bin > 0: Styling.GetXaxis().SetRangeUser(0, Styling.GetXaxis().GetBinUpEdge(max_vis_bin))
    pu.drawStyling(Styling, ymin, ymax, RemoveLabel=False, TopPad=False)

    for H in loaded_histos:
        H.Draw("SAMEHIST")

    pu.DrawLegend(NperCol=2)

    region_label = ""
    if len(Options.regionLabel) > 0: region_label = Options.regionLabel
    else:
        try:
            region_label = FourLep_RegionLabels[loaded_histos[0].GetRegion()]
        except:
            region_label = loaded_histos[0].GetRegion()

    pu.DrawPlotLabels(0.195, 0.88, region_label, loaded_histos[0].GetAnalysis(), Options.noATLAS)
    pu.DrawTLatex(0.195, 0.72, modelString(smp_name))
    pu.saveHisto("%s/%s" % (Options.outputDir, canvasName), Options.OutFileType)
    return True


def plotSignificances(Options, backgrounds, signals, smp_name, bonus_str):
    canvasName = "Significance_%s_%s%s" % (smp_name, backgrounds[0].GetRegion(), bonus_str)
    pu = PlotUtils(status=Options.label, size=24, lumi=Options.lumi)

    pu.Prepare1PadCanvas(canvasName, 800, 600, Options.quadCanvas)
    pu.GetCanvas().cd()

    pu.CreateLegend(0.58, 0.75, 0.965, 0.94)
    sigg_histos = []
    for i in range(len(backgrounds)):
        sigg_histos += CreateSiggiHistos([signals[i]],
                                         backgrounds[i],
                                         RelUnc=Options.BkgUncertainty,
                                         ScaleSig=1.,
                                         DoIntegral=False,
                                         UseSUSYSignificance=Options.UseSUSYSignificance)
    pu.AddToLegend(sigg_histos, "PL")

    ymin = 0
    ymax = max([5.] + [s.GetMaximum() for s in sigg_histos]) * 1.52
    if max([s.GetMaximum() for s in sigg_histos]) < 0.5: return False
    Styling = sigg_histos[0].Clone("styling")
    Styling.GetXaxis().SetTitle("Discriminating variable")
    Styling.GetYaxis().SetTitle("Significance Z_{n}")
    pu.drawStyling(Styling, ymin, ymax, RemoveLabel=False, TopPad=False)

    for H in sigg_histos:
        H.Draw("SAMEHIST")

    pu.DrawLegend(NperCol=2)
    region_label = ""
    if len(Options.regionLabel) > 0: region_label = Options.regionLabel
    else:
        try:
            region_label = FourLep_RegionLabels[backgrounds[0].GetRegion()]
        except:
            region_label = backgrounds[0].GetRegion()

    pu.DrawPlotLabels(0.195, 0.88, region_label, backgrounds[0].GetAnalysis(), Options.noATLAS)
    pu.DrawTLatex(0.195, 0.72, modelString(smp_name))

    pu.saveHisto("%s/%s" % (Options.outputDir, canvasName), Options.OutFileType)
    return True


def drawComparison(Options, analysis, region, variables=[], bonus_str=""):
    Histos_Bkg = [CreateHistoSets(Options, analysis, region, var[0], UseData=False)[0].GetSummedBackground() for var in variables]
    Colors = [
        ROOT.TColor.GetColor(111, 224, 5),
        ROOT.TColor.GetColor(162, 145, 94),
        ROOT.TColor.GetColor(210, 104, 23),
        #ROOT.TColor.GetColor(102, 252, 245),
        ROOT.TColor.GetColor(245, 82, 253),
        ROOT.TColor.GetColor(20, 54, 208),
        ROOT.kRed,
    ]
    for i, Bkg in enumerate(Histos_Bkg):
        Bkg.SetTitle(variables[i][1])
        Bkg.SetLineColor(Colors[i])
        Bkg.SetLineWidth(3)
        Bkg.SetMarkerSize(0)
    #plotVariables(Options, Histos_Bkg, "Background", bonus_str)
    ### Now look into the signalDir and plot foreach signal the discriminating variables
    for Sig in os.listdir(Options.SignalDir):
        if not IsROOTFile(Sig): continue
        if len([x for x in Options.FileFilters if x in Sig]) > 0: continue
        Signals = [
            ROOT.XAMPP.SampleHisto(Sig[:Sig.rfind(".")], var[0], analysis, region, "%s/%s" % (Options.SignalDir, Sig)) for var in variables
        ]
        for i, SigSample in enumerate(Signals):
            SigSample.loadIntoMemory()
            SigSample.SetTitle(variables[i][1])
            SigSample.SetLineColor(Colors[i])
            SigSample.SetLineWidth(3)
            SigSample.SetMarkerStyle(0)
            SigSample.SetMarkerSize(0)

            SigSample.setSampleType(ROOT.XAMPP.SampleHisto.Signal)
            SigSample.SetLumi(Options.lumi)

        ### Next step is to avaluate the significance for each variable
        if not plotSignificances(Options, Histos_Bkg, Signals, Signals[0].GetName(), bonus_str): continue
        plotVariables(Options, Signals, Signals[0].GetName(), bonus_str)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description=
        "This script plots the cumulative distributions of different discriminating variables and their respective significances in the 4L analysis.",
        prog='VariableChoice',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.add_argument("--SignalDir",
                        help="Directory where all signals are processed.",
                        default="/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-01-26/GGM_Sensitivity/")
    parser.add_argument("--UseSUSYSignificance", help="Use the Buttinger - Lefebre significance", action='store_true', default=False)
    parser.set_defaults(outputDir="DiscriminatingVariables/")
    parser.add_argument("--BkgUncertainty", help="Assumed uncertainty on the background", default=0.3, type=float)

    parser.add_argument("--FileFilters",
                        help="Filter expressions to sort out different file names",
                        nargs="+",
                        default=[
                            "data",
                            "Sherpa",
                            "PowHeg",
                            "MG5",
                            "Reducible",
                            "aMC",
                            "aMc",
                            "BiLepton",
                            "MGPy8EG",
                        ])
    parser.set_defaults(lumi=139.)

    PlottingOptions = parser.parse_args()
    CreateDirectory(PlottingOptions.outputDir, False)

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = GetStructure(PlottingOptions)

    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)

    Variables_ToPlot = [
        ("Cul_meff", "m_{eff}"),
        ("Signal_HtLep", "H_{T}^{lepton}"),
        ("Ht_Jet", "H_{T}^{jet}"),
        ("Cul_met", "E_{T}^{miss}"),
        #("LowCut_MtLepMin", "m_{T}^{min}"),
        #("LowCut_MtLepMax", "m_{T}^{max}"),
    ]
    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            drawComparison(PlottingOptions, ana, region, Variables_ToPlot, bonus_str="")
