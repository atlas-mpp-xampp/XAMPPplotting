import os, commands, time, argparse, logging
from XAMPPplotting.FileStructureHandler import GetFileHandler
from XAMPPplotting.FileUtils import ReadInputConfig, ReadMetaInputs
from ClusterSubmission.Utils import CheckConfigPaths, id_generator, WriteList, ReadListFromFile, IsROOTFile, setup_engine, setupBatchSubmitArgParser, AppendToList, FillWhiteSpaces
from distutils.spawn import find_executable

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)


class ClusterAnalysisSubmit(object):
    def __init__(
            self,
            cluster_engine=None,
            input_cfgs=[],
            histo_cfg="",
            run_cfgs=[],
            tree_cfgs=[],
            hold_jobs=[],
            ## Number of jobs per parallelised session
            nSystPerArray=-1,
            noSyst=False,
            noSystK=False,  ### Disable only kinematic systematics
            noPRW=False,
            ### Number of runconfigs per array
            nRunCfgPerJob=1,
            nFilesPerJob=-1,
            remaining_split=1,
            # Timing information
            runTime='24:00:00',
            runMem=1400,
            submit_meta_jobs=True,
            files_per_merge=50,
            histoExec="WriteDefaultHistos",
            treeExec="WriteDefaultTrees"):

        self.__cluster_engine = cluster_engine
        self.__histoExec = histoExec
        self.__treeExec = treeExec
        self.__cluster_engine = cluster_engine

        self.__run_cfg_name = id_generator(90)
        self.__in_cfg_name = id_generator(90)
        self.__out_cfg_name = id_generator(90)

        ### Histo config
        self.__histo_job_cfg = self.engine().link_to_copy_area(histo_cfg)
        self.__scheduled_jobs = 0
        #### Splitting variables
        self.__noSyst = noSyst
        self.__noSystK = noSystK or noSyst
        self.__noPRW = noPRW
        self.__n_runCfgPerJob = nRunCfgPerJob
        self.__n_systPerArray = nSystPerArray
        self.__n_filesPerJob = nFilesPerJob
        ### Meta data
        self.__submit_meta_jobs = submit_meta_jobs
        self.__meta_cfgs = []
        ### Hold jobs
        self.__hold_jobs = [H for H in hold_jobs]
        ### Merging
        self.__merge_interfaces = []
        self.__files_per_merge_itr = files_per_merge
        self.__remaining_split = remaining_split
        ### Memory & run time
        self.__run_time = runTime
        self.__vmem = runMem
        GetFileHandler().switchOffMsg()
        self.__write_plotting_cfgs(run_cfgs=run_cfgs, in_cfgs=input_cfgs)
        self.__write_treemaking_cfgs(tree_cfgs=tree_cfgs, in_cfgs=input_cfgs)

    ### This method writes the job configuration to launch histo jobs
    def __write_plotting_cfgs(self, run_cfgs=[], in_cfgs=[]):
        if not self.__histo_job_cfg: return False
        ### Actually we do not need this anymore
        #if not self.engine().submit_hook(): return False
        if len(run_cfgs) == 0: return False
        if len(in_cfgs) == 0:
            logging.error("No valid input configs were given")
            return False
        ### Then check if run configs are submitted in bundles
        if self.__n_runCfgPerJob > 1:
            new_cfgs = []
            cfg_content = []
            for n, cfg in enumerate(run_cfgs, 1):
                ### If the file does not exist bail out immediately
                linked_cfg = self.engine().link_to_copy_area(cfg)
                if not linked_cfg: return False
                cfg_content += ["Import %s" % (linked_cfg)]
                if n % self.__n_runCfgPerJob == 0 or n == len(run_cfgs):
                    list_name = WriteList(cfg_content, "%s/%s.conf" % (self.engine().config_dir(), id_generator(35)))
                    new_cfgs += [list_name]
                    cfg_content = ["#### Temporary configuration file written to summarize one or the other run config inside one job"]
            ### Finally overwrite the old run_configs
            run_cfgs = new_cfgs

        #### write dedicated configs if no systematics should be applied
        if self.__noSyst or self.__noPRW or self.__noSystK:
            new_cfgs = []
            for cfg in run_cfgs:
                list_name = "%s/%s.conf" % (self.engine().config_dir(), id_generator(30))
                extra_opt = []
                if self.__noPRW: extra_opt += ["noPRW"]
                if self.__noSyst: extra_opt += ["noSyst"]
                if self.__noSystK: extra_opt += ["noSystK"]
                WriteList(extra_opt + ReadListFromFile(cfg), list_name)
                new_cfgs += [list_name]
            run_cfgs = new_cfgs
        elif self.__n_systPerArray < 1:
            run_cfgs = [self.engine().link_to_copy_area(cfg) for cfg in run_cfgs]
        #### Next step is to create a configuration list
        for cfg in in_cfgs:
            if not self.__assemble_job_cfg(cfg, run_cfgs, process_histos=True):
                if os.path.exists(self.engine().config_dir()): os.system("rm -rf %s" % (self.engine().config_dir()))
                self.__scheduled_jobs = 0
                return False
        return True

    def __write_treemaking_cfgs(self, tree_cfgs=[], in_cfgs=[]):
        if self.__histo_job_cfg: return False
        if len(tree_cfgs) == 0:
            logging.error("No tree configs were given")
            return False
        if len(in_cfgs) == 0:
            logging.error("No valid input configs were given")
            return False
        ### No longer needed due to unique config file naming
        #if not self.engine().submit_hook(): return False

        if self.__noSyst or self.__noSystK:
            extra_opt = []
            if self.__noSyst: extra_opt += ["noSyst"]
            if self.__noSystK: extra_opt += ["noSystK"]

            new_cfgs = []
            for cfg in tree_cfgs:
                list_name = "%s/%s.conf" % (self.engine().config_dir(), id_generator(30))
                WriteList(extra_opt + ReadListFromFile(cfg), list_name)
                new_cfgs += [list_name]
            tree_cfgs = new_cfgs
        elif self.__n_systPerArray < 1:
            tree_cfgs = [self.engine().link_to_copy_area(cfg) for cfg in tree_cfgs]
        for cfg in in_cfgs:
            if not self.__assemble_job_cfg(cfg, tree_cfgs, process_histos=False):
                if os.path.exists(self.engine().config_dir()): os.system("rm -rf %s" % (self.engine().config_dir()))
                self.__scheduled_jobs = 0
                return False
        return True

    def __getKinematicSyst(self, in_rootFile):
        if self.__noSystK: return ["Nominal"]
        import ROOT
        root_file = GetFileHandler().LoadFile(in_rootFile).get()
        if not root_file:
            logging.error("<__getKinematicSyst>: File %s seems to be not valid" % (in_rootFile))
            return []
        TreesInFile = [
            Key.GetName() for Key in root_file.GetListOfKeys() if Key.ReadObj().InheritsFrom("TTree") and Key.GetName() != "MetaDataTree"
        ]
        AnalysisName = ""
        try:
            AnalysisName = [Tree[:Tree.rfind("_")] for Tree in TreesInFile if Tree.endswith("Nominal")][0]
        except:
            logging.error("<__getKinematicSyst>: No tree found ending with _Nominal")
            return []
        return sorted([T[len(AnalysisName) + 1:] for T in TreesInFile if T.startswith(AnalysisName)])

    def __assemble_job_cfg(self, incfg, run_cfgs, process_histos=True):
        import ROOT
        copied_runs = [r for r in run_cfgs]
        max_letters = max([len(r) for r in run_cfgs])
        logging.info(FillWhiteSpaces(max_letters + 4, Space="#"))
        logging.info("Will assemble jobs for the following %d %s config files" % (len(run_cfgs), "Run" if process_histos else "Tree"))
        for r in copied_runs:
            logging.info("  + %s" % (r))
        logging.info(FillWhiteSpaces(max_letters + 4, Space="-"))
        ### Read the ROOT files.
        root_files = ReadInputConfig(incfg)
        n_files = len(root_files)
        if len(root_files) == 0:
            logging.error("%s does not seem to contain any ROOT file" % (incfg))
            return False
        linked_in = self.engine().link_to_copy_area(incfg)
        logging.info("<assemble_job_cfg>: Assemble the job for %s" % (incfg))
        ### Do not resubmit the meta data job if the meta input is already defined
        if self.__submit_meta_jobs and len(ReadMetaInputs(incfg)) == 0:
            meta_file = "%s/%s.root" % (self.engine().tmp_dir(), id_generator(44))
            self.__meta_cfgs += [(meta_file, linked_in)]
            AppendToList(["MetaInput %s" % (meta_file)], linked_in)

        if self.__n_systPerArray > 0 and not self.__noSyst:
            ### Obtain first the kinematic systematics
            kine_syst = self.__getKinematicSyst(root_files[0])
            if len(kine_syst) == 0:
                logging.error("<assemble_job_cfg>: No systematics were provided")
                return False
            syst_cfgs = []
            nominal_tree_name = [
                Key.GetName() for Key in GetFileHandler().LoadFile(root_files[0]).GetListOfKeys()
                if Key.GetName().endswith("Nominal") and Key.ReadObj().InheritsFrom("TTree")
            ][0]
            nominal_tree = GetFileHandler().LoadFile(root_files[0]).Get(nominal_tree_name)
            for run in copied_runs:
                syst_to_add = []
                #### Add the kinematic systematics first because they
                #### must be the same for every run config
                for n, syst in enumerate(kine_syst):
                    if n + 1 == len(kine_syst): syst_to_add += [syst]
                    if (n > 0 and n % self.__n_systPerArray == 0) or n + 1 == len(kine_syst):
                        new_cfg = "%s/%s.conf" % (self.engine().config_dir(), id_generator(40))
                        if process_histos:
                            WriteList(ReadListFromFile(run) + ["Systematic %s" % (" ".join(syst_to_add))] + ["noSystW"], new_cfg)
                        else:
                            WriteList(
                                ReadListFromFile(run) + [
                                    "RunConfig %s" % (WriteList(["Systematic %s" % (" ".join(syst_to_add))], "%s/%s.conf" %
                                                                (self.engine().config_dir(), id_generator(39))))
                                ], new_cfg)

                        syst_cfgs += [new_cfg]
                        syst_to_add = []
                    syst_to_add += [syst]
                syst_to_add = []
                if not process_histos: continue
                ### Now parse the weight systematics to the system
                ROOT.XAMPP.NormalizationDataBase.resetDataBase()
                m_AnalysisSetup = ROOT.XAMPP.AnalysisSetup()
                m_AnalysisSetup.LoadWeightsOnly()
                if not m_AnalysisSetup.ParseConfig(run, run, incfg):
                    return False
                Weighter = ROOT.XAMPP.Weight.getWeighter(False)
                if not Weighter.InitWeights(): return False
                weight_syst = Weighter.FindWeightVariations(nominal_tree) if m_AnalysisSetup.do_weight_syst() else []
                for n, syst in enumerate(weight_syst):
                    if n > 0 and n % self.__n_systPerArray == 0:
                        new_cfg = "%s/%s.conf" % (self.engine().config_dir(), id_generator(40))
                        WriteList(ReadListFromFile(run) + ["noSystK", "noNominal", "SystematicW %s" % (" ".join(syst_to_add))], new_cfg)
                        syst_cfgs += [new_cfg]
                        syst_to_add = []
                    syst_to_add += [syst]
                ### Erase the syst to add variable just to make sure
                if len(syst_to_add) > 0:
                    new_cfg = "%s/%s.conf" % (self.engine().config_dir(), id_generator(40))
                    WriteList(ReadListFromFile(run) + ["noSystK", "noNominal", "SystematicW %s" % (" ".join(syst_to_add))], new_cfg)
                    syst_cfgs += [new_cfg]
            copied_runs = syst_cfgs
        ### Define the split according to the number of jobs per file
        begin = 0
        end = n_files if self.__n_filesPerJob < 0 or n_files < self.__n_filesPerJob else self.__n_filesPerJob
        job_in_cfgs = []
        job_run_cfgs = []

        submitted_blocks = 0
        while end <= n_files:
            job_run_cfgs += copied_runs
            job_in_cfgs += ["%s --begin %d --end %d" % (linked_in, begin, end) for i in range(len(copied_runs))]
            begin = end
            if self.__n_filesPerJob < 0: end += 1
            elif end + self.__n_filesPerJob <= n_files: end += self.__n_filesPerJob
            elif end < n_files: end = n_files
            else: end += 1
            submitted_blocks += 1
        ### First write the lists to the disk
        ### Append everything to the end in order to preserve the dependency
        ### of previously configured jobs in the chain
        AppendToList(job_in_cfgs, self.input_job_cfg())
        AppendToList(job_run_cfgs, self.run_job_cfg())

        #### That is the number of files we must merge in the end
        out_name = incfg[incfg.rfind("/") + 1:incfg.rfind(".")]
        assembled_configs = [] if not os.path.exists(self.tmp_file_name_cfg()) else ReadListFromFile(self.tmp_file_name_cfg())
        files_to_merge = ["%s/%s_%d.root" % (self.engine().tmp_dir(), out_name, i) for i in range(len(job_run_cfgs))]

        #### Merge jobs are submitted in the old way, however we must tell on which chain in the array
        #### It should depend. Otherwise users will just wait forever until all analysis jobs are done.
        #### There is no guarantee that the jobs will all succeed in the end. So we should make the
        #### output files as soon as possible
        AppendToList(files_to_merge, self.tmp_file_name_cfg())
        self.__scheduled_jobs += len(job_run_cfgs)
        start_reg = len(assembled_configs)

        #### Submit the merge jobs
        self.__merge_interfaces += [
            self.engine().create_merge_interface(
                out_name=out_name,
                files_to_merge=files_to_merge,
                hold_jobs=[(self.job_name(), [start_reg + i + 1 for i in range(len(files_to_merge))])],
                files_per_job=self.__files_per_merge_itr
                if process_histos or self.__n_systPerArray < 0 or self.__remaining_split == 1 else len(copied_runs),
                final_split=self.__remaining_split if not process_histos else 1,
                shuffle_files=submitted_blocks == 1 or self.__remaining_split == 1,
            )
        ]
        return True

    def merge_interfaces(self):
        return self.__merge_interfaces

    def engine(self):
        return self.__cluster_engine

    def job_name(self):
        return self.engine().job_name()

    ### Location of the histo_cfg
    def histo_job_cfg(self):
        return self.__histo_job_cfg

    ### Location where the input job cfg is stored
    def input_job_cfg(self):
        return "%s/%s.conf" % (self.engine().config_dir(), self.__in_cfg_name)

    ### Location where the temporary files are stored
    def tmp_file_name_cfg(self):
        return "%s/%s.conf" % (self.engine().config_dir(), self.__out_cfg_name)

    def histo_exec(self):
        return self.__histoExec

    def tree_exec(self):
        return self.__treeExec

    ### Location where the run job cfg is stored
    def run_job_cfg(self):
        return "%s/%s.conf" % (self.engine().config_dir(), self.__run_cfg_name)

    def common_meta_data(self):
        return self.__submit_meta_jobs

    def n_sheduled(self):
        return self.__scheduled_jobs

    def run_time(self):
        return self.__run_time

    def memory(self):
        return self.__vmem

    def submit_meta_jobs(self):
        if self.common_meta_data() and len(self.__meta_cfgs) > 0:
            meta_exec_list = "%s/%s" % (self.engine().config_dir(), id_generator(32))
            WriteList(["TemporaryMetaTree -o %s -i %s" % (meta_file, in_cfg) for meta_file, in_cfg in self.__meta_cfgs], meta_exec_list)
            if not self.engine().submit_array(script="ClusterSubmission/Run.sh",
                                              sub_job="MetaData",
                                              mem=self.engine().merge_mem(),
                                              run_time=self.engine().merge_time(),
                                              hold_jobs=self.hold_jobs(),
                                              env_vars=[("ListOfCmds", meta_exec_list)],
                                              array_size=len(self.__meta_cfgs)):
                return False
        return True

    def hold_jobs(self):
        return self.__hold_jobs

    def submit(self):
        if self.n_sheduled() == 0:
            logging.error("<submit>: Nothing has been scheduled")
            return False
        if not self.engine().submit_build_job(): return False
        if not self.submit_meta_jobs(): return False
        ### Submit the meta data jobs
        if not self.engine().submit_array(
                script="XAMPPplotting/exeOnBatch.sh",
                mem=self.memory(),
                run_time=self.run_time(),
                hold_jobs=self.hold_jobs() if not self.__submit_meta_jobs else [self.engine().subjob_name("MetaData")],
                env_vars=[
                    ("JobCfg", self.run_job_cfg()),
                    ("OutCfg", self.tmp_file_name_cfg()),
                    ("InCfg", self.input_job_cfg()),
                    ("Exec", self.histo_exec() if self.__histo_job_cfg else self.tree_exec()),
                ] + ([] if not self.histo_job_cfg() else [("HistoCfg", self.histo_job_cfg())]),
                array_size=self.n_sheduled()):
            return False
        to_hold = []
        for merge in self.__merge_interfaces:
            if not merge.submit_job(): return False
            to_hold += [self.engine().subjob_name("merge-%s" % (merge.outFileName()))]
        if not self.engine().submit_clean_all(hold_jobs=to_hold): return False
        return self.engine().finish()


def setupAnalysisParser():
    parser = setupBatchSubmitArgParser()
    parser.set_defaults(Merge_vmem=2000)
    parser.add_argument('--inputConf', '-I', nargs="+", default=[], help="Input configs to run over", required=True)
    parser.add_argument('--runConf', '-R', help='RunConfigs to run. Must be given with Histo configs', nargs='+', default=[])
    parser.add_argument('--histoConf', '-H', help='HistoConfig to run', default="")
    parser.add_argument('--treeConf', '-T', help='TreeConfigs', nargs='+', default=[])

    parser.add_argument("--remainingSplit", help="Specify a remaining split of the files", default=1, type=int)

    parser.add_argument('--HoldJob', default=[], nargs="+", help='Specify job names which should be finished before your job is starting. ')
    parser.add_argument('--RunTime', help='Changes the RunTime of the analysis Jobs', default='07:59:59')
    parser.add_argument('--vmem', help='Changes the virtual memory needed by each jobs', type=int, default=2000)
    parser.add_argument('--nSystPerJob', help="How many systematics should be executed per job", type=int, default=-1)
    parser.add_argument('--nFilesPerJob', help="How many input files are processed per job", type=int, default=-1)
    parser.add_argument('--nRunConfPerJob', help="How many RunConf files should be executed per job", type=int, default=1)
    parser.add_argument("--FilesPerMerge", help="How many files per merge iteration should be taken", type=int, default=75)
    # Exchange the executable if you want to use the extensions provided by XAMPPtmva
    parser.add_argument('--HistoExec', help='Which executable should be called for writing histos', default='WriteDefaultHistos')
    parser.add_argument('--TreeExec', help='Which executable should be called for writing trees', default='WriteDefaultTrees')
    parser.add_argument("--noCommonMetaData",
                        help="Disable that a temporary meta data tree is written before the analysis jobs start",
                        action='store_false',
                        default=True)
    parser.add_argument("--noSyst", help="Disable the systematics on the fly", action='store_true', default=False)
    parser.add_argument("--noSystK", help="Disable the kinematic systematics on the fly", action='store_true', default=False)

    parser.add_argument("--noPRW", help="Disable pileup reweighting ont the fly", action='store_true', default=False)

    parser.add_argument("--SpareWhatsProcessedIn",
                        help="If the cluster decided to die during production, you can skip the processed files in the directories",
                        default=[],
                        nargs="+")

    return parser


def main():
    RunOptions = setupAnalysisParser().parse_args()

    Spared_Configs = []
    #### The previous round of cluster screwed up. But had some results. There is no
    #### reason to reprocess them. So successful files are not submitted twice
    if len(RunOptions.SpareWhatsProcessedIn) > 0:
        logging.info("Cluster did not perform so well last time? This little.. butterfingered..")
        for dirToSpare in RunOptions.SpareWhatsProcessedIn:
            if not os.path.isdir(dirToSpare):
                logging.error("I need a directory to look up %s" % (dirToSpare))
                exit(1)
            for finished in sorted(os.listdir(dirToSpare)):
                if not IsROOTFile(finished): continue
                cfg_name = finished[:finished.rfind(".")]
                logging.info("Yeah... %s has already beeen processed. Let's skip it.." % (cfg_name))
                Spared_Configs.append(cfg_name)
    ####
    ####
    ####
    Spared_Configs += [
        x[:x.rfind("_")] for x in Spared_Configs
        if x[x.rfind("_") + 1:].isdigit() and len([y for y in Spared_Configs if y[:y.rfind("_")] == x[:x.rfind("_")] > 1])
    ]

    if not find_executable(RunOptions.HistoExec):
        logging.error("The executable %s to produce historgams is not installed. In which package is that defined?!" %
                      (RunOptions.HistoExec))
        exit(1)
    elif not find_executable(RunOptions.TreeExec):
        logging.error("The executable %s to make small trees is not installed. In which package is that defined?!" % (RunOptions.TreeExec))
        exit(1)

    cluster_engine = setup_engine(RunOptions)
    submit_job = ClusterAnalysisSubmit(
        cluster_engine=cluster_engine,
        input_cfgs=sorted([C for C in CheckConfigPaths(RunOptions.inputConf) if C[C.rfind("/") + 1:C.rfind(".")] not in Spared_Configs]),
        histo_cfg=RunOptions.histoConf,
        run_cfgs=CheckConfigPaths(RunOptions.runConf),
        tree_cfgs=CheckConfigPaths(RunOptions.treeConf),
        ## Number of jobs per parallelized session
        nSystPerArray=RunOptions.nSystPerJob,
        noSyst=RunOptions.noSyst,
        noSystK=RunOptions.noSystK,
        noPRW=RunOptions.noPRW,
        ### Number of run configs per array
        nRunCfgPerJob=RunOptions.nRunConfPerJob,
        nFilesPerJob=RunOptions.nFilesPerJob,
        runTime=RunOptions.RunTime,
        runMem=RunOptions.vmem,
        hold_jobs=RunOptions.HoldJob,
        histoExec=RunOptions.HistoExec,
        submit_meta_jobs=RunOptions.noCommonMetaData,
        files_per_merge=RunOptions.FilesPerMerge,
        remaining_split=RunOptions.remainingSplit,
        treeExec=RunOptions.TreeExec)

    submit_job.submit()


if __name__ == '__main__':
    main()
