import os, sys, math
import argparse
from pprint import pprint
from XAMPPplotting.CreateXsecFromResummino import GetDSIDs, WriteXsectionFile


def CreateArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--InFile", help="Directory where the input files from Resummino are located", nargs="+", default=[])
    parser.add_argument("--FinalStates", help="Directory where the input files from Resummino are located", nargs="+", default=[], type=int)

    parser.add_argument("--OutFile", help="The outfile", default="xSections.txt")
    parser.add_argument('--JobOptionsDir', help="Directory of the files containing the 1-line JO", required=True)
    parser.add_argument("--Models", help="To which models the xSection can be applied", nargs="+", default=[])
    parser.add_argument("--NumInFb", help="The numbers inside the infile are given in fb instead of pb", action="store_true", default=False)
    parser.add_argument("--AsymError", help="There are asymmetric errors given in the file", action="store_true", default=False)

    return parser


def ReadLHCxSectionFile(InFile, FinalState=0):
    xSecDict = {}
    File = open(InFile, "r")
    for Line in File:
        Line = Line.strip().replace("GeV", "")
        try:
            Mass = int(Line.split()[0])
            xSection = float(Line.split()[1])
            if Options.NumInFb: xSection *= 1.e-3
        except:
            print "WARNING: Could not do anything with line", Line
            continue
        try:
            if not Options.AsymError:
                #########################################
                #      Mass xSection \pm Error
                #########################################
                if "\xc2\xb1" in Line.split()[2]:
                    if "%" in Line.split()[3]: Error = float(Line.split()[3].replace("%", "")) / 1.e2
                    else: Error = float(Line.split()[3]) / xSection
                ########################################
                #     Mass xSection Error
                ########################################
                else:
                    Error = float(Line.split()[2]) * (1. if not Options.NumInFb else 1.e-3) / xSection
            else:
                print "Whaaat"
            xSecDict[Mass] = {FinalState: (xSection, Error)}
        except:
            pass
    return xSecDict


if __name__ == '__main__':
    Options = CreateArgumentParser().parse_args()
    xSecDB = {}
    for i in range(0, len(Options.InFile)):
        nextDB = ReadLHCxSectionFile(Options.InFile[i], Options.FinalStates[i])
        if i == 0: xSecDB = nextDB
        else:
            for x in xSecDB.iterkeys():
                if x in nextDB.iterkeys(): xSecDB[x].update(nextDB[x])

    DSIDs = GetDSIDs(Options)
    WriteXsectionFile(Options.OutFile, DSIDs, xSecDB)
