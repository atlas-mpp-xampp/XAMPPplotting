import ROOT
import argparse

import collections

import math
import itertools
from array import array
from ClusterSubmission.Utils import CreateDirectory


class FakeFactorWeight(object):
    def __init__(self, options):
        self.FakeFactor = {}
        print "initialising Fakefactor Histos"
        Regions = [
            "3L1l", "3L1lnoZ", "3L1lZ", "2L2l", "2L2lnoZ", "2L2lZ", "2L1l1t", "2L1l1tnoZ", "2L1l1tZ", "2L1T1l", "2L1T1lnoZ", "2L1T1lZ",
            "2L1T1t", "2L1T1tnoZ", "2L1T1tZ", "2L2t", "2L2tnoZ", "2L2tZ", "3L1t", "3L1tnoZ", "3L1tZ"
        ]
        for region in Regions:
            self.FakeFactor[region] = FakeFactor(region, options)
        print "initialising Fakefactor Histos. Done"

    def EvaluateFakeFactorWeight(self, event, region, lightleptonnumber, taunumber, isCR2=False):
        if (len(event.LooseEle_pt) + len(event.LooseMuo_pt) < lightleptonnumber):
            print "Warning: Number of loose light lepton is lower than the needed number in this Control region. N_elec = {}, N_muon = {}, Region: {}. returning 1".format(
                len(event.LooseEle_pt), len(event.LooseMuo_pt), region)
            return 1
        if (len(event.LooseTau_pt) < taunumber):
            print "Warning: Number of loose tau is lower than the needed number in this Control region. N_tau = {}, Region: {}. returning 1".format(
                len(event.LooseTau_pt), region)
            return 1

        if (lightleptonnumber + taunumber) > 2:
            print "ERROR: number of fakeleptons > 2, not implemented, returning 0"
            return 0

        FakeFactors_lightlepton = []

        FakeFactors_tau = []

        if not lightleptonnumber == 0:
            for i in range(len(event.LooseEle_pt)):  # just loop over pt, eta has the same number of entries
                pt = event.LooseEle_pt[i]
                eta = event.LooseEle_eta[i]
                FakeFactors_lightlepton.append(self.FakeFactor[region].EvaluateWeight("Electron", pt, abs(eta)))

            # ~ for i in range(len(event.LooseMuo_pt)):
            # ~ pt = event.LooseMuo_pt[i]
            # ~ eta = event.LooseMuo_eta[i]
            # ~ FakeFactors_lightlepton.append(self.FakeFactor[region].EvaluateWeight("Muon", pt, abs(eta)))
            # ~ FakeFactors_lightlepton.append(1)

        if len(FakeFactors_lightlepton) == 0:
            Fakeweights = {"Nominal": 1}
            return Fakeweights
        LightLepDenominator = math.factorial(len(FakeFactors_lightlepton)) / (
            math.factorial(lightleptonnumber) * math.factorial(len(FakeFactors_lightlepton) - lightleptonnumber))

        LightLepCombinations = itertools.combinations(FakeFactors_lightlepton, lightleptonnumber)

        if not taunumber == 0:
            for i in range(len(event.LooseTau_pt)):

                pt = event.LooseTau_pt[i]
                eta = event.LooseTau_eta[i]

                if event.LooseTau_prongs[i] == 1:
                    particle = "1P_Tau"
                if event.LooseTau_prongs[i] == 3:
                    particle = "3P_Tau"
                FakeFactors_tau.append(self.FakeFactor[region].EvaluateWeight(particle, pt, abs(eta)))

        TauDenominator = math.factorial(
            len(FakeFactors_tau)) / (math.factorial(taunumber) * math.factorial(len(FakeFactors_tau) - taunumber))

        TauCombinations = itertools.combinations(FakeFactors_tau, taunumber)

        FakeFactorWeight_lightlep = 0

        FakeFactorWeight_tau = 0

        if lightleptonnumber == 1:
            for FF in LightLepCombinations:
                FakeFactorWeight_lightlep += FF[0]

        elif lightleptonnumber == 2:
            for FF in LightLepCombinations:
                FakeFactorWeight_lightlep += FF[0] * FF[1]

        FakeFactorWeight_lightlep = FakeFactorWeight_lightlep / LightLepDenominator

        if taunumber == 1:
            for FF in TauCombinations:
                FakeFactorWeight_tau += FF[0]

        elif taunumber == 2:
            for FF in TauCombinations:
                FakeFactorWeight_tau += FF[0] * FF[1]

        FakeFactorWeight_tau = FakeFactorWeight_tau / TauDenominator

        if not lightleptonnumber == 0 and not taunumber == 0:
            FakeFactorWeight = FakeFactorWeight_lightlep * FakeFactorWeight_tau

        elif not lightleptonnumber == 0:
            FakeFactorWeight = FakeFactorWeight_lightlep

        elif not taunumber == 0:
            FakeFactorWeight = FakeFactorWeight_tau

        else:
            print "Error: number of tau and lightlepton is 0"
            return 0
        if isCR2 == True:
            FakeFactorWeight = -FakeFactorWeight

        Fakeweights = {"Nominal": FakeFactorWeight}

        return Fakeweights


class FakeFactor(object):
    def __init__(self, CRregion, options):
        self.options = options
        self.FakeTypes = options.faketypes  # Faketypes to be considered
        self.Processes = options.processes  # processes to be considered
        self.useProcessfraction = True
        # name of the fakefactorhistos

        self.FakeFactorElectron_ttbar = {
            # ~ "LF": "LF_Nominal/CRt#bar{t}/CRt#bar{t}_Electrons_origLF_pteta",
            "HF": "HF_Nominal/CRt#bar{t}/CRt#bar{t}_Electrons_origHF_pteta"
            # ~ "Conv": "Conv_Nominal/CRt#bar{t}/CRt#bar{t}_Electrons_origConv_pteta"
        }

        self.FakeFactorElectron_Zjets = {
            # ~ "LF": "LF_Nominal/Z#mu#mu/Z#mu#mu_Electrons_origLF_pteta",
            # ~ "HF": "HF_Nominal/Z#mu#mu/Z#mu#mu_Electrons_origHF_pteta",
            # ~ "Conv": "Conv_Nominal/Z#mu#mu/Z#mu#mu_Electrons_origConv_pteta"
        }

        # ~ self.FakeFactorMuon_ttbar = {
        # ~ "LF": "LF_Nominal/CRt#bar{t}/CRt#bar{t}_Muons_origLF_pteta",
        # ~ "HF": "HF_Nominal/CRt#bar{t}/CRt#bar{t}_Muons_origHF_pteta"
        # ~ }

        # ~ self.FakeFactorMuon_Zjets = {
        # ~ "LF": "LF_Nominal/Z#mu#mu/Z#mu#mu_Muons_origLF_pteta",
        # ~ "HF": "HF_Nominal/Z#mu#mu/Z#mu#mu_Muons_origHF_pteta"
        # ~ }

        # ~ self.FakeFactor1PTau = {
        # ~ "LF": "LF_Nominal/FakeFactor/FakeFactor_Tau_origLF_prong1P_pteta",
        # ~ "HF": "HF_Nominal/FakeFactor/FakeFactor_Tau_origHF_prong1P_pteta",
        # ~ "Gluon": "Gluon_Nominal/FakeFactor/FakeFactor_Tau_origGluon_prong1P_pteta",
        # ~ "Elec": "Elec_Nominal/FakeFactor/FakeFactor_Tau_origElec_prong1P_pteta",
        # ~ "Conv": "Conv_Nominal/FakeFactor/FakeFactor_Tau_origConv_prong1P_pteta"
        # ~ }

        # ~ self.FakeFactor3PTau = {
        # ~ "LF": "LF_Nominal/FakeFactor/FakeFactor_Tau_origLF_prong3P_pteta",
        # ~ "HF": "HF_Nominal/FakeFactor/FakeFactor_Tau_origHF_prong3P_pteta",
        # ~ "Gluon": "Gluon_Nominal/FakeFactor/FakeFactor_Tau_origGluon_prong3P_pteta",
        # ~ "Elec": "Elec_Nominal/FakeFactor/FakeFactor_Tau_origElec_prong3P_pteta",
        # ~ "Conv": "Conv_Nominal/FakeFactor/FakeFactor_Tau_origConv_prong3P_pteta"
        # ~ }

        # ~ self.ScaleFactorElectron = {"HF": "HF_Nominal/ScaleFactor/ScaleFactor_Electrons_origHF_pt"}

        # ~ self.ScaleFactorMuon = {"HF": "HF_Nominal/ScaleFactor/ScaleFactor_Muons_origHF_pt"}

        # ~ self.ScaleFactor1PTau = {
        # ~ "LF": "LF_Nominal/ScaleFactor/ScaleFactor_Tau_origLF_prong1P_pt",
        # ~ "HF": "HF_Nominal/ScaleFactor/ScaleFactor_Tau_origHF_prong1P_pt"
        # ~ }

        # ~ self.ScaleFactor3PTau = {
        # ~ "LF": "LF_Nominal/ScaleFactor/ScaleFactor_Tau_origLF_prong3P_pt",
        # ~ "HF": "HF_Nominal/ScaleFactor/ScaleFactor_Tau_origHF_prong3P_pt"
        # ~ }

        self.ProcFracElectron = {
            # ~ "ttbar": {
            # ~ "LF": "%s/ttbar/LF/ProcessFraction_LooseElectrons_origLF_pteta" % (CRregion),
            # ~ "HF": "%s/ttbar/HF/ProcessFraction_LooseElectrons_origHF_pteta" % (CRregion),
            # ~ "Conv": "%s/ttbar/Conv/ProcessFraction_LooseElectrons_origConv_pteta" % (CRregion)
            # ~ },
            # ~ "Zjets": {
            # ~ "LF": "%s/Zjets/LF/ProcessFraction_LooseElectrons_origLF_pteta" % (CRregion),
            # ~ "HF": "%s/Zjets/HF/ProcessFraction_LooseElectrons_origHF_pteta" % (CRregion),
            # ~ "Conv": "%s/Zjets/Conv/ProcessFraction_LooseElectrons_origConv_pteta" % (CRregion)
            # ~ }
        }

        # ~ self.ProcFracMuon = {
        # ~ "ttbar": {
        # ~ "LF": "%s/ttbar/LF/ProcessFraction_LooseMuons_origLF_pteta" % (CRregion),
        # ~ "HF": "%s/ttbar/HF/ProcessFraction_LooseMuons_origHF_pteta" % (CRregion)
        # ~ },
        # ~ "Zjets": {
        # ~ "LF": "%s/Zjets/LF/ProcessFraction_LooseMuons_origLF_pteta" % (CRregion),
        # ~ "HF": "%s/Zjets/HF/ProcessFraction_LooseMuons_origHF_pteta" % (CRregion)
        # ~ }
        # ~ }

        # ~ self.ProcFrac1PTau = {
        # ~ "ttbar": {
        # ~ "LF": "%s/ttbar/LF/ProcessFraction_LooseTau_origLF_prong1P_pteta" % (CRregion),
        # ~ "HF": "%s/ttbar/HF/ProcessFraction_LooseTau_origHF_prong1P_pteta" % (CRregion),
        # ~ "Gluon": "%s/ttbar/Gluon/ProcessFraction_LooseTau_origGluon_prong1P_pteta" % (CRregion),
        # ~ "Elec": "%s/ttbar/Elec/ProcessFraction_LooseTau_origElec_prong1P_pteta" % (CRregion),
        # ~ "Conv": "%s/ttbar/Conv/ProcessFraction_LooseTau_origConv_prong1P_pteta" % (CRregion)
        # ~ },
        # ~ "Zjets": {
        # ~ "LF": "%s/Zjets/LF/ProcessFraction_LooseTau_origLF_prong1P_pteta" % (CRregion),
        # ~ "HF": "%s/Zjets/HF/ProcessFraction_LooseTau_origHF_prong1P_pteta" % (CRregion),
        # ~ "Gluon": "%s/Zjets/Gluon/ProcessFraction_LooseTau_origGluon_prong1P_pteta" % (CRregion),
        # ~ "Elec": "%s/Zjets/Elec/ProcessFraction_LooseTau_origElec_prong1P_pteta" % (CRregion),
        # ~ "Conv": "%s/Zjets/Conv/ProcessFraction_LooseTau_origConv_prong1P_pteta" % (CRregion)
        # ~ }
        # ~ }

        # ~ self.ProcFrac3PTau = {
        # ~ "ttbar": {
        # ~ "LF": "%s/ttbar/LF/ProcessFraction_LooseTau_origLF_prong3P_pteta" % (CRregion),
        # ~ "HF": "%s/ttbar/HF/ProcessFraction_LooseTau_origHF_prong3P_pteta" % (CRregion),
        # ~ "Gluon": "%s/ttbar/Gluon/ProcessFraction_LooseTau_origGluon_prong3P_pteta" % (CRregion),
        # ~ "Elec": "%s/ttbar/Elec/ProcessFraction_LooseTau_origElec_prong3P_pteta" % (CRregion),
        # ~ "Conv": "%s/ttbar/Conv/ProcessFraction_LooseTau_origConv_prong3P_pteta" % (CRregion)
        # ~ },
        # ~ "Zjets": {
        # ~ "LF": "%s/Zjets/LF/ProcessFraction_LooseTau_origLF_prong3P_pteta" % (CRregion),
        # ~ "HF": "%s/Zjets/HF/ProcessFraction_LooseTau_origHF_prong3P_pteta" % (CRregion),
        # ~ "Gluon": "%s/Zjets/Gluon/ProcessFraction_LooseTau_origGluon_prong3P_pteta" % (CRregion),
        # ~ "Elec": "%s/Zjets/Elec/ProcessFraction_LooseTau_origElec_prong3P_pteta" % (CRregion),
        # ~ "Conv": "%s/Zjets/Conv/ProcessFraction_LooseTau_origConv_prong3P_pteta" % (CRregion)
        # ~ }
        # ~ }

        self.FakeFactorFile_lightlep_ttbar = ROOT.TFile.Open("%s/FakeFactors_lightlepton_ttbar.root" % (options.FakefactorFiles))
        self.FakeFactorFile_lightlep_Zjets = ROOT.TFile.Open("%s/FakeFactors_lightlepton_Zjets.root" % (options.FakefactorFiles))

        self.FakeFactorFile_tau_ttbar = ROOT.TFile.Open("%s/FakeFactors_tau_ttbar.root" % (options.FakefactorFiles))
        self.FakeFactorFile_tau_Zjets = ROOT.TFile.Open("%s/FakeFactors_tau_Zjets.root" % (options.FakefactorFiles))

        self.ScaleFactorFile_lightlep = ROOT.TFile.Open("%s/ScaleFactor_lightlepton.root" % (options.FakefactorFiles))
        self.ScaleFactorFile_tau = ROOT.TFile.Open("%s/ScaleFactor_tau.root" % (options.FakefactorFiles))

        if "ttbar" in self.Processes and not "Zjets" in self.Processes:
            self.ProcessfractionFile_tau = ROOT.TFile.Open("%s/ttbar/ProcessFraction_tau_ttbar.root" % (options.FakefactorFiles))
            self.ProcessfractionFile_lightlep = ROOT.TFile.Open("%s/ttbar/ProcessFraction_lightlepton_ttbar.root" %
                                                                (options.FakefactorFiles))
        elif "Zjets" in self.Processes and not "ttbar" in self.Processes:

            self.ProcessfractionFile_tau = ROOT.TFile.Open("%s/Zjets/ProcessFraction_tau_Zjets.root" % (options.FakefactorFiles))
            self.ProcessfractionFile_lightlep = ROOT.TFile.Open("%s/Zjets/ProcessFraction_lightlepton_Zjets.root" %
                                                                (options.FakefactorFiles))

        else:
            self.ProcessfractionFile_tau = ROOT.TFile.Open("%s/ProcessFraction_tau.root" % (options.FakefactorFiles))
            self.ProcessfractionFile_lightlep = ROOT.TFile.Open("%s/ProcessFraction_lightlepton.root" % (options.FakefactorFiles))

        self.FakeFactorHistos_Electron = {}
        self.ProcessFractionHistos_Electron = {}
        # ~ self.FakeFactorHistos_Muon = {}
        # ~ self.ProcessFractionHistos_Muon = {}
        # ~ self.FakeFactorHistos_1prong = {}
        # ~ self.ProcessFractionHistos_1prong = {}
        # ~ self.FakeFactorHistos_3prong = {}
        # ~ self.ProcessFractionHistos_3prong = {}

        # ~ self.ScaleFactorHistos_Electron = {}
        # ~ self.ScaleFactorHistos_Muon = {}
        # ~ self.ScaleFactorHistos_1PTau = {}
        # ~ self.ScaleFactorHistos_3PTau = {}

        # loading the fakefactorhistos
        for process in self.Processes:
            self.FakeFactorHistos_Electron[process] = {}
            self.ProcessFractionHistos_Electron[process] = {}
            # ~ self.FakeFactorHistos_Muon[process] = {}
            # ~ self.ProcessFractionHistos_Muon[process] = {}
            # ~ self.FakeFactorHistos_1prong[process] = {}
            # ~ self.ProcessFractionHistos_1prong[process] = {}
            # ~ self.FakeFactorHistos_3prong[process] = {}
            # ~ self.ProcessFractionHistos_3prong[process] = {}

        if "ttbar" in self.Processes:
            for faketype, histoname in self.FakeFactorElectron_ttbar.items():
                if faketype in self.FakeTypes:
                    Histo = self.FakeFactorFile_lightlep_ttbar.Get(histoname)
                    self.FakeFactorHistos_Electron["ttbar"][faketype] = Histo
        if "Zjets" in self.Processes:
            for faketype, histoname in self.FakeFactorElectron_Zjets.items():
                if faketype in self.FakeTypes:
                    Histo = self.FakeFactorFile_lightlep_Zjets.Get(histoname)
                    self.FakeFactorHistos_Electron["Zjets"][faketype] = Histo

        # ~ if "ttbar" in self.Processes:
        # ~ for faketype, histoname in self.FakeFactorMuon_ttbar.items():
        # ~ if faketype in self.FakeTypes:
        # ~ Histo = self.FakeFactorFile_lightlep_ttbar.Get(histoname)
        # ~ self.FakeFactorHistos_Muon["ttbar"][faketype] = Histo
        # ~ if "Zjets" in self.Processes:
        # ~ for faketype, histoname in self.FakeFactorMuon_Zjets.items():
        # ~ if faketype in self.FakeTypes:
        # ~ Histo = self.FakeFactorFile_lightlep_Zjets.Get(histoname)
        # ~ self.FakeFactorHistos_Muon["Zjets"][faketype] = Histo

        # ~ for faketype, histoname in self.FakeFactor1PTau.items():
        # ~ if faketype in self.FakeTypes:
        # ~ if "ttbar" in self.Processes:
        # ~ Histo = self.FakeFactorFile_tau_ttbar.Get(histoname)
        # ~ self.FakeFactorHistos_1prong["ttbar"][faketype] = Histo
        # ~ if "Zjets" in self.Processes:
        # ~ Histo = self.FakeFactorFile_tau_Zjets.Get(histoname)
        # ~ self.FakeFactorHistos_1prong["Zjets"][faketype] = Histo

        # ~ for faketype, histoname in self.FakeFactor3PTau.items():
        # ~ if faketype in self.FakeTypes:
        # ~ if "ttbar" in self.Processes:
        # ~ Histo = self.FakeFactorFile_tau_ttbar.Get(histoname)
        # ~ self.FakeFactorHistos_3prong["ttbar"][faketype] = Histo
        # ~ if "Zjets" in self.Processes:
        # ~ Histo = self.FakeFactorFile_tau_Zjets.Get(histoname)
        # ~ self.FakeFactorHistos_3prong["Zjets"][faketype] = Histo

        for process in self.ProcFracElectron:
            for faketype, histoname in self.ProcFracElectron[process].items():
                if faketype in self.FakeTypes and process in self.Processes:
                    Histo = self.ProcessfractionFile_lightlep.Get(histoname)
                    self.ProcessFractionHistos_Electron[process][faketype] = Histo

        # ~ for process in self.ProcFracMuon:
        # ~ for faketype, histoname in self.ProcFracMuon[process].items():
        # ~ if faketype in self.FakeTypes and process in self.Processes:
        # ~ Histo = self.ProcessfractionFile_lightlep.Get(histoname)
        # ~ self.ProcessFractionHistos_Muon[process][faketype] = Histo

        # ~ for process in self.ProcFrac1PTau:
        # ~ for faketype, histoname in self.ProcFrac1PTau[process].items():
        # ~ if faketype in self.FakeTypes and process in self.Processes:
        # ~ Histo = self.ProcessfractionFile_tau.Get(histoname)
        # ~ self.ProcessFractionHistos_1prong[process][faketype] = Histo

        # ~ for process in self.ProcFrac3PTau:
        # ~ for faketype, histoname in self.ProcFrac3PTau[process].items():
        # ~ if faketype in self.FakeTypes and process in self.Processes:
        # ~ Histo = self.ProcessfractionFile_tau.Get(histoname)
        # ~ self.ProcessFractionHistos_3prong[process][faketype] = Histo

        # ~ for faketype, histoname in self.ScaleFactorElectron.items():
        # ~ if faketype in self.FakeTypes:
        # ~ Histo = self.ScaleFactorFile_lightlep.Get(histoname)
        # ~ self.ScaleFactorHistos_Electron[faketype] = Histo

        # ~ for faketype, histoname in self.ScaleFactorMuon.items():
        # ~ if faketype in self.FakeTypes:
        # ~ Histo = self.ScaleFactorFile_lightlep.Get(histoname)
        # ~ self.ScaleFactorHistos_Muon[faketype] = Histo

        # ~ for faketype, histoname in self.ScaleFactor1PTau.items():
        # ~ if faketype in self.FakeTypes:
        # ~ Histo = self.ScaleFactorFile_tau.Get(histoname)
        # ~ self.ScaleFactorHistos_1PTau[faketype] = Histo

        # ~ for faketype, histoname in self.ScaleFactor3PTau.items():
        # ~ if faketype in self.FakeTypes:
        # ~ Histo = self.ScaleFactorFile_tau.Get(histoname)
        # ~ self.ScaleFactorHistos_3PTau[faketype] = Histo

    def EvaluateWeight(self, particle, pt, eta):

        if particle == "Electron":
            FakeFactorHistos = self.FakeFactorHistos_Electron
            # ~ ScaleFactorHistos = self.ScaleFactorHistos_Electron
            ProcFracHistos = self.ProcessFractionHistos_Electron
        # ~ elif particle == "Muon":
        # ~ FakeFactorHistos = self.FakeFactorHistos_Muon
        # ~ ScaleFactorHistos = self.ScaleFactorHistos_Muon
        # ~ ProcFracHistos = self.ProcessFractionHistos_Muon
        # ~ elif particle == "1P_Tau":
        # ~ FakeFactorHistos = self.FakeFactorHistos_1prong
        # ~ ScaleFactorHistos = self.ScaleFactorHistos_1PTau
        # ~ ProcFracHistos = self.ProcessFractionHistos_1prong
        # ~ elif particle == "3P_Tau":
        # ~ FakeFactorHistos = self.FakeFactorHistos_3prong
        # ~ ScaleFactorHistos = self.ScaleFactorHistos_3PTau
        # ~ ProcFracHistos = self.ProcessFractionHistos_3prong
        # ~ else:
        # ~ print "unknown particle"
        # ~ return 0

        AverageFakeFactor = 0
        for process in FakeFactorHistos:

            for faketype in FakeFactorHistos[process]:

                FakeFactorHisto = FakeFactorHistos[process][faketype]

                FakeFactorBin = FakeFactorHisto.FindBin(pt, eta)
                FakeFactor = FakeFactorHisto.GetBinContent(FakeFactorBin)

                # ~ ScaleFactor = 1.
                # ~ if self.options.addScalefactor:
                # ~ if faketype in ScaleFactorHistos:
                # ~ ScaleFactorHisto = ScaleFactorHistos[faketype]
                # ~ ScaleFactorBin = ScaleFactorHisto.FindBin(pt)
                # ~ ScaleFactor = ScaleFactorHisto.GetBinContent(ScaleFactorBin)
                if process in ProcFracHistos:
                    if faketype in ProcFracHistos[process]:
                        ProcFracHisto = ProcFracHistos[process][faketype]

                        ProcFracBin = ProcFracHisto.FindBin(pt, eta)
                        ProcFrac = ProcFracHisto.GetBinContent(ProcFracBin)
                        AverageFakeFactor += FakeFactor * ProcFrac
                    else:
                        AverageFakeFactor += FakeFactor
                else:
                    AverageFakeFactor += FakeFactor

        return AverageFakeFactor

    def CalculateVariation(self,
                           particle,
                           pt,
                           eta,
                           Fakefactorvariation=False,
                           Scalefactorvariation=False,
                           Processfractionvariation=False,
                           VariedFaketype="",
                           downvariation=False):
        if particle == "Electron":
            FakeFactorHistos = self.FakeFactorHistos_Electron
            ScaleFactorHistos = self.ScaleFactorHistos_Electron
            ProcFracHistos = self.ProcessFractionHistos_Electron
        elif particle == "Muon":
            FakeFactorHistos = self.FakeFactorHistos_Muon
            ScaleFactorHistos = self.ScaleFactorHistos_Muon
            ProcFracHistos = self.ProcessFractionHistos_Muon
        elif particle == "1P_Tau":
            FakeFactorHistos = self.FakeFactorHistos_1prong
            ScaleFactorHistos = self.ScaleFactorHistos_1PTau
            ProcFracHistos = self.ProcessFractionHistos_1prong
        elif particle == "3P_Tau":
            FakeFactorHistos = self.FakeFactorHistos_3prong
            ScaleFactorHistos = self.ScaleFactorHistos_3PTau
            ProcFracHistos = self.ProcessFractionHistos_3prong
        else:
            print "unknown particle"
            return 0
        if not Fakefactorvariation and not Scalefactorvariation and not Processfractionvariation:
            print "Error: no variation given"
            return 0
        if (Fakefactorvariation and (Scalefactorvariation or Processfractionvariation)) or (
                Scalefactorvariation and
            (Fakefactorvariation or Processfractionvariation)) or (Processfractionvariation and
                                                                   (Fakefactorvariation or Scalefactorvariation)):
            print "Error: Cannot calculate multiple variations simultaniously"
            return 0
        if Scalefactorvariation and VariedFaketype == "":
            print "Error: for which faketype do you want to calculate the scalefactorvariation"
            return 0

        AverageFakeFactor = 0
        for process in FakeFactorHistos:

            for faketype in FakeFactorHistos[process]:

                FakeFactorHisto = FakeFactorHistos[process][faketype]

                FakeFactorBin = FakeFactorHisto.FindBin(pt, eta)
                if Fakefactorvariation:
                    if not downvariation:
                        FakeFactor = FakeFactorHisto.GetBinContent(FakeFactorBin) + FakeFactorHisto.GetBinError(FakeFactorBin)
                    else:
                        FakeFactor = FakeFactorHisto.GetBinContent(FakeFactorBin) - FakeFactorHisto.GetBinError(FakeFactorBin)
                else:
                    FakeFactor = FakeFactorHisto.GetBinContent(FakeFactorBin)

                ScaleFactor = 1.
                if self.options.addScalefactor:
                    if faketype in ScaleFactorHistos:
                        ScaleFactorHisto = ScaleFactorHistos[faketype]
                        ScaleFactorBin = ScaleFactorHisto.FindBin(pt)
                        if Scalefactorvariation and faketype == VariedFaketype:
                            if not downvariation:
                                ScaleFactor = ScaleFactorHisto.GetBinContent(ScaleFactorBin) + ScaleFactorHisto.GetBinError(ScaleFactorBin)
                            else:
                                ScaleFactor = ScaleFactorHisto.GetBinContent(ScaleFactorBin) - ScaleFactorHisto.GetBinError(ScaleFactorBin)
                        else:
                            ScaleFactor = ScaleFactorHisto.GetBinContent(ScaleFactorBin)
                ProcFracHisto = ProcFracHistos[process][faketype]

                ProcFracBin = ProcFracHisto.FindBin(pt, eta)
                if Processfractionvariation:
                    if not downvariation:
                        ProcFrac = ProcFracHisto.GetBinContent(ProcFracBin) + ProcFracHisto.GetBinError(ProcFracBin)
                    else:
                        ProcFrac = ProcFracHisto.GetBinContent(ProcFracBin) - ProcFracHisto.GetBinError(ProcFracBin)
                else:
                    ProcFrac = ProcFracHisto.GetBinContent(ProcFracBin)

                if self.useProcessfraction:
                    AverageFakeFactor += FakeFactor * ProcFrac * ScaleFactor
                else:
                    AverageFakeFactor += FakeFactor * ScaleFactor
        # ~ if particle == "Muon" and  Scalefactorvariation and downvariation:
        # ~ print AverageFakeFactor

        return AverageFakeFactor


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description=
        'This script calculates the fakefactorweight from histfittertrees and creates a tree containing the fakefactorweight  \"python CalculateFakeFactors.py -h\"',
        prog='ApplyFakeFactors',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--inputFile", help="the input histfittertrees", required=True)
    parser.add_argument("--FakefactorFiles", help="Folder containing the fakefactor histograms", default="/ptmp/mpp/maren/Fakefactor/")
    parser.add_argument("--addScalefactor", help="apply scalefactors to the fakefactors", action='store_true', default=False)

    parser.add_argument("--treeName", help="the name of the tree", default="DebugTree_Nominal")
    parser.add_argument("--outDir",
                        help="Specify an output folder where the closureplots get stored",
                        default='/ptmp/mpp/maren/FakeHistFitterTrees/')
    parser.add_argument("--faketypes",
                        help="specify the faketypes that should be used",
                        nargs='+',
                        default=["LF", "HF", "Gluon", "Elec", "Conv"])
    parser.add_argument("--processes", help="specify the processes used for the fakefactors", nargs='+', default=["ttbar", "Zjets"])
    parser.add_argument("--nevents", help="how many events should be processed. All by default", type=int, default=-1)

    options = parser.parse_args()
    if options.addScalefactor:
        print "Apply Scalefactors aswell"
    CreateDirectory(options.outDir, False)

    if options.nevents > 0:
        limitEvents = True
    else:
        limitEvents = False

    FakeFactors = FakeFactorWeight(options)

    File = ROOT.TFile.Open(options.inputFile)

    Tree = File.Get(options.treeName)

    newFileName = options.inputFile.split("/")[-1]

    newFile = ROOT.TFile("{}/{}".format(options.outDir, newFileName), "RECREATE")

    newTree = Tree.CloneTree(options.nevents)

    FakeFactorarray = array('f', [0])

    branch = newTree.Branch("FakefactorWeight", FakeFactorarray, 'FakefactorWeight/F')

    if options.inputFile.split("/")[-1].startswith("data"):
        isData = True
    else:
        isData = False

    print "Start Loop over Events"
    Nevents = 0
    Totalevents = Tree.GetEntries()

    for event in Tree:
        if limitEvents == True and Nevents >= options.nevents:
            break
        if Nevents > 0 and Nevents % 1000 == 0:
            print "Executed {}/{} events".format(Nevents, Totalevents)
        FakeFactorWeight = 1

        # 4L CR1

        if event.Is3L1l == 1:
            FakeFactor = FakeFactors.EvaluateFakeFactorWeight(event, "2L2l", lightleptonnumber=1, taunumber=0)
            FakeFactorWeight = FakeFactor["Nominal"]

        # 4L CR2
        # ~ if event.Is2L2l == 1:

        # ~ FakeFactor = FakeFactors.EvaluateFakeFactorWeight(event, "2L2l", lightleptonnumber=2, taunumber=0, isCR2=True)
        # ~ FakeFactorWeight = FakeFactor["Nominal"]

        FakeFactorarray[0] = FakeFactorWeight

        branch.Fill()

        Nevents += 1
    print "finished loop over events"
    newTree.Write()
    print "Created File: {}".format(options.inputFile.split("/")[-1])
    newFile.Close()

    File.Close()
