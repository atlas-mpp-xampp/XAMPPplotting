import ROOT
import sys
import argparse
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
import Common as com
import math
import os
from array import array
from ClusterSubmission.Utils import CreateDirectory


class ProcessFraction(object):
    def __init__(self, Options, ana, region, var):
        self.__Options = Options
        self.ana = ana
        self.region = region
        self.var = var
        self.DiscrVar = var.split("_")[0]
        self.Particle = var.split("_")[1]
        self.NFake = var.split("_")[2]
        self.prong = var.split("_")[3] if len(var.split("_")) >= 4 else None
        self.BackgroundHistos = {}
        self.ProcFracHistos = self.CreateProcessFractionHisto()

    def CalculateIntegralAndError(self, histo, ibin):
        integral = 0
        error = 0
        for yi in range(histo.GetNbinsY() + 2):
            integral += histo.GetBinContent(ibin, yi)
            error += histo.GetBinError(ibin, yi) * histo.GetBinError(ibin, yi)
        return [integral, math.sqrt(error)]

    def CreateProcessFractionHisto(self):

        HistoSets = XAMPPplotting.PlottingHistos.CreateHistoSets(self.__Options, self.ana, self.region, self.var, UseData=False)
        if not HistoSets:
            return

        HistoSet = HistoSets[0]
        NormalizationHisto = HistoSet.GetSummedBackground().GetHistogram()

        BackgroundHistos = HistoSet.GetBackgrounds()
        NormalizationHistoClone = NormalizationHisto.Clone()
        for i in range(NormalizationHisto.GetNbinsX() + 2):

            NormalizationFactor = self.CalculateIntegralAndError(NormalizationHisto, i)
            for yi in range(NormalizationHistoClone.GetNbinsY() + 2):

                NormalizationHistoClone.SetBinContent(i, yi, NormalizationFactor[0])
                NormalizationHistoClone.SetBinError(i, yi, NormalizationFactor[1])

        ProcFracHistos = {}
        for histo in BackgroundHistos:
            ProcFracHisto = histo.GetHistogram().Clone()
            keyname = histo.GetName()
            self.BackgroundHistos[keyname] = ProcFracHisto
            ProcFracHisto.Divide(NormalizationHistoClone)

            ProcFracHistos[keyname] = ProcFracHisto

        return ProcFracHistos


class Pair(object):
    def __init__(self, Options, ProcFrac1Fake, ProcFrac2Fake):
        self.__Options = Options

        self.ProcFrac1Fake = ProcFrac1Fake
        self.ProcFrac2Fake = ProcFrac2Fake
        self.ana = self.ProcFrac1Fake.ana
        self.region = self.ProcFrac1Fake.region
        ParticleRaw = self.ProcFrac1Fake.Particle.replace("Fake", "")
        Particle = ParticleRaw.replace("Loose", "")
        self.Particle = Particle
        self.lumi = 0
        self.ProcFracHistos = {}
        self.XaxisTitle = ""

        self.BinDef_1Fake = {
            "Elec": ["LF", "HF", "Conv", "Unmatched"],
            "Muon": ["LF", "HF", "Unmatched"],
            "Tau": ["LF", "HF", "Gluon", "Conv", "Elec", "Unmatched"]
        }
        self.BinDef_2Fake = {
            "Elec": [["LF", "LF"], ["LF", "HF"], ["LF", "Conv"], ["HF", "HF"], ["HF", "Conv"], ["Conv", "Conv"], ["Unmatched",
                                                                                                                  "Unmatched"]],
            "Muon": [["LF", "LF"], ["LF", "HF"], ["HF", "HF"], ["Unmatched", "Unmatched"]],
            "Tau": [["LF", "LF"], ["LF", "HF"], ["LF", "Gluon"], ["LF", "Conv"], ["LF", "Elec"], ["HF", "HF"], ["HF", "Gluon"],
                    ["HF", "Conv"], ["HF", "Elec"], ["Gluon", "Gluon"], ["Gluon", "Conv"], ["Gluon", "Elec"], ["Conv", "Conv"],
                    ["Conv", "Elec"], ["Elec", "Elec"], ["Unmatched", "Unmatched"]]
        }

        self.GetFakeContributions()

        for process, procfrachistos in self.ProcFracHistos.iteritems():
            self.PlotProcessFraction(procfrachistos, process)

    def GetFakeContributions(self):

        HistoSets_1fake = XAMPPplotting.PlottingHistos.CreateHistoSets(self.__Options,
                                                                       self.ProcFrac1Fake.ana,
                                                                       self.ProcFrac1Fake.region,
                                                                       self.ProcFrac1Fake.var,
                                                                       UseData=False)
        if not HistoSets_1fake:
            return
        HistoSet_1fake = HistoSets_1fake[0]
        self.lumi = HistoSet_1fake.GetLumi()
        SummedBackgroundHisto_1fake = HistoSet_1fake.GetSummedBackground().GetHistogram()
        self.XaxisTitle = SummedBackgroundHisto_1fake.GetXaxis().GetTitle()
        BackgroundHistos_1fake = HistoSet_1fake.GetBackgrounds()
        HistoSets_2fake = XAMPPplotting.PlottingHistos.CreateHistoSets(self.__Options,
                                                                       self.ProcFrac1Fake.ana,
                                                                       self.ProcFrac1Fake.region,
                                                                       self.ProcFrac1Fake.var,
                                                                       UseData=False)
        if not HistoSets_2fake:
            return
        HistoSet_2fake = HistoSets_2fake[0]
        SummedBackgroundHisto_2fake = HistoSet_2fake.GetSummedBackground().GetHistogram()
        BackgroundHistos_2fake = HistoSet_2fake.GetBackgrounds()
        for histo_1fake in BackgroundHistos_1fake:
            Process = histo_1fake.GetName()
            histogram_1fake = histo_1fake.GetHistogram()
            for histo_2fake in BackgroundHistos_2fake:
                if not histo_2fake.GetName() == Process:
                    continue
                histogram_2fake = histo_2fake.GetHistogram()
                self.ProcFracHistos[Process] = self.GetFakeContribution(histogram_1fake, histogram_2fake, Process)
        self.ProcFracHistos["SummedBackground"] = self.GetFakeContribution(SummedBackgroundHisto_1fake, SummedBackgroundHisto_2fake,
                                                                           "SummedBackground")

    def GetFakeContribution(self, histo_1fake, histo_2fake, Process):

        NBins = histo_1fake.GetNbinsX()

        BinEdges = []
        for xi in range(histo_1fake.GetNbinsX() + 1):
            BinEdges.append(histo_1fake.GetXaxis().GetBinLowEdge(xi + 1))

        faketypehistos = {}
        for faketype in self.BinDef_1Fake[self.Particle]:
            if self.ProcFrac1Fake.prong:
                faketypehistos[faketype] = ROOT.TH1D(
                    "%s_%s_%s_%s_%s_%s_%s" % (self.ana, self.region, Process, self.ProcFrac1Fake.DiscrVar, self.ProcFrac1Fake.Particle,
                                              self.ProcFrac1Fake.prong, faketype), "", NBins, array("d", BinEdges))
            else:
                faketypehistos[faketype] = ROOT.TH1D(
                    "%s_%s_%s_%s_%s_%s" %
                    (self.ana, self.region, Process, self.ProcFrac1Fake.DiscrVar, self.ProcFrac1Fake.Particle, faketype), "", NBins,
                    array("d", BinEdges))
        if self.ProcFrac1Fake.prong:
            normalizationhisto = ROOT.TH1D(
                "%s_%s_%s_%s_%s_%s_normalization" %
                (self.ana, self.region, Process, self.ProcFrac1Fake.DiscrVar, self.ProcFrac1Fake.Particle, self.ProcFrac1Fake.prong), "",
                NBins, array("d", BinEdges))
        else:
            normalizationhisto = ROOT.TH1D(
                "%s_%s_%s_%s_%s_normalization" % (self.ana, self.region, Process, self.ProcFrac1Fake.DiscrVar, self.ProcFrac1Fake.Particle),
                "", NBins, array("d", BinEdges))

        for xi in range(histo_1fake.GetNbinsX() + 2):
            bincontent = {}  # calculate bincontent and error for each faketype
            binerror = {}
            for yi in range(histo_1fake.GetNbinsY()):  # here we don't need the underflow and overflow bins
                # getbincontent yi + 1
                if yi + 1 <= len(self.BinDef_1Fake[self.Particle]):

                    if not self.BinDef_1Fake[self.Particle][yi] in bincontent:
                        bincontent[self.BinDef_1Fake[self.Particle][yi]] = histo_1fake.GetBinContent(xi, yi + 1)

                    else:
                        bincontent[self.BinDef_1Fake[self.Particle][yi]] += histo_1fake.GetBinContent(xi, yi + 1)
                    if not self.BinDef_1Fake[self.Particle][yi] in binerror:
                        binerror[self.BinDef_1Fake[self.Particle][yi]] = histo_1fake.GetBinError(xi, yi + 1) * histo_1fake.GetBinError(
                            xi, yi + 1)
                    else:
                        binerror[self.BinDef_1Fake[self.Particle][yi]] += histo_1fake.GetBinError(xi, yi + 1) * histo_1fake.GetBinError(
                            xi, yi + 1)

                ## same for events with two fakes
                if yi + 1 <= len(self.BinDef_2Fake[self.Particle]):
                    if not self.BinDef_2Fake[self.Particle][yi][0] in bincontent:
                        bincontent[self.BinDef_2Fake[self.Particle][yi][0]] = histo_2fake.GetBinContent(xi, yi + 1)
                    else:
                        bincontent[self.BinDef_2Fake[self.Particle][yi][0]] += histo_2fake.GetBinContent(xi, yi + 1)
                    if not self.BinDef_2Fake[self.Particle][yi][1] in bincontent:
                        bincontent[self.BinDef_2Fake[self.Particle][yi][1]] = histo_2fake.GetBinContent(xi, yi + 1)
                    else:
                        bincontent[self.BinDef_2Fake[self.Particle][yi][1]] += histo_2fake.GetBinContent(xi, yi + 1)
                    if not self.BinDef_2Fake[self.Particle][yi][0] in binerror:
                        binerror[self.BinDef_2Fake[self.Particle][yi][0]] = histo_2fake.GetBinError(xi, yi + 1) * histo_2fake.GetBinError(
                            xi, yi + 1)
                    else:
                        binerror[self.BinDef_2Fake[self.Particle][yi][0]] += histo_2fake.GetBinError(xi, yi + 1) * histo_2fake.GetBinError(
                            xi, yi + 1)
                    if not self.BinDef_2Fake[self.Particle][yi][1] in binerror:
                        binerror[self.BinDef_2Fake[self.Particle][yi][1]] = histo_2fake.GetBinError(xi, yi + 1) * histo_2fake.GetBinError(
                            xi, yi + 1)
                    else:
                        binerror[self.BinDef_2Fake[self.Particle][yi][1]] += histo_2fake.GetBinError(xi, yi + 1) * histo_2fake.GetBinError(
                            xi, yi + 1)

            for faketype, histo in faketypehistos.iteritems():
                histo.SetBinContent(xi, bincontent[faketype])
                histo.SetBinError(xi, math.sqrt(binerror[faketype]))

        for histo in faketypehistos.values():
            normalizationhisto.Add(histo)
        for histo in faketypehistos.values():
            histo.Divide(normalizationhisto)
        return faketypehistos

    def PlotProcessFraction(self, procfrachistos, process):

        pu = PlotUtils(status=Options.label, size=24, lumi=self.lumi)
        if self.ProcFrac1Fake.prong:
            CanvasName = "ProcessFration_%s_%s_%s_%s_%s_%s" % (self.ana, self.region, process, self.ProcFrac1Fake.DiscrVar,
                                                               self.ProcFrac1Fake.Particle, self.ProcFrac1Fake.prong)
        else:
            CanvasName = "ProcessFration_%s_%s_%s_%s_%s" % (self.ana, self.region, process, self.ProcFrac1Fake.DiscrVar,
                                                            self.ProcFrac1Fake.Particle)
        pu.Prepare1PadCanvas(CanvasName, 800, 600, self.__Options.quadCanvas)
        ProcFracStack = ROOT.THStack("ProcFrac", "")
        legend_x1 = 0.7
        legend_x2 = 0.9
        legend_y1 = 0.6
        legend_y2 = 0.85
        legend = ROOT.TLegend(legend_x1, legend_y1, legend_x2, legend_y2)
        for faketype, histo in procfrachistos.iteritems():
            ProcFracStack.Add(histo)
            if faketype == "HF":
                histo.SetLineColor(ROOT.kRed)
                histo.SetMarkerColor(ROOT.kRed)
                histo.SetFillColor(ROOT.kRed)

            if faketype == "LF":
                histo.SetLineColor(ROOT.kCyan)
                histo.SetMarkerColor(ROOT.kCyan)
                histo.SetFillColor(ROOT.kCyan)

            if faketype == "Gluon":
                histo.SetLineColor(ROOT.kYellow)
                histo.SetMarkerColor(ROOT.kYellow)
                histo.SetFillColor(ROOT.kYellow)

            if faketype == "Elec":
                histo.SetLineColor(ROOT.kBlue)
                histo.SetMarkerColor(ROOT.kBlue)
                histo.SetFillColor(ROOT.kBlue)

            if faketype == "Conv":
                histo.SetLineColor(ROOT.kGreen)
                histo.SetMarkerColor(ROOT.kGreen)
                histo.SetFillColor(ROOT.kGreen)

            if faketype == "Unmatched":
                histo.SetLineColor(ROOT.kMagenta)
                histo.SetMarkerColor(ROOT.kMagenta)
                histo.SetFillColor(ROOT.kMagenta)

            legend.AddEntry(histo, faketype, "f")
        ProcFracStack.Draw("hist")
        ProcFracStack.SetMaximum(2)
        ProcFracStack.GetXaxis().SetTitle(self.XaxisTitle)
        ProcFracStack.GetYaxis().SetTitle("Processfraction")
        legend.SetBorderSize(0)
        legend.Draw()

        pu.saveHisto("%s/%s" % (Options.outputDir, CanvasName), Options.OutFileType)


class ProcessFractions(object):
    def __init__(self, Options):
        self.__Options = Options
        self.__ProcessFractions = self.CalculateProcessFraction()
        self.StoreProcessFraction()

        self.__Pairs = self.FindPairs()

    def CalculateProcessFraction(self):
        ProcessFractions = []
        FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(Options)
        for ana in FileStructure.GetConfigSet().GetAnalyses():

            for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):

                for var in FileStructure.GetConfigSet().GetVariables(ana, region):
                    ProcFrac = ProcessFraction(self.__Options, ana, region, var)
                    if not ProcFrac.ProcFracHistos:
                        continue
                    ProcessFractions.append(ProcFrac)
        return ProcessFractions

    def FindPairs(self):

        Pairs = []
        for ProcFrac_i in self.__ProcessFractions:
            if not ProcFrac_i.NFake == "1fake":
                continue
            for ProcFrac_j in self.__ProcessFractions:

                if not ProcFrac_i.ana == ProcFrac_j.ana:
                    continue
                if not ProcFrac_i.region == ProcFrac_j.region:
                    continue
                if not ProcFrac_i.DiscrVar == ProcFrac_j.DiscrVar:
                    continue
                if not ProcFrac_i.Particle == ProcFrac_j.Particle:
                    continue

                if not ProcFrac_i.prong == ProcFrac_j.prong:
                    continue
                if not ProcFrac_j.NFake == "2fake":
                    continue
                Pairs.append(Pair(self.__Options, ProcFrac_i, ProcFrac_j))

        return Pairs

    def StoreProcessFraction(self):

        ProcessFractionFile = ROOT.TFile("%s/ProcessFraction.root" % (Options.outputDir), "RECREATE")

        for ProcFrac in self.__ProcessFractions:
            for key, histo in ProcFrac.ProcFracHistos.iteritems():

                if ProcFrac.region not in ProcessFractionFile.GetListOfKeys():
                    ProcessFractionFile.mkdir(ProcFrac.region)
                folder = ProcessFractionFile.Get(ProcFrac.region)
                if key not in folder.GetListOfKeys():
                    folder.mkdir(key)
                directoryname = "%s/%s" % (ProcFrac.region, key)
                directory = ProcessFractionFile.GetDirectory(directoryname)
                directory.WriteObject(histo, ProcFrac.var)

        ProcessFractionFile.Close()


if __name__ == "__main__":
    Options = com.GetOptions()
    CreateDirectory(Options.outputDir, False)
    ProcessFractions(Options)
