import ROOT
import sys
import argparse
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
import Common as com
import math
import os
from array import array
from ClusterSubmission.Utils import CreateDirectory

pos_quantity = -1
pos_particle = 0


def get_field_value(varname, field):
    tokens = varname.split("_")

    for i, token in enumerate(tokens):
        if field in token and i != pos_particle and i != pos_quantity:
            return token.replace(field, "")

    print "Warning: not recognised field {} for variable {} ".format(field, varname)
    return "None"


def get_particle(varname):
    return varname.split("_")[pos_particle]


def get_quantity(var):
    return var.split("_")[pos_quantity]


def get_origin(varname):
    return get_field_value(varname, 'orig')


def get_prongs(varname):
    return get_field_value(varname, 'prong')


def get_definition(varname):
    return get_field_value(varname, 'def')


def analysis_is_allowed(analysis):
    return analysis == "FakeFactor_Nominal" or analysis == "FourLepton_Nominal"


def region_is_allowed(region):
    return region in com.analysis_regions


def var_has_any_input_particle(var):
    if var.split("_")[pos_particle] in com.particles:
        return True
    return False


def var_has_any_input_quantity(var):
    if get_quantity(var) in com.fake_quantities:
        return True
    return False


def var_has_any_input_type(var):
    if get_origin(var) in com.fake_types:
        return True
    return False


def var_has_any_prong(var):
    if get_prongs(var) in com.prongs:
        return True
    return False


def var_has_any_definition(var):
    if get_definition(var) in com.definitions:
        return True
    return False


def var_is_inclusive_origin(var):
    return get_origin(var) == "Incl"


# checks if the variable should be considered for the processfraction
def CheckVariable(var):
    if not var_has_any_input_particle(var):
        return False

    if not var_has_any_input_quantity(var):
        return False
    if get_particle(var) == "Tau":
        if not var_has_any_prong(var):
            return False
    if not var_has_any_definition(var):
        return False
    if not var_has_any_input_type(var):
        return False

    if get_origin(var) == "Incl":
        return False
    if get_origin(var) == "Fake":
        return False
    return True


# contains histograms for different fake origins
class ProcessFraction():
    def __init__(self, Options, ana, region, listofvars=None):
        self.__Options = Options
        self.analysis = ana
        self.region = region
        self.particle = get_particle(listofvars[0])
        self.definition = get_definition(listofvars[0])
        self.quantity = get_quantity(listofvars[0])
        if self.particle == "Tau":
            self.prongs = get_prongs(listofvars[0])
        self.__vars = listofvars
        self.__normalizationhistoTemplate = None

        self.__is1D = True

        # self.__histos should contain the histograms accessed by   self.__histos[process][faketype]
        self.__histos = self.GetHistos()
        self.RemoveNegativeEntries()

        self.__normalizationhistos = self.GetNormalizationHisto()
        self.__plotprocessfractionhistos = self.CalculateProcessFraction(True)
        self.processfractionhistos = self.CalculateProcessFraction()

        self.__colorscheme = {
            "LF": ROOT.kCyan,
            "HF": ROOT.kRed,
            "Gluon": ROOT.kYellow,
            "Real": ROOT.kBlue,
            "Conv": ROOT.kGreen,
            "CONV": ROOT.kGreen,
            "Elec": ROOT.kMagenta,
            "Unmatched": ROOT.kMagenta
        }

        # plots should be only created for 1D histos
        if self.__is1D:
            self.PlotProcessFraction()
        else:
            self.Plot2DProcessFraction()

    # reads all histograms and order them by process and faketype
    def GetHistos(self):
        histosperprocess = {}
        for var in self.__vars:
            HistoSets = XAMPPplotting.PlottingHistos.CreateHistoSets(self.__Options, self.analysis, self.region, var, False)
            if not HistoSets:
                continue
            HistoSet = HistoSets[0]

            if not self.__normalizationhistoTemplate:  # getting an empty histogram that can be used to build the normalizationhistogram for each background
                self.__normalizationhistoTemplate = HistoSet.GetSummedBackground().GetHistogram().Clone("Normalizationhisto_template")
                self.__normalizationhistoTemplate.Reset()
            # plots should be only created for 1D histos
            if HistoSet.GetSummedBackground().isTH2() or HistoSet.GetSummedBackground().isTH3() or HistoSet.GetSummedBackground().isTH2Poly(
            ):
                self.__is1D = False
            # ~ histos = [HistoSet.GetSummedBackground()]

            histos = HistoSet.GetBackgrounds()

            faketype = get_origin(var)

            for histo in histos:

                process = histo.GetName()
                # ~ for xi in range(histo.GetHistogram().GetNbinsX()+2):
                # ~ print "overflow bincontent (raw): xi ", histo.GetHistogram().GetBinContent(xi)
                if not process in histosperprocess:
                    histosperprocess[process] = {}
                histosperprocess[process][faketype] = histo

        return histosperprocess

    def RemoveNegativeEntries(self):
        for process, faketypes in self.__histos.items():
            for faketype, samplehisto in faketypes.items():
                histo = samplehisto.GetHistogram()
                for xi in range(histo.GetNbinsX() + 1):
                    if not self.__is1D:
                        for yi in range(histo.GetNbinsY() + 1):
                            if histo.GetBinContent(xi + 1, yi + 1) < 0:
                                # ~ print "removed negative bin for %s" % (faketype)
                                histo.SetBinContent(xi + 1, yi + 1, 0)
                                histo.SetBinError(xi + 1, yi + 1, 0)
                    else:
                        if histo.GetBinContent(xi + 1) < 0:
                            histo.SetBinContent(xi + 1, 0)
                            histo.SetBinError(xi + 1, 0)

    # calculate the normalization histo for each process by summing all faketypes
    def GetNormalizationHisto(self):
        normalizationhistos = {}
        for process, faketypes in self.__histos.items():

            normalizationhisto = self.__normalizationhistoTemplate.Clone(
            )  # clone it to prevent overwriting the histogram for each bkg process
            normalizationhisto.Reset()
            for faketype, samplehisto in faketypes.items():
                histo = samplehisto.GetHistogram().Clone()
                normalizationhisto.Add(histo)
            normalizationhistos[process] = normalizationhisto

        # lets get the normalizationhisto for the sum of bkg. have to get it after removing negative contributions, therefore one cannot use GetSummedBackground
        SumBkgnormalizationhisto = self.__normalizationhistoTemplate.Clone()
        SumBkgnormalizationhisto.Reset()
        for process, normalizationhisto in normalizationhistos.items():
            SumBkgnormalizationhisto.Add(normalizationhisto)
            # ~ for xi in range(normalizationhisto.GetNbinsX()+2):
            # ~ print "bincontent background normalization: ", normalizationhisto.GetBinContent(xi)
        normalizationhistos["SumBG"] = SumBkgnormalizationhisto
        # ~ for xi in range(SumBkgnormalizationhisto.GetNbinsX()+2):
        # ~ print "bincontent summedbackground normalization: ", SumBkgnormalizationhisto.GetBinContent(xi)
        return normalizationhistos

    # calculate processfraction for each process and faketype
    def CalculateProcessFraction(self, forPlots=False):
        processfractionhistos = {}
        bincontent = []

        for process, faketypes in self.__histos.items():
            processfractionhistos[process] = {}
            for faketype, samplehisto in faketypes.items():
                histo = samplehisto.GetHistogram().Clone()
                # for plots the processfraction should be normalized for each process seperately
                # the final values are normalized to the full background
                if forPlots == True:
                    histo.Divide(self.__normalizationhistos[process])
                else:
                    histo.Divide(self.__normalizationhistos["SumBG"])
                    # ~ if self.__is1D:
                    # ~ if len(bincontent) == 0:
                    # ~ for xi in range(histo.GetNbinsX() + 2):
                    # ~ bincontent.append(histo.GetBinContent(xi))
                    # ~ else:
                    # ~ for xi in range(histo.GetNbinsX() + 2):
                    # ~ bincontent[xi] += (histo.GetBinContent(xi))

                processfractionhistos[process][faketype] = histo
        # ~ if forPlots == False:
        # ~ for content in bincontent:
        # ~ print "sum of processfractions: ", content
        return processfractionhistos

    # create plots for each process. is performed only for 1D histos
    def PlotProcessFraction(self):
        for process, faketypes in self.__plotprocessfractionhistos.items():

            pu = PlotUtils(status=self.__Options.label, size=24)
            if self.particle == "Tau":
                CanvasName = "%s_%s_%s%s_%s_%s" % (process, self.region, self.definition, self.particle, self.prongs, self.quantity)
            else:
                CanvasName = "%s_%s_%s%s_%s" % (process, self.region, self.definition, self.particle, self.quantity)
            pu.Prepare1PadCanvas(CanvasName, 800, 600, self.__Options.quadCanvas)
            can = pu.GetCanvas()
            ProcFracStack = ROOT.THStack("ProcFrac_%s" % (CanvasName), "")
            legend_x1 = 0.7
            legend_x2 = 0.9
            legend_y1 = 0.6
            legend_y2 = 0.85
            legend = ROOT.TLegend(legend_x1, legend_y1, legend_x2, legend_y2)
            completeEmptyHistos = True  # if all histograms in the region are empty, they are not plotted
            for faketype, histo in faketypes.items():
                if histo.GetEntries() == 0:  # empty faketypes should not appear in the legend
                    # ~ print "empty histo", faketype
                    continue
                completeEmptyHistos = False
                histo.SetLineColor(self.__colorscheme[faketype])
                histo.SetMarkerColor(self.__colorscheme[faketype])
                histo.SetFillColor(self.__colorscheme[faketype])
                legend.AddEntry(histo, faketype, "f")
                ProcFracStack.Add(histo)
            if completeEmptyHistos:
                continue
            ProcFracStack.Draw("hist")
            ProcFracStack.SetMaximum(1.8)

            XaxisTitle = self.__normalizationhistoTemplate.GetXaxis().GetTitle()
            ProcFracStack.GetXaxis().SetTitle(XaxisTitle)
            ProcFracStack.GetYaxis().SetTitle("Processfraction")
            legend.SetBorderSize(0)
            legend.Draw()

            xCoord = 0.195
            yCoord = 0.83
            if not self.__Options.noATLAS:
                pu.DrawAtlas(xCoord, yCoord)
                yCoord -= 0.08
            pu.DrawRegionLabel(self.analysis, self.region, xCoord, yCoord)
            yCoord -= 0.08
            if self.particle == "Tau":
                pu.DrawTLatex(xCoord, yCoord, "%s %s" % (self.particle, self.prongs))
            else:
                pu.DrawTLatex(xCoord, yCoord, "%s" % (self.particle))
            pu.saveHisto("%s/%s" % (Options.outputDir, CanvasName), Options.OutFileType)

    def Plot2DProcessFraction(self):
        for process, faketypes in self.__plotprocessfractionhistos.items():
            for faketype, histo in faketypes.items():
                if histo.GetEntries() == 0:
                    continue
                pu = PlotUtils(status=self.__Options.label, size=24)
                if self.particle == "Tau":
                    CanvasName = "%s_%s_%s%s_%s_%s_%s" % (process, self.region, self.definition, self.particle, faketype, self.prongs,
                                                          self.quantity)
                else:
                    CanvasName = "%s_%s_%s%s_%s_%s" % (process, self.region, self.definition, self.particle, faketype, self.quantity)
                pu.Prepare1PadCanvas(CanvasName, 800, 600, self.__Options.quadCanvas)
                can = pu.GetCanvas()
                ROOT.gStyle.SetOptStat(0)
                histo.SetTitle("")
                histo.Draw("colz")
                xCoord = 0.195
                yCoord = 0.83
                if not self.__Options.noATLAS:
                    pu.DrawAtlas(xCoord, yCoord)
                    yCoord -= 0.08
                pu.DrawRegionLabel(self.analysis, self.region, xCoord, yCoord)
                yCoord -= 0.08
                if self.particle == "Tau":
                    pu.DrawTLatex(xCoord, yCoord, "%s %s %s" % (self.particle, self.prongs, faketype))
                else:
                    pu.DrawTLatex(xCoord, yCoord, "%s %s" % (self.particle, faketype))
                pu.saveHisto("%s/%s" % (Options.outputDir, CanvasName), Options.OutFileType)


# contains all processfractions for each variable
class ProcessFractions(object):
    def __init__(self, Options):
        self.__Options = Options
        self.__ProcessFractions = self.GetProcessFraction()
        self.__File = None
        print "store Process fractions"
        self.StoreProcessFractions()

    def GetProcessFraction(self):
        ProcessFractions = []
        FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(self.__Options)

        for ana in FileStructure.GetConfigSet().GetAnalyses():

            if not analysis_is_allowed(ana):

                continue
            for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
                if not region_is_allowed(region):
                    continue

                alreadyconsidered = []
                for var in FileStructure.GetConfigSet().GetVariables(ana, region):

                    if not CheckVariable(var):
                        continue
                    particle = get_particle(var)
                    definition = get_definition(var)
                    quantity = get_quantity(var)
                    if get_particle(var) == "Tau":
                        prong = get_prongs(var)
                        fakeoriginindependentvar = [particle, definition, prong, quantity]
                    else:
                        fakeoriginindependentvar = [particle, definition, quantity]

                    if fakeoriginindependentvar in alreadyconsidered:
                        continue
                    alreadyconsidered.append(fakeoriginindependentvar)

                    variablelist = []

                    for var2 in FileStructure.GetConfigSet().GetVariables(ana, region):
                        if not CheckVariable(var2):
                            continue
                        if not get_particle(var2) == get_particle(var):
                            continue
                        if not get_definition(var2) == get_definition(var):
                            continue
                        if not get_quantity(var2) == get_quantity(var):
                            continue
                        if get_particle(var) == "Tau":
                            if not get_prongs(var2) == get_prongs(var):
                                continue
                        variablelist.append(var2)
                    if variablelist:
                        print "Createprocessfraction", variablelist
                        ProcessFractions.append(ProcessFraction(self.__Options, ana, region, variablelist))

        return ProcessFractions

    def CreateOutputFile(self):
        self.__File = ROOT.TFile("%s/ProcessFraction.root" % (self.__Options.outputDir), "RECREATE")
        if not self.__File:
            return False
        #make structure in files: region/bkg/faketype
        for ProcessFraction in self.__ProcessFractions:
            for process, faketypes in ProcessFraction.processfractionhistos.items():
                for faketype in faketypes:
                    if ProcessFraction.region not in self.__File.GetListOfKeys():
                        self.__File.mkdir(ProcessFraction.region)
                    if ProcessFraction.region in self.__File.GetListOfKeys():
                        folder = self.__File.Get(ProcessFraction.region)
                        if process not in folder.GetListOfKeys():
                            folder.mkdir(process)
                        if process in folder.GetListOfKeys():
                            subfolder = folder.Get(process)
                            if faketype not in subfolder.GetListOfKeys():
                                subfolder.mkdir(faketype)

        return True

    def StoreProcessFractions(self):
        if not self.__ProcessFractions:
            print "Error: no Processfraction calculated..."
            return False

        if not self.CreateOutputFile():
            print "Error: Problem while creating files for the process fractions..."
            return False

        for ProcessFraction in self.__ProcessFractions:
            bincontent = []
            for process, faketypes in ProcessFraction.processfractionhistos.items():
                for faketype, histo in faketypes.items():
                    directory = self.__File.GetDirectory("%s/%s/%s" % (ProcessFraction.region, process, faketype))

                    if ProcessFraction.particle == "Tau":
                        histoname = "ProcessFraction_%s%s_orig%s_prong%s_%s" % (ProcessFraction.definition, ProcessFraction.particle,
                                                                                faketype, ProcessFraction.prongs, ProcessFraction.quantity)
                    else:
                        histoname = "ProcessFraction_%s%s_orig%s_%s" % (ProcessFraction.definition, ProcessFraction.particle, faketype,
                                                                        ProcessFraction.quantity)
                    if len(bincontent) == 0:
                        for xi in range(histo.GetNbinsX() + 2):
                            bincontent.append(histo.GetBinContent(xi))
                    else:
                        for xi in range(histo.GetNbinsX() + 2):
                            bincontent[xi] += histo.GetBinContent(xi)

                    directory.WriteObject(histo, histoname)
            # ~ for content in bincontent:
            # ~ print "Stored Processfractionsum: ", content
        self.__File.Close()
        return True


if __name__ == "__main__":
    Options = com.GetOptions()
    CreateDirectory(Options.outputDir, False)
    ProcessFractions(Options)
    print "finished"
