""" @package CalculateFakeFactors

How it works:
=============
Pairs = { Pair, Pair, ...} <==  Pair = { Point(enum), Point(denom) } <== input distribution histograms
|
'-=>  FakeRatios = {FakeRatio, FakeRatio, ..} ==> output ratio histograms

How it runs:
============
python XAMPPplotting/python/CalculateFakeFactors.py  -c XAMPPplotting/python/DSConfigs/FourLeptonSF.py \
 -o FakeFactorFiles \
 --fake-types Incl LF CONV Unmatched \ 
 --fake-quantities eta pt \
 --particles El Tau Mu
 --noSignal --noSyst
"""

##
# lib imports
#
import ROOT
from array import array
import argparse
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
import collections
import Common as com
import math


##
# Class Defaults
# @short Interface class to the input format. The other classes should stay unbroken.
# Formats able to cope with:
# - Tau_defSignal_origCONV_etaIncl_ptIncl_prongIncl_pt
# - Mu_defLoose_origCONV_etaIncl_ptIncl_phi
#
class Defaults(object):
    def __init__(self):
        self.analysis = "SFAnalysis"
        self.conjugates = [("Loose", "Signal")]
        self.definition_enum = "Signal"
        self.origin_inclusive_name = "Incl"
        self.fields = ["def", "orig", "eta", "pt"]
        self.position_particle = 0
        self.position_quantity = -1

    def get_analysis(self):
        return self.analysis

    def get_enum_definition(self):
        return self.definition_enum

    def get_field_value(self, varname, field):
        tokens = varname.split("_")
        pos_quantity = len(varname) - 1 if self.position_quantity == -1 else self.position_quantity
        for i, token in enumerate(tokens):
            if field in token and i != self.position_particle and i != pos_quantity:
                return token.replace(field, "")
        return "None"

    def get_origin(self, varname):
        return self.get_field_value(varname, 'orig')

    def get_definition(self, varname):
        return self.get_field_value(varname, 'def')

    def get_prongs(self, varname):
        if self.get_particle(varname) == "Tau":
            return self.get_field_value(varname, 'prong')
        return None

    def get_quantity(self, varname):
        return varname.split("_")[self.position_quantity]

    def get_particle(self, varname):
        return varname.split("_")[self.position_particle]

    def get_variation(self, varname):
        return "Nominal"  #TBD

    def is_origin_inclusive(self, varname):
        return self.get_field_value(varname, 'orig') == self.origin_inclusive_name

    def is_data_friendly(self, varname):
        return self.is_origin_inclusive(varname)

    def is_mc_friendly(self, varname):
        return True  #always for the time being (excpetions are: data-driven bkg estimates)

    def is_numerator(self, varname):
        return self.get_definition(varname) == self.definition_enum

    def field_is_particle_definition(self, field):
        return ('def' in field)


##
# Class Point
# @brief keeps selected histograms with extended attributes
#
class Point(Defaults, object):
    def __init__(self, _analysis, _region, _variable):
        Defaults.__init__(self)
        self.analysis = _analysis
        self.region = _region  #eg. Z#mu#mu, ttbar, ...
        self.variable = _variable  #eg. SFAnalysis_Nominal_Z#mu#mu_Tau_defLoose_origFake_etaIncl_ptIncl_prong1P_eta
        self.variation = self.get_variation(_variable)
        self.quantity = self.get_quantity(_variable)
        self.particle = self.get_particle(_variable)
        self.definition = self.get_definition(_variable)
        self.origin = self.get_origin(_variable)
        self.prongs = self.get_prongs(_variable)
        self.iam_data_friendly = self.is_data_friendly(_variable)
        self.iam_mc_friendly = self.is_mc_friendly(_variable)
        self.iam_numerator = self.is_numerator(_variable)
        self.me = self.definition
        self.conjugate = "None"
        self.iam_conjugated = self.find_conjugate()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def same(self, point):
        return point.analysis == self.analysis and point.region == self.region and point.variable == self.variable

    def show(self):
        print self.analysis, self.variation, self.region, self.variable

    def stringify(self):
        return "%s_%s_%s_%s: me = %s, conj = %s, ready = %i" % (self.analysis, self.variation, self.region, self.variable, self.me,
                                                                self.conjugate, self.iam_conjugated)

    def find_conjugate(self):
        for c in self.conjugates:
            if self.me in c:
                self.conjugate = c[1] if self.me == c[0] else c[0]
                return True
        print "Warning: Point - Cannot find conjugate for Point ", self.stringify()
        return False

    def conjugates_with(self, point):

        return point.analysis == self.analysis \
            and point.region == self.region \
            and point.quantity == self.quantity \
            and point.particle == self.particle \
            and point.origin == self.origin \
            and point.prongs == self.prongs \
            and point.me == self.conjugate


#Class Pair of Points
class Pair(Defaults, object):
    def __init__(self, _numerator, _denominator):
        Defaults.__init__(self)
        self.numerator = _numerator
        self.denominator = _denominator
        self.analysis = _numerator.analysis
        self.structure = self.build_structure()
        self.status = self.check_status()

    def build_structure(self):
        return "%s_%s/%s/" % (self.numerator.origin, self.numerator.variation, self.numerator.region)

    def check_status(self):
        tokens_num = self.numerator.variable.split("_")
        tokens_den = self.denominator.variable.split("_")

        if len(tokens_num) != len(tokens_den):
            print "Error: Pair - incompatible pair size:"
            self.show()
            return False
        for i, token in enumerate(tokens_num):
            if not self.field_is_particle_definition(token):
                if token != tokens_den[i]:
                    print "Error: Pair - incompatible pair due to wrong field conjugation:"
                    self.show()
                    return False
        return True

    def show(self):
        self.numerator.show()
        self.denominator.show()


##
# Class Pairs
# - selects the desired histograms
# - create Points
# - pairs the points and stores the results into Pair objects
# - keep the yielded Pair onjects
#
class Pairs(Defaults, object):
    def __init__(self, file_structure=None, analyses=[], analysis_regions=[], particles=[], prongs=[], types=[], quantities=[]):
        try:
            Defaults.__init__(self)
        except RuntimeError:
            print "Error: initialization failure of Defaults in Pairs..."
            pass
        self.file_structure = file_structure
        self.analyses = analyses
        self.analysis_regions = analysis_regions
        self.points = []
        self.pairs = []
        self.variations = set(["Nominal"])
        self.regions = set()
        self.particles = set(particles)
        self.prongs = set(prongs)
        self.types = set(types)
        self.quantities = set(quantities)
        self.scanning = self.ScanStructure()
        self.pairing = self.PairVariables()

    @property
    def empty(self):
        return not self.pairs

    def point_exists(self, point):
        return any(point.same(ipoint) for ipoint in self.points)

    def has_any_input_particle(self, var):
        if var.split("_")[self.position_particle] in self.particles:
            return True
        return False

    def has_any_input_prong(self, var):
        value = self.get_field_value(var, "prong")
        if value in self.prongs:
            return True
        return False

    def has_any_input_type(self, var):
        value = self.get_field_value(var, "orig")
        if value in self.types:
            return True
        return False

    def has_any_input_quantity(self, var):
        if var.split("_")[self.position_quantity] in self.quantities:
            return True
        return False

    def analysis_is_allowed(self, analysis):
        return analysis in self.analyses

    def region_is_allowed(self, region):
        return region in self.analysis_regions

    # puts Point entities in a list
    def ScanStructure(self):
        if not self.file_structure:
            sys.exit("Error: Pairs - invalid file structure...")
        else:
            print "Info: file structure is valid in Pairs..."

        if not issubclass(type(self.file_structure), XAMPPplotting.FileStructureHandler.FileStructHandler):
            sys.exit("Error: Pairs - Object is not FileStructure but '{}'...".format(type(self.file_structure)))
        else:
            print "Info: file structure loaded in Pairs..."

        _variations = set()
        _regions = set()
        #FileStructure = XAMPPplotting.FileStructureHandler.GetStructure()
        for ana in self.file_structure.GetConfigSet().GetAnalyses():
            tokens = ana.split("_")
            analysis = tokens[0] if len(tokens) > 0 else "Unknown"
            variation = tokens[1] if len(tokens) > 1 else "Unknown"

            if not self.analysis_is_allowed(analysis):
                print "Info: analysis %s NOT considered in the scan..." % (analysis)
                print "Info: allowed analyses", self.analyses
                continue
            else:
                print "Info: analysis %s considered in the scan..." % (analysis)

            self.regions.update(set(self.file_structure.GetConfigSet().GetAnalysisRegions(ana)))

            #Options.systematics = FileStructure[ana]["Systematics"]

            for region in self.file_structure.GetConfigSet().GetAnalysisRegions(ana):
                if not self.region_is_allowed(region):
                    print "Info: region %s NOT considered in the scan..." % (region)
                    continue
                else:
                    print "Info: region %s considered in the scan..." % (region)

                for var in self.file_structure.GetConfigSet().GetVariables(ana, region):

                    #check particles
                    if not self.has_any_input_particle(var):
                        continue
                    #check prongs
                    if var.split("_")[self.position_particle] == "Tau":
                        if not self.has_any_input_prong(var):
                            continue
                    #check quantity
                    if not self.has_any_input_quantity(var):
                        continue
                    if not self.has_any_input_type(var):
                        continue

                    print "Info: var %s considered in the scan..." % (var)

                    #add now a new Point
                    newpoint = Point(ana, region, var)
                    if not self.point_exists(newpoint):
                        self.points.append(newpoint)
                        #print newpoint.stringify()

        return True

    def pair_exists(self, pair=None):
        for ipair in self.pairs:
            if ipair.numerator == pair.numerator and ipair.denominator == ipair.denominator:
                return True

        return False

    def append_pair(self, pair=None):
        if not self.pair_exists(pair):
            self.pairs.append(pair)

    # puts Pair(Point, Point) objects in a list
    def PairVariables(self):
        if not self.points:
            return False

        for ipoint in self.points:
            for jpoint in self.points:
                if ipoint == jpoint:
                    continue
                if ipoint.conjugates_with(jpoint):
                    if not ipoint.iam_conjugated:
                        print "Warning: Pairs - skipping point because no conjugation was found ", newpoint.stringify()
                    if ipoint.iam_numerator:
                        self.append_pair(Pair(ipoint, jpoint))
                    else:
                        self.append_pair(Pair(jpoint, ipoint))
        return True

    def ShowPairs(self):
        for i in self.pairs:
            i.show()
            print "--"


##
# @class FakeRatio
class FakeRatio(object):
    def __init__(self, options=None, pair=None):
        "Keep list of fake ratios of pair. A pair can appear in many processes"
        self.options = options
        self.pair = pair
        #
        self.verbose = False
        self.structure = pair.structure
        self.folder = pair.structure.split("/")[0] if len(pair.structure.split("/")) >= 1 else "folder"
        self.subfolder = pair.structure.split("/")[1] if len(pair.structure.split("/")) >= 2 else "subfolder"
        self.ratiohistos = []
        self.processes = set([])
        self.__is1D = True
        self.ifhistosets = self.CreateHistoSets()

        self.PlotHisto()

    def is_histogram(self, obj):
        if issubclass(type(obj), ROOT.TObject) and "TH1" in obj.IsA().GetName():
            return True
        if issubclass(type(obj), ROOT.TObject) and "TH2" in obj.IsA().GetName():
            return True
        print "Error: FakeRatio Expecting a TH1 or TH2 object ..."
        return False

    def Append(self, obj):
        if not self.is_histogram(obj):
            return False
        self.ratiohistos.append(obj)
        return True

    def CompareLists(self, l1=[], l2=[]):
        compare = lambda x, y: collections.Counter(x) == collections.Counter(y)
        return compare(l1, l2)

    def CreateHistoSet(self, inpoint, use_data):
        l = XAMPPplotting.PlottingHistos.CreateHistoSets(self.options, inpoint.analysis, inpoint.region, inpoint.variable, use_data)
        return l[0] if l else None

    def consistent_histos(self, h1, h2):

        if h1.GetDimension() != h2.GetDimension():
            print "Error: FakeRatio consistent_histos - different dimensions"
            return False

        if h1.GetNbinsX() != h2.GetNbinsX():
            print "Error: FakeRatio consistent_histos - different bins ", h1.GetNbinsX(), h2.GetNbinsX()
            return False

        if not ROOT.TMath.AreEqualRel(h1.GetXaxis().GetXmin(), h2.GetXaxis().GetXmin(), 1.E-12) or \
                not ROOT.TMath.AreEqualRel(h1.GetXaxis().GetXmax(), h2.GetXaxis().GetXmax(), 1.E-12):
            print "Error: FakeRatio consistent_histos - different axes"
            return False

        return True

    def AddRatio(self, HistoNum=None, HistoDen=None, is_mc=False, Lumi=1.):

        if not HistoNum or not HistoDen:
            print "Error: FakeRatio AddRatio - null input histos"
            return False

        if not self.consistent_histos(HistoNum.GetHistogram(), HistoDen.GetHistogram()):
            print "Error: FakeRatio AddRatio - inconsistent histos"
            return False

        scale = 1. / Lumi if self.options.anti_scale_mc and is_mc else 1.

        proc = HistoNum.GetName()  #given by numerator
        if self.options.performrebinning and (self.pair.numerator.quantity == "pt" or self.pair.numerator.quantity == "ptplot"
                                              or self.pair.numerator.quantity == "pteta" or self.pair.numerator.quantity == "ptetaplot"):
            RebinnedHistos = self.RebinHisto(HistoNum, HistoDen)

            hr = XAMPPplotting.Utils.CreateRatioHisto(RebinnedHistos[0], RebinnedHistos[1], scale)
            #~ hr.SetName("Ratio_" + self.pair.numerator.analysis + "_" + self.pair.numerator.region + "_" + self.pair.numerator.variable)

        else:

            hr = XAMPPplotting.Utils.CreateRatioHisto(HistoNum, HistoDen, scale)  #does the TH1 Divide
        if self.pair.numerator.particle == "Tau":
            hr.SetName(self.pair.numerator.region + "_" + self.pair.numerator.particle + "_orig" + self.pair.numerator.origin + "_prong" +
                       self.pair.numerator.prongs + "_" + self.pair.numerator.quantity)
        else:
            hr.SetName(self.pair.numerator.region + "_" + self.pair.numerator.particle + "_orig" + self.pair.numerator.origin + "_" +
                       self.pair.numerator.quantity)

        hr.SetTitle(proc)

        if not hr:
            print "Error: FakeRatio histo ratio is null for process ", proc
            return False
        else:
            print "Info: FakeRatio created histo ratio for process", proc

        print "Info: FakeRatio appending ratio %s for process %s" % (hr.GetName(), proc)

        return self.Append(hr)

    def GetAllMCHistos(self, Set):
        Hist = [Set.GetSummedBackground()]
        Hist += Set.GetBackgrounds()
        Hist += Set.GetSignals()
        return Hist

    def GetSumBkgMCHistos(self, Set):
        Hist = [Set.GetSummedBackground()]
        return Hist

    def CreateHistoSets(self):
        if not self.pair.status:
            print "Warning: FakeRatio - ratio will not be calculate for Pair due to bad status:"
            self.pair.show()
            return False

        use_data = self.pair.numerator.iam_data_friendly
        use_mc = self.pair.numerator.iam_mc_friendly
        is_mc = self.pair.numerator.iam_mc_friendly
        HistoSetNumerator = self.CreateHistoSet(self.pair.numerator, use_data)  #type: XAMPPplotting.PlottingHistos.HistoSet
        HistoSetDenominator = self.CreateHistoSet(self.pair.denominator, use_data)

        if not HistoSetNumerator or not HistoSetDenominator:
            if self.verbose:
                print "Warning: FakeRatio skipping {} because either enumerator denominator HistoSet is invalid...".format(
                    self.pair.numerator.stringify())
            return False

        if use_data:
            #data
            HistoNum = None
            HistoDen = None
            HistoNum = HistoSetNumerator.GetData()[0] if HistoSetNumerator.GetData() else None  # Note: 0 element of SampleHisto list
            HistoDen = HistoSetDenominator.GetData()[0] if HistoSetDenominator.GetData() else None
            if self.options.background_subtraction and HistoNum.isData():
                for Bkg in HistoSetNumerator.GetBackgrounds():
                    HistoNum.Add(Bkg, -1)
                for Bkg in HistoSetDenominator.GetBackgrounds():
                    HistoDen.Add(Bkg, -1)

            if not self.AddRatio(HistoNum, HistoDen, is_mc):
                print "Error: FakeRatio cannot divide data histo for case:"
                self.pair.show()
                return False
            else:
                if self.verbose:
                    print "Info: FakeRatio CreateHistoSets- Data histo is added: ", use_data, self.pair.numerator.variable, HistoNum.GetName(
                    )
                self.processes.add(HistoNum.GetName())

        if use_mc:
            #sum bkg or all mc
            NumeratorHistos = self.GetSumBkgMCHistos(HistoSetNumerator) if self.options.only_sum_bkg else self.GetAllMCHistos(
                HistoSetNumerator)
            DenominatorHistos = self.GetSumBkgMCHistos(HistoSetDenominator) if self.options.only_sum_bkg else self.GetAllMCHistos(
                HistoSetDenominator)

            if NumeratorHistos[0].isTH2() or NumeratorHistos[0].isTH3() or NumeratorHistos[0].isTH2Poly():
                self.__is1D = False
            #print HistoSetNumerator.GetVariable()
            for i in range(len(NumeratorHistos)):
                HistoNum = NumeratorHistos[i]
                HistoDen = DenominatorHistos[i]
                if not self.AddRatio(HistoNum, HistoDen, True, HistoNum.GetLumi()):
                    print "Error: FakeRatio cannot divide sum bkg histo for case:"
                    self.pair.show()
                    return False
                else:
                    self.processes.add(HistoNum.GetName())
                    if self.verbose:
                        print "Info: FakeRatio CreateHistoSets- Processes is added: data=%i var=%s histo numerator name=%s " % (
                            use_data, self.pair.numerator.variable, HistoNum.GetName())

        print "Info: FakeRatio folder=%s subfolder=%s" % (self.folder, self.subfolder)

        return True

    def RebinHisto(self, NumHisto, DenHisto):
        HistoNum = NumHisto.GetHistogram()
        HistoDen = DenHisto.GetHistogram()

        HistoNumName = NumHisto.GetName(
        ) + "_" + self.pair.numerator.analysis + "_" + self.pair.numerator.region + "_" + self.pair.numerator.variable
        HistoDenName = NumHisto.GetName(
        ) + "_" + self.pair.denominator.analysis + "_" + self.pair.denominator.region + "_" + self.pair.denominator.variable

        ## binning is defined by the numerator since the signal leptons have higher statistical uncertainties
        if NumHisto.isTH2():
            newbins = [HistoNum.GetXaxis().GetBinLowEdge(1)]
            bincontent = [0 for i in range(HistoNum.GetNbinsY())]
            binerror = [0 for i in range(HistoNum.GetNbinsY())]

            for xi in range(HistoNum.GetNbinsX()):
                UncertaintyPassed = True
                for yi in range(HistoNum.GetNbinsY()):
                    bincontent[yi] += HistoNum.GetBinContent(xi + 1, yi + 1)
                    binerror[yi] += HistoNum.GetBinError(xi + 1, yi + 1) * HistoNum.GetBinError(xi + 1, yi + 1)

                    if bincontent[yi] > 0:
                        if math.sqrt(binerror[yi]) / bincontent[yi] > self.options.rebinning_maxUncertainty:
                            UncertaintyPassed = False
                if UncertaintyPassed == True:
                    for yi in range(HistoNum.GetNbinsY()):
                        bincontent[yi] = 0
                        binerror[yi] = 0
                    if not HistoNum.GetXaxis().GetBinLowEdge(xi + 2) in newbins:
                        newbins.append(HistoNum.GetXaxis().GetBinLowEdge(xi + 2))
            if not HistoNum.GetXaxis().GetBinLowEdge(HistoNum.GetNbinsX() + 1) in newbins:
                newbins.append(HistoNum.GetXaxis().GetBinLowEdge(HistoNum.GetNbinsX() + 1))

            RebinnedHistoNum = self.CreateRebinned2DHisto(HistoNum, newbins, HistoNumName)
            RebinnedHistoDen = self.CreateRebinned2DHisto(HistoDen, newbins, HistoDenName)
        else:
            newbins = [HistoNum.GetBinLowEdge(1)]
            bincontent = 0
            binerror = 0
            # to ignore the first empty bins for rebinning (e.g. below pt threshold)
            passedfirstnonemptybin = False
            for i in range(HistoNum.GetNbinsX()):
                if bincontent > 0:
                    passedfirstnonemptybin == True
                    if math.sqrt(binerror) / bincontent < self.options.rebinning_maxUncertainty:
                        bincontent = 0
                        binerror = 0
                        if not HistoNum.GetBinLowEdge(i + 1) in newbins:
                            newbins.append(HistoNum.GetBinLowEdge(i + 1))
                bincontent += HistoNum.GetBinContent(i + 1)
                binerror += HistoNum.GetBinError(i + 1) * HistoNum.GetBinError(i + 1)
                if bincontent == 0 and not i == 0 and passedfirstnonemptybin == False:
                    newbins.append(HistoNum.GetBinLowEdge(i + 1))
            if not HistoNum.GetBinLowEdge(HistoNum.GetNbinsX() + 1) in newbins:
                newbins.append(HistoNum.GetBinLowEdge(HistoNum.GetNbinsX() + 1))
            if self.verbose:
                print "Info: Create rebinned Histo"
                print "Info: original Bin number in x direction: ", HistoNum.GetNbinsX()
                print "Info: target Bin number: ", len(newbins) - 1
                print "Info: new bins in x direction: ", newbins

            RebinnedHistoNum = HistoNum.Rebin(len(newbins) - 1, HistoNumName, array("d", newbins))
            RebinnedHistoDen = HistoDen.Rebin(len(newbins) - 1, HistoDenName, array("d", newbins))
        return [RebinnedHistoNum, RebinnedHistoDen]

    def CreateRebinned2DHisto(self, Histo, newbins, HistoName):
        if self.verbose:
            print "Info: Create rebinned Histo"
            print "Info: original Bin number in x direction: ", Histo.GetNbinsX()
            print "Info: original Bin number in y direction: ", Histo.GetNbinsY()
            print "Info: target Bin number: ", len(newbins) - 1
            print "Info: new bins in x direction: ", newbins

        Ybins = []
        for yi in range(Histo.GetNbinsY() + 1):
            Ybins.append(Histo.GetYaxis().GetBinLowEdge(yi + 1))
        rebinnedHisto = ROOT.TH2D("", "", len(newbins) - 1, array("d", newbins), len(Ybins) - 1, array("d", Ybins))
        xAxisLabel = Histo.GetXaxis().GetTitle()
        yAxisLabel = Histo.GetYaxis().GetTitle()
        rebinnedHisto.GetXaxis().SetTitle(xAxisLabel)
        rebinnedHisto.GetYaxis().SetTitle(yAxisLabel)
        for xi in range(Histo.GetNbinsX() + 2):

            for yi in range(Histo.GetNbinsY() + 2):
                rebinnedHisto.Fill(Histo.GetXaxis().GetBinCenter(xi), Histo.GetYaxis().GetBinCenter(yi), Histo.GetBinContent(xi, yi))

        ## binerrors
        binerrorXY = []
        binerrorY = [0 for i in range(Histo.GetNbinsY() + 2)]
        binindex = 0
        for xi in range(Histo.GetNbinsX() + 2):
            if Histo.GetXaxis().GetBinLowEdge(xi) >= newbins[binindex]:
                binindex += 1
                binerrorXY.append(binerrorY)
                for yi in range(Histo.GetNbinsY() + 2):
                    binerrorY[yi] = 0
            for yi in range(Histo.GetNbinsY() + 2):
                binerrorY[yi] += Histo.GetBinError(xi, yi) * Histo.GetBinError(xi, yi)
            if xi == Histo.GetNbinsX() + 1:
                binerrorXY.append(binerrorY)

        ## fill errors

        for xi in range(rebinnedHisto.GetNbinsX() + 2):
            for yi in range(rebinnedHisto.GetNbinsY() + 2):
                rebinnedHisto.SetBinError(xi, yi, math.sqrt(binerrorXY[xi][yi]))

        return rebinnedHisto

    def PlotHisto(self):
        use_data = self.pair.numerator.iam_data_friendly

        for histo in self.ratiohistos:

            pu = PlotUtils(status=self.options.label, size=24)
            CanvasName = "%s_%s" % (histo.GetTitle(), histo.GetName())
            pu.Prepare1PadCanvas(CanvasName, 800, 600)
            can = pu.GetCanvas()
            #~ pu.DrawPlotLabels(0.195, 0.88, "", "", self.options.noATLAS)

            if self.__is1D:
                histo.SetFillStyle(0)
                histo.SetLineColor(1)
                histo.SetLineWidth(2)
                histo.SetLineStyle(1)
                histo.SetMarkerColor(1)
                histo.GetYaxis().SetTitle("FakeFactor")
                histo.GetYaxis().SetRangeUser(0, 1.8 * histo.GetMaximum())

                histo.Draw("e hist")

            else:

                histo.Draw("colz")
                histo.GetZaxis().SetTitle("FakeFactor")
                can.SetRightMargin(0.18)
            pu.DrawAtlas(0.195, 0.88)
            pu.saveHisto("%s/%s" % (self.options.outputDir, CanvasName), self.options.OutFileType)
            print "OverflowBin: ", histo.GetBinContent(histo.GetNbinsX() + 1)


class ScaleFactor(Defaults, object):
    def __init__(self, options, InputList=[]):
        Defaults.__init__(self)
        self.options = options
        self.Input = InputList
        self.analysis = ""
        self.variation = ""
        self.region = ""
        self.lumi = 0
        self.verbose = False
        self.__newbins = []
        self.__is1D = True

        # RealLeptonContributions are subtracted from data
        #~ self.__RealLeptonNumerator = None
        #~ self.__RealLeptonDenominator = None
        self.__NumeratorToSubstract = None
        self.__DenominatorToSubstract = None

        # order is important. MC has to be substracted from Data
        self.MCNumerator = None
        self.MCDenominator = None
        self.DataNumerator = None
        self.DataDenominator = None
        self.RetrieveHistograms(isData=False)
        self.RetrieveHistograms(isData=True)

        # data before MC, because of rebinning

        self.DataFakeFactor = self.CalculateFakeFactor(self.DataNumerator, self.DataDenominator, isData=True)
        self.MCFakeFactor = self.CalculateFakeFactor(self.MCNumerator, self.MCDenominator, isData=False)

        self.ScaleFactor = self.CalculateScaleFactor()

        self.PlotHistos()

        if self.region == "CRt#bar{t}":
            self.folder = "HF_%s" % (self.variation)
        elif self.region == "Z#mu#mu":
            self.folder = "LF_%s" % (self.variation)
        else:
            print "Warning: Scalefactor in region %s correspond to unknown faketype." % (self.region)
            self.folder = self.region

        self.subfolder = "ScaleFactor"
        self.structure = "%s/%s" % (self.folder, self.subfolder)

    def RetrieveHistograms(self, isData):
        if not "Incl" in self.options.fake_types:
            print "Error: Incl Fake-types missing. Can not calculate Fakefactor for data without"
            return
        if not "Real" in self.options.fake_types:
            print "Warning: Real leptons missing in fake-types."

        Numeratorhist = None
        Denominatorhist = None

        for ipair in self.Input:

            if (isData == False) and (ipair.numerator.origin == "Incl" or ipair.numerator.origin == "Fake"):
                continue

            if (isData == True) and not ipair.numerator.origin == "Incl":
                continue

            HistoSetNumerator = XAMPPplotting.PlottingHistos.CreateHistoSets(self.options, ipair.numerator.analysis, ipair.numerator.region,
                                                                             ipair.numerator.variable, isData)[0]

            if ipair.numerator.origin == "Incl":
                self.lumi = HistoSetNumerator.GetLumi()

            if HistoSetNumerator.GetSummedBackground().isTH2() or HistoSetNumerator.GetSummedBackground().isTH3(
            ) or HistoSetNumerator.GetSummedBackground().isTH2Poly():
                self.__is1D = False

            HistoSetDenominator = XAMPPplotting.PlottingHistos.CreateHistoSets(self.options, ipair.numerator.analysis,
                                                                               ipair.numerator.region, ipair.denominator.variable,
                                                                               isData)[0]

            if (isData == False):
                if self.region == "":
                    self.region = ipair.numerator.region
                if self.region == "CRt#bar{t}":
                    faketype = "HF"
                elif self.region == "Z#mu#mu":
                    faketype = "LF"
                else:
                    faketype = ""
                if not ipair.numerator.origin == faketype:
                    if not self.__NumeratorToSubstract:
                        self.__NumeratorToSubstract = HistoSetNumerator.GetSummedBackground().GetHistogram().Clone()
                    else:
                        self.__NumeratorToSubstract.Add(HistoSetNumerator.GetSummedBackground().GetHistogram().Clone())
                    if not self.__DenominatorToSubstract:
                        self.__DenominatorToSubstract = HistoSetDenominator.GetSummedBackground().GetHistogram().Clone()
                    else:
                        self.__DenominatorToSubstract.Add(HistoSetDenominator.GetSummedBackground().GetHistogram().Clone())

            # real lepton contribution should be substracted from data
            #~ if (isData == False) and (ipair.numerator.origin == "Real"):
            #~ RealLeptonNumeratorhisto = HistoSetNumerator.GetSummedBackground().GetHistogram()
            #~ RealLeptonDenominatorhisto = HistoSetDenominator.GetSummedBackground().GetHistogram()

            #~ self.__RealLeptonNumerator = RealLeptonNumeratorhisto.Clone()
            #~ self.__RealLeptonDenominator = RealLeptonDenominatorhisto.Clone()
            #~ continue

            # data fakefactor

            if isData:
                self.analysis = ipair.numerator.analysis
                self.variation = ipair.numerator.variation

                if self.region == "CRt#bar{t}":
                    faketype = "HF"
                elif self.region == "Z#mu#mu":
                    faketype = "LF"
                else:
                    faketype = ""
                if ipair.numerator.particle == "Tau":
                    self.histoname = "ScaleFactor_%s_orig%s_prong%s_%s" % (ipair.numerator.particle, faketype, ipair.numerator.prongs,
                                                                           ipair.numerator.quantity)

                else:
                    self.histoname = "ScaleFactor_%s_orig%s_%s" % (ipair.numerator.particle, faketype, ipair.numerator.quantity)

                NumeratorHisto = None
                for data in HistoSetNumerator.GetData():

                    if not NumeratorHisto:
                        NumeratorHisto = data.GetHistogram()
                    else:
                        NumeratorHisto.Add(data.GetHistogram())

                DenominatorHisto = None
                for data in HistoSetDenominator.GetData():
                    if not DenominatorHisto:
                        DenominatorHisto = data.GetHistogram()
                    else:
                        DenominatorHisto.Add(data.GetHistogram())

                # substracting the real lepton contribution from data
                if self.__NumeratorToSubstract and self.__DenominatorToSubstract:
                    NumeratorHisto.Add(self.__NumeratorToSubstract, -self.lumi)
                    DenominatorHisto.Add(self.__DenominatorToSubstract, -self.lumi)
                else:
                    print "Warning: no real lepton contribution to substract from data found"
            # MC fakefactor
            else:
                if self.region == "CRt#bar{t}":
                    faketype = "HF"
                elif self.region == "Z#mu#mu":
                    faketype = "LF"
                else:
                    faketype = ""

                if ipair.numerator.origin == faketype:
                    NumeratorHisto = HistoSetNumerator.GetSummedBackground().GetHistogram()
                    DenominatorHisto = HistoSetDenominator.GetSummedBackground().GetHistogram()
                else:
                    continue

            if not Numeratorhist:
                histoname = ipair.numerator.variable
                histoname.replace("orig%s" % (self.get_origin(histoname)), "")
                Numeratorhist = NumeratorHisto.Clone(histoname)
            else:
                Numeratorhist.Add(NumeratorHisto.Clone())

            if not Denominatorhist:
                histoname = ipair.numerator.variable
                histoname.replace("orig%s" % (self.get_origin(histoname)), "")
                Denominatorhist = DenominatorHisto.Clone(histoname)
            else:
                Denominatorhist.Add(DenominatorHisto.Clone())

        if not Numeratorhist or not Denominatorhist:
            print "Error: No Histograms found to calculate the scalefactor"
            return None
        else:
            if isData:
                self.DataNumerator = Numeratorhist
                self.DataDenominator = Denominatorhist
            else:
                self.MCNumerator = Numeratorhist
                self.MCDenominator = Denominatorhist
        # ~ else:
        # ~ # rebin the input histograms starting with MC
        # ~ if self.options.performrebinning:
        # ~ if not isData:
        # ~ # this already consider 1D and 2D histos
        # ~ RebinnedHistos = self.RebinHisto(Numeratorhist, Denominatorhist)
        # ~ else:

        # ~ # here the same binning as montecarlo should be used
        # ~ if self.__is1D:
        # ~ RebinnedHistoNum = Numeratorhist.Rebin(
        # ~ len(self.__newbins) - 1, "%s_num" % (self.histoname), array("d", self.__newbins))
        # ~ RebinnedHistoDen = Denominatorhist.Rebin(
        # ~ len(self.__newbins) - 1, "%s_den" % (self.histoname), array("d", self.__newbins))
        # ~ RebinnedHistos = [RebinnedHistoNum, RebinnedHistoDen]
        # ~ else:
        # ~ RebinnedHistoNum = self.CreateRebinned2DHisto(Numeratorhist, self.__newbins, "%s_num" % (self.histoname))
        # ~ RebinnedHistoDen = self.CreateRebinned2DHisto(Denominatorhist, self.__newbins, "%s_den" % (self.histoname))
        # ~ RebinnedHistos = [RebinnedHistoNum, RebinnedHistoDen]
        # ~ RebinnedHistos[0].Divide(RebinnedHistos[1])
        # ~ return RebinnedHistos[0]
        # ~ else:
        # ~ Numeratorhist.Divide(Denominatorhist)
        # ~ return Numeratorhist

    def CalculateFakeFactor(self, Numeratorhist, Denominatorhist, isData):

        if self.options.performrebinning:
            if isData:
                # this already consider 1D and 2D histos
                RebinnedHistos = self.RebinHisto(Numeratorhist, Denominatorhist)
            else:
                # here the same binning as montecarlo should be used
                if self.__is1D:
                    RebinnedHistoNum = Numeratorhist.Rebin(len(self.__newbins) - 1, "%s_num" % (self.histoname), array("d", self.__newbins))
                    RebinnedHistoDen = Denominatorhist.Rebin(
                        len(self.__newbins) - 1, "%s_den" % (self.histoname), array("d", self.__newbins))
                    RebinnedHistos = [RebinnedHistoNum, RebinnedHistoDen]
                else:
                    RebinnedHistoNum = self.CreateRebinned2DHisto(Numeratorhist, self.__newbins, "%s_num" % (self.histoname))
                    RebinnedHistoDen = self.CreateRebinned2DHisto(Denominatorhist, self.__newbins, "%s_den" % (self.histoname))
                    RebinnedHistos = [RebinnedHistoNum, RebinnedHistoDen]
            RebinnedHistos[0].Divide(RebinnedHistos[1])
            return RebinnedHistos[0]
        else:
            Numeratorhist.Divide(Denominatorhist)
            return Numeratorhist

    def CalculateScaleFactor(self):
        if self.MCFakeFactor and self.DataFakeFactor:
            NumeratorHisto = self.DataFakeFactor.Clone(self.histoname)
            NumeratorHisto.Divide(self.MCFakeFactor)
            return NumeratorHisto
        else:
            print "Warning: Can not calculate scalefactor. Fakefactors are missing"
            return None

    def PlotHistos(self):

        if self.__is1D:
            pu = PlotUtils(status=self.options.label, size=24, lumi=self.lumi)
            CanvasName = self.ScaleFactor.GetName()
            if self.MCFakeFactor.GetEntries() == 0:
                print "Warning: MC Histogram is empty for Scalefactorcalculation"
                return

            if self.DataFakeFactor.GetEntries() == 0:
                print "Warning: Data Histogram is empty for Scalefactorcalculation"
                return
            pu.Prepare2PadCanvas(CanvasName, 800, 600)
            pu.GetTopPad().cd()
            legend_x1 = 0.7
            legend_x2 = 0.9
            legend_y1 = 0.7
            legend_y2 = 0.85
            legend = ROOT.TLegend(legend_x1, legend_y1, legend_x2, legend_y2)
            legend.AddEntry(self.MCFakeFactor, "MC", "l")
            legend.AddEntry(self.DataFakeFactor, "Data", "lp")
            self.MCFakeFactor.SetFillStyle(0)
            self.MCFakeFactor.SetLineColor(2)
            self.MCFakeFactor.SetLineWidth(2)
            self.MCFakeFactor.SetLineStyle(1)
            self.MCFakeFactor.GetYaxis().SetTitle("FakeFactor")
            self.MCFakeFactor.GetYaxis().SetRangeUser(0, 1.8 * self.DataFakeFactor.GetMaximum())

            self.MCFakeFactor.Draw("hist")
            self.DataFakeFactor.SetFillStyle(0)
            self.DataFakeFactor.SetLineColor(1)
            self.DataFakeFactor.SetLineWidth(2)
            self.DataFakeFactor.SetLineStyle(1)
            self.DataFakeFactor.Draw("e same")

            pu.DrawPlotLabels(0.195, 0.83, self.region, self.analysis, self.options.noATLAS)

            legend.SetBorderSize(0)
            legend.Draw()
            # ~ pu.DrawAtlas(0.195, 0.88)
            pu.GetBottomPad().cd()
            pu.GetBottomPad().SetGridy()

            self.ScaleFactor.GetYaxis().SetTitle("Scalefactor")
            self.ScaleFactor.Draw()
            print "OverflowBin: ", self.ScaleFactor.GetBinContent(self.ScaleFactor.GetNbinsX() + 1)

            pu.saveHisto("%s/%s_%s_%s" % (self.options.outputDir, self.analysis, self.region, CanvasName), self.options.OutFileType)

    def RebinHisto(self, HistoNum, HistoDen):

        HistoNumName = HistoNum.GetName()
        HistoDenName = HistoDen.GetName()

        ## binning is defined by the numerator since the signal leptons have higher statistical uncertainties
        if not self.__is1D:
            newbins = [HistoNum.GetXaxis().GetBinLowEdge(1)]
            bincontent = [0 for i in range(HistoNum.GetNbinsY())]
            binerror = [0 for i in range(HistoNum.GetNbinsY())]

            for xi in range(HistoNum.GetNbinsX()):
                UncertaintyPassed = True
                for yi in range(HistoNum.GetNbinsY()):
                    bincontent[yi] += HistoNum.GetBinContent(xi + 1, yi + 1)
                    binerror[yi] += HistoNum.GetBinError(xi + 1, yi + 1) * HistoNum.GetBinError(xi + 1, yi + 1)

                    if bincontent[yi] > 0:
                        if math.sqrt(binerror[yi]) / bincontent[yi] > self.options.rebinning_maxUncertainty:
                            UncertaintyPassed = False

                if UncertaintyPassed == True:
                    for yi in range(HistoNum.GetNbinsY()):
                        bincontent[yi] = 0
                        binerror[yi] = 0
                    if not HistoNum.GetXaxis().GetBinLowEdge(xi + 2) in newbins:
                        newbins.append(HistoNum.GetXaxis().GetBinLowEdge(xi + 2))
            if not HistoNum.GetXaxis().GetBinLowEdge(HistoNum.GetNbinsX() + 1) in newbins:
                newbins.append(HistoNum.GetXaxis().GetBinLowEdge(HistoNum.GetNbinsX() + 1))

            RebinnedHistoNum = self.CreateRebinned2DHisto(HistoNum, newbins, HistoNumName)
            RebinnedHistoDen = self.CreateRebinned2DHisto(HistoDen, newbins, HistoDenName)
        else:
            newbins = [HistoNum.GetBinLowEdge(1)]
            bincontent = 0
            binerror = 0
            # to ignore the first empty bins for rebinning (e.g. below pt threshold)
            passedfirstnonemptybin = False
            for i in range(HistoNum.GetNbinsX()):
                if bincontent > 0:
                    passedfirstnonemptybin = True
                    if math.sqrt(binerror) / bincontent < self.options.rebinning_maxUncertainty:
                        bincontent = 0
                        binerror = 0
                        if not HistoNum.GetBinLowEdge(i + 1) in newbins:
                            newbins.append(HistoNum.GetBinLowEdge(i + 1))
                bincontent += HistoNum.GetBinContent(i + 1)
                binerror += HistoNum.GetBinError(i + 1) * HistoNum.GetBinError(i + 1)
                if bincontent == 0 and not i == 0 and passedfirstnonemptybin == False:
                    newbins.append(HistoNum.GetBinLowEdge(i + 1))

            if not HistoNum.GetBinLowEdge(HistoNum.GetNbinsX() + 1) in newbins:
                newbins.append(HistoNum.GetBinLowEdge(HistoNum.GetNbinsX() + 1))
            if self.verbose:
                print "Info: Create rebinned Histo"
                print "Info: original Bin number in x direction: ", HistoNum.GetNbinsX()
                print "Info: target Bin number: ", len(newbins) - 1
                print "Info: new bins in x direction: ", newbins

            RebinnedHistoNum = HistoNum.Rebin(len(newbins) - 1, HistoNumName, array("d", newbins))
            RebinnedHistoDen = HistoDen.Rebin(len(newbins) - 1, HistoDenName, array("d", newbins))
        self.__newbins = newbins
        return [RebinnedHistoNum, RebinnedHistoDen]

    def CreateRebinned2DHisto(self, Histo, newbins, HistoName):
        if self.verbose:
            print "Info: Create rebinned Histo"
            print "Info: original Bin number in x direction: ", Histo.GetNbinsX()
            print "Info: original Bin number in y direction: ", Histo.GetNbinsY()
            print "Info: target Bin number: ", len(newbins) - 1
            print "Info: new bins in x direction: ", newbins

        Ybins = []
        for yi in range(Histo.GetNbinsY() + 1):
            Ybins.append(Histo.GetYaxis().GetBinLowEdge(yi + 1))
        rebinnedHisto = ROOT.TH2D("", "", len(newbins) - 1, array("d", newbins), len(Ybins) - 1, array("d", Ybins))
        for xi in range(Histo.GetNbinsX() + 2):

            for yi in range(Histo.GetNbinsY() + 2):
                rebinnedHisto.Fill(Histo.GetXaxis().GetBinCenter(xi), Histo.GetYaxis().GetBinCenter(yi), Histo.GetBinContent(xi, yi))

        ## binerrors
        binerrorXY = []
        binerrorY = [0 for i in range(Histo.GetNbinsY() + 2)]
        binindex = 0
        for xi in range(Histo.GetNbinsX() + 2):
            if Histo.GetXaxis().GetBinLowEdge(xi) >= newbins[binindex]:
                binindex += 1
                binerrorXY.append(binerrorY)
                for yi in range(Histo.GetNbinsY() + 2):
                    binerrorY[yi] = 0
            for yi in range(Histo.GetNbinsY() + 2):
                binerrorY[yi] += Histo.GetBinError(xi, yi) * Histo.GetBinError(xi, yi)
            if xi == Histo.GetNbinsX() + 1:
                binerrorXY.append(binerrorY)

        ## fill errors

        for xi in range(rebinnedHisto.GetNbinsX() + 2):
            for yi in range(rebinnedHisto.GetNbinsY() + 2):
                rebinnedHisto.SetBinError(xi, yi, math.sqrt(binerrorXY[xi][yi]))

        return rebinnedHisto


##
# @class FakeRatios
#
class FakeRatios(Defaults, object):
    def __init__(self, options=None, pairs=None):
        """
        Keep FakeRatio objects together and store them in persiting root files
        """
        try:
            Defaults.__init__(self)
        except RuntimeError:
            print "Error: initialization failure of Defaults in FakeRations..."
            pass
        #
        self.__options = options
        self.__pairs = pairs
        #
        self.__verbose = False
        self.__fake_ratios = []
        self.__files = {}
        self.__written = []
        if self.__options.CalculateFakefactor:
            self.__ready = self.__add_pairs()
        if self.__options.CalculateScalefactor:
            self.__scalefactors = []
            self.__add_scalefactors
            self.__ScaleFactorFile = None

    @property
    def __add_scalefactors(self):
        scalefactorvars = []

        for ipair in self.__pairs.pairs:

            if "%s/%s/%s" % (ipair.numerator.analysis, ipair.numerator.region, ipair.numerator.variable) in scalefactorvars:
                continue

            SFInput = []
            for jpair in self.__pairs.pairs:

                if jpair.numerator.analysis == ipair.numerator.analysis \
                 and jpair.numerator.region == ipair.numerator.region \
                 and jpair.numerator.variation == ipair.numerator.variation \
                 and jpair.numerator.quantity == ipair.numerator.quantity \
                 and jpair.numerator.particle == ipair.numerator.particle \
                 and jpair.numerator.definition == ipair.numerator.definition \
                 and jpair.numerator.prongs == ipair.numerator.prongs:

                    scalefactorvars.append("%s/%s/%s" % (jpair.numerator.analysis, jpair.numerator.region, jpair.numerator.variable))

                    SFInput.append(jpair)

            self.__scalefactors.append(ScaleFactor(self.__options, SFInput))

    def StoreScaleFactors(self):
        self.__ScaleFactorFile = ROOT.TFile("%s/ScaleFactor.root" % (self.__options.outputDir), "RECREATE")
        for sf in self.__scalefactors:

            if sf.folder not in self.__ScaleFactorFile.GetListOfKeys():
                self.__ScaleFactorFile.mkdir(sf.folder)
            if sf.folder in self.__ScaleFactorFile.GetListOfKeys():
                folder = self.__ScaleFactorFile.Get(sf.folder)
                if sf.subfolder not in folder.GetListOfKeys():
                    folder.mkdir(sf.subfolder)
            directory = self.__ScaleFactorFile.GetDirectory(sf.structure)

            histoname = sf.histoname
            directory.WriteObject(sf.ScaleFactor, histoname)
            print "store: %s" % (histoname)
        self.__ScaleFactorFile.Close()
        return True

    def empty(self):
        return not self.__fake_ratios

    def __add(self, _fake_ratio):
        self.__fake_ratios.append(_fake_ratio)

    def __add_pairs(self):
        if not self.__pairs:
            return False
        for ipair in self.__pairs.pairs:
            print "\nInfo: New fake ratio..."
            self.__add(FakeRatio(self.__options, ipair))
            if self.__verbose:
                print "Info: FakeRatios fake ratio is added for:"
                ipair.show()
        return True

    def __book_output_files(self, fakeratio):

        for proc in set(fakeratio.processes):
            if proc not in self.__files:
                self.__files[proc] = ROOT.TFile(
                    "%s/%s" % (self.__options.outputDir, self.__options.outFile.replace(".root", "_%s.root" % (proc))), "RECREATE")

                if self.__verbose:
                    print "Info: __book_output_files - outputfile {} is booked...".format(self.__files[proc].GetName())

        return True

    def __create_output_files(self):

        if not self.__fake_ratios:
            print "Warning: No fake ratios found -- output files cannot be created ..."
            return False

        for fr in self.__fake_ratios:

            #book files
            if not self.__book_output_files(fr):
                print "Warning: create_output_files - couldn't book file for fake ratio:"
                fr.pair.show()
                continue

            #make structure in files
            for proc in fr.processes:
                #create folder
                if fr.folder not in self.__files[proc].GetListOfKeys():
                    self.__files[proc].mkdir(fr.folder)

            #create subfolder
                if fr.folder in self.__files[proc].GetListOfKeys():
                    folder = self.__files[proc].Get(fr.folder)
                    if fr.subfolder not in folder.GetListOfKeys():
                        folder.mkdir(fr.subfolder)

        return True

    def __close_output_files(self):
        for f in self.__files.itervalues():
            f.Close()
        return True

    def __is_directory(self, obj):
        return issubclass(type(obj), ROOT.TObject) and "TDirectoryFile" in obj.IsA().GetName()

    ## shorten the histo name
    # eg.  Nom_ZmumuSFAnalysisZmumuTau_defSignal_origIncl_etaIncl_ptIncl_prong3P_pt --> Ratio_Nom_anaZmumu_partTau_defSignal_origIncl_etaIncl_ptIncl_prong3P_pt
    # @param self The object pointer
    # @param _histo the TH1 object
    def __reform_histo_name(self, _histo):
        obj = _histo.GetName()
        print "\tName bef: ", obj
        for particle in self.__pairs.particles:
            if particle in obj:
                obj = obj.replace(particle, "_part" + particle)

        #obj = obj.replace("__", "_") #get rid of double _ in histo names
        #obj = obj.replace(" ", "") #get rid of spaces
        obj = obj.replace("#", "")  #get rid of # in histo names

        for p in self.__fake_ratios[0].processes:
            if p in obj:
                if p == "SumBG" or "Data":
                    obj = obj.replace("_" + p, "")
                else:
                    obj = obj.replace("_" + p, "_proc" + p)  # this only for bkgs: XAMPPplotting/python/DataMCPlots.py will complain
        print "\tName aft: ", obj
        return obj

    def __histo_has_a_booked_process(self, fr, h):
        for proc in fr.processes:
            if proc in h.GetTitle():
                print "Info: histo %s of process %s is associated to process %s" % (h.GetName(), h.GetTitle(), proc)
                return True

        print "Warning: histo %s with process %s is ownerless, cannot be assinged to any known process " % (h.GetName(), h.GetTitle())
        print "Processes:", fr.processes
        return False

    def __to_be_stored(self, name):
        #self.options
        return True

    def __to_be_duplicated(self, name):
        origin = self.get_origin(name)
        return origin in self.__options.fake_type_dupes_source

    def persistify(self):
        #check status of pairs
        if not self.__ready:
            print "Error: no pairs of fake ratios registered..."
            return False

        #prepare output file
        print "\nInfo: FakeRatios preparing output files..."
        if not self.__create_output_files():
            print "Error: Problem while creating files for the fake ratios..."
            return False

        #loop over all fake ratios
        print "\nInfo: FakeRatios - loop over %i FRs..." % (len(self.__fake_ratios))
        for fr in self.__fake_ratios:
            analysis = fr.pair.numerator.analysis
            print "Info: FakeRatios - considering analysis %s" % (analysis)
            #loop over histograms per fake ratio object
            for histo in fr.ratiohistos:
                if not self.__histo_has_a_booked_process(fr, histo):
                    sys.exit("Error: cannot handle histo %s" % (histo.GetName()))
                #loop over process/output files to identify the best place for this histo
                for proc in self.__files.iterkeys():
                    # go to the position
                    #~ if not analysis in histo.GetName():
                    #~ continue
                    if not proc in histo.GetTitle():
                        continue
                    print "\nInfo:  FakeRatios analysis %s, process %s associated to histo %s of process %s" % (
                        analysis, proc, histo.GetName(), histo.GetTitle())

                    # get right directory
                    directory = self.__files[proc].GetDirectory(fr.structure)
                    if self.__is_directory(directory):
                        if self.__verbose:
                            print "Info: and for this process %s it goes into structure %s" % (proc, fr.structure)
                            print "Info: persistify histogram '{}' process '{}' analysis '{}' in dir '{}'".format(
                                histo.GetName(), proc, analysis, directory.GetName())

                        hname = self.__reform_histo_name(histo)
                        #~ hname = histo.GetName()
                        hname_key = histo.GetName()
                        print "Storing", hname, hname_key
                        if self.__to_be_stored(hname):
                            if not hname_key in self.__written:
                                print "Info: persistify - writing original: %s" % (hname)
                                directory.WriteObject(histo, hname_key)
                                self.__written.append(hname)
                            else:
                                print "Warning: persistify - skipping already written: %s" % (hname)

                        if self.__to_be_duplicated(hname):
                            for origin in self.__options.fake_type_dupes_target:
                                hname_dupe = hname.replace(self.__options.fake_type_dupes_source, origin)
                                directory.WriteObject(histo, hname_dupe)
                                print "Info: persistify - duplicating original %s to %s" % (hname, hname_dupe)
                    else:
                        print "Error: not a root directory ", directory

        if not self.__close_output_files():
            print "Error: Problem while closing the files the fake ratios..."
            return False

        return True


# main
def main(argv):

    Options = com.GetOptions()

    if not com.SetOutputFolder(Options):
        sys.exit("Error: Exiting at SetOutputFolder ...")

    if not com.SetRoot():
        sys.exit("Error: Exiting at SetRoot ...")

    #create a set of Pairs
    cPairs = Pairs(file_structure=com.FileStructure(Options),
                   analyses=com.analyses,
                   analysis_regions=com.analysis_regions,
                   particles=com.particles,
                   prongs=com.prongs,
                   types=com.fake_types,
                   quantities=com.fake_quantities)

    if cPairs.empty:
        sys.exit("Warning: no pairs were provided and nothing will be processed...")
    else:
        cPairs.ShowPairs()

    #compute ratios and dump results into a root file
    print "\nInfo: Fake ratios..."
    cRatios = FakeRatios(options=Options, pairs=cPairs)

    if Options.CalculateFakefactor:
        if cRatios.empty():
            sys.exit("Warning: no ratios were provided and nothing will be stored...")
        print "\nInfo: Storing fake ratios..."
        if not cRatios.persistify():
            sys.exit("Error: Exiting when storing the fake ratios ...")

    if Options.CalculateScalefactor:
        if not cRatios.StoreScaleFactors():
            sys.exit("Error: Exiting when storing the scalefactors ...")

    print "Info: Done!"


# run main
if __name__ == "__main__":

    main(sys.argv[1:])
