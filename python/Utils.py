#! /usr/bin/env python
from ClusterSubmission.Utils import FillWhiteSpaces, id_generator
from XAMPPplotting.Defs import *
import logging, math, ROOT


def setupBaseParser(parser):
    parser.add_argument('-v', '--var', help='Specify variable to be plotted, otherwise plot all', nargs='+', default=[])
    parser.add_argument(
        '--analysis',
        '-a',
        help=
        'Specify the analysis to be plotted, otherwise plot all (It is stronly recommended to specify your analysis for preventing problems in reading the systematic uncertainties.)',
        nargs='+',
        default=[])
    parser.add_argument('-c', '--config', "--dsconfig", help='Specify a config file containing the samples to plot', nargs='+', default=[])
    parser.add_argument('-r', '--regions', help='Specify the analysis to be plotted, otherwise plot all', nargs='+', default=[])
    parser.add_argument('-s',
                        '--systematics',
                        help="Specify a list of systematics if not all of them shall be drawn",
                        nargs='+',
                        default=[])
    parser.add_argument('--ExcludeSyst', help="Specify a list of systematics to be excluded from the plot", nargs='+', default=[])
    parser.add_argument('--UpperPadRanges', help="Specify upper and lower range value of top pad", nargs=2, default=[], type=float)
    parser.add_argument('--LowerPadRanges', help="Specify upper and lower range value of bottom pad", nargs=2, default=[], type=float)
    parser.add_argument('-o', '--outputDir', help='Specify an output folder where the final histograms get stored', default='Plots')
    parser.add_argument('-f', '--OutFileType', help='specify filetypes of created plots', nargs='+', default=["eps"])
    parser.add_argument('--noSyst', help='do not draw systematic uncertainties', action='store_true', default=False)
    parser.add_argument('--noUpperPanelSyst',
                        help='do not draw systematic uncertainties in upper panel',
                        action='store_true',
                        default=False)
    parser.add_argument('--noRatioSysLegend',
                        help='do not draw legend in lower pad explaining the stat+sys errors',
                        action='store_true',
                        default=False)
    parser.add_argument('--noSignal', help='do not draw signal MC', action='store_true', default=False)
    parser.add_argument('--noSignalStack',
                        help='Does not draw the signals stacked on top of the background',
                        action='store_true',
                        default=False)
    parser.add_argument('--doLogY', help='draw logarithmic y-axis', action='store_true', default=False)
    parser.add_argument('--doLogX', help='draw logarithmic x-axis', action='store_true', default=False)
    parser.add_argument('--label', help='Specify the label to be plotted', default="Internal")
    parser.add_argument('--regionLabel', help='Specify a region label to be drawn', default='')
    parser.add_argument('--noATLAS', help='Disable the drawing of the ATLAS Internal label', action='store_true', default=False)
    parser.add_argument('--LegendXCoords', help="Specify x1 and x2 for the legend coordinates", nargs=2, default=[0.5, 0.97], type=float)
    parser.add_argument('--LegendYCoords', help="Specify y1 and y2 for the legend coordinates", nargs=2, default=[0.45, 0.9], type=float)
    parser.add_argument('--LegendTextSize', help="Legend text size", default=22, type=float)
    parser.add_argument('--YaxisRange', help="Specify y1 and y2 for the y-axis", nargs=2, default=[-1., -1.], type=float)
    parser.add_argument('--EntriesPerLegendColumn', help="Specify number of entries drawn per column in legend", default=6, type=int)

    parser.add_argument('--nominalName', help='Specify the name of the nominal (systematic) tree to be drawn', default='Nominal')
    parser.add_argument('--noRatio', help='do not draw ratio panel in 1D histograms', action='store_true', default=False)
    parser.add_argument('--printYields', help='printout event yields', action='store_true', default=False)

    parser.add_argument('--skipTH1', help='Leave out to plot 1D histograms', action='store_true', default=False)
    parser.add_argument('--skipTH2', help='Leave out to plot 2D histograms', action='store_true', default=False)

    parser.add_argument('--quadCanvas', help='Use 600x600 canvases instead of 800x600 one', action='store_true', default=False)

    parser.add_argument('-l', '--lumi', help='scale MC samples to a specific luminosity', default=1., type=float)
    parser.add_argument('--lumi-label', help='Preferred luminosity label', type=str, default="")

    parser.add_argument('--store-yields-outfile', help='Basename of file which keeps the yields', default='yields')
    parser.add_argument('--store-yields-raw', help='Store the yields in a text file without any format', action='store_true', default=False)
    parser.add_argument('--store-yields-latex',
                        help='Store the yields in a text file using a LaTeX format',
                        action='store_true',
                        default=False)
    parser.add_argument('--store-yields-hbins',
                        help='Store the yields in a text file as histogram bins. To be used by WriteValuesIntoTH1.py',
                        action='store_true',
                        default=False)
    parser.add_argument('--store-yields-histo', help='Store the yields as histograms in root files.', action='store_true', default=False)
    parser.add_argument('--store-yields-store-ratios', help='Store histogram ratios as yields', action='store_true', default=False)
    parser.add_argument('--store-yields-store-values', help='Store histogram content values as yields', action='store_true', default=False)
    parser.add_argument("--store-yields-dupes-source",
                        help='Duplicate this yield with this specific string key (source)',
                        default="",
                        type=str)
    parser.add_argument('--store-yields-dupes-target', help='Duplicated yields with this key. (targets) ', type=str, nargs='+')
    parser.add_argument("--summaryPlotOnly",
                        help="Skips the creation of the particular PDFs, but only the All* files are made",
                        default=False,
                        action="store_true")
    parser.add_argument("--show2DText", help="Writes the numbers in each TH2 histogram", default=False, action="store_true")
    parser.add_argument("--skipBkgComposition2D",
                        help="Only plot the SumBG, signal and the data in 2D.",
                        default=False,
                        action="store_true")

    return parser


def store_yields(PlottingOptions):
    if PlottingOptions.store_yields_raw or PlottingOptions.store_yields_latex:
        return True
    if PlottingOptions.store_yields_hbins or PlottingOptions.store_yields_histo:
        return True
    return False


# this funtion returns a dictionary of the form:
# <Name of DSConfig> : { "Data" : {Data samples}, "Signal" : {Signal samples}, "Background" : {Background samples} ]
# where {Data samples}, {Signal samples}, {Background samples} are dictionnaries containing the samples to plot as itervalues and their names as iterkeys
def IsItemInListOfKeys(TObject, Item):
    try:
        for Key in TObject.GetListOfKeys():
            if Item == Key.GetName():
                return True
    except:
        logging.error("could not call GetListOf Keys on object " + str(TObject))
    return False


def GetPlotRangeX(histo):
    if not histo: return 0
    return histo.GetXaxis().GetBinUpEdge(histo.GetNbinsX()) - histo.GetXaxis().GetBinLowEdge(1)


def GetPlotRangeY(histo):
    if not histo: return 0
    return histo.GetYaxis().GetBinUpEdge(histo.GetNbinsY()) - histo.GetYaxis().GetBinLowEdge(1)


def getSamplesToUse(DSconfigs):
    SamplesToUse = {}
    # this is for using customized DSConfig files
    if len(DSconfigs) > 0:
        modnames = DSconfigs
        finalModnames = []  # this lists only contains the real python imports (without paths)
        for modname in modnames:
            modpath = '/'.join(modname.split('/')[:-1])  # check if modname is python module only or contains a path
            if modpath != '':
                sys.path.insert(0, modpath)
                name = modname.split('/')[-1].replace('.py', '')  # python module names get imported without '.py'
                if not name in finalModnames:
                    exec('import %s' % name)
                    finalModnames.append(name)
            else:
                sys.path.insert(0, os.getcwd())
                name = modname.replace('.py', '')  # python module names get imported without '.py'
                if not name in finalModnames:
                    exec('import %s' % name)
                    finalModnames.append(name)

        AllInputDS = {}
        for modname in finalModnames:
            AllInputDS[modname] = []
            try:
                mod = sys.modules[modname]
            except:
                mod = sys.modules["XAMPPplotting." + modname]
            for k in mod.__dict__:
                if k[:2] != '__':
                    AllInputDS[modname].append(mod.__dict__[k])

        ModToConfigNames = {}
        ModToRefSampleNames = {}
        for modname in AllInputDS.iterkeys():
            ModToRefSampleNames[modname] = ""
            ModToConfigNames[modname] = modname
            for smp in AllInputDS[modname]:
                try:
                    mod = sys.modules[modname]
                except:
                    mod = sys.modules["XAMPPplotting." + modname]

                if isinstance(smp, mod.DSConfigName):
                    ModToConfigNames[modname] = smp.Name
                    ModToRefSampleNames[modname] = smp.Reference
                    break

        for modname in AllInputDS.iterkeys():
            SamplesToUse[modname] = {}
            SamplesToUse[modname]["Name"] = ModToConfigNames[modname]
            SamplesToUse[modname]["RefSample"] = ModToRefSampleNames[modname]

            SamplesToUse[modname]["Configs"] = []
            for smp in AllInputDS[modname]:
                try:
                    mod = sys.modules[modname]
                except:
                    mod = sys.modules["XAMPPplotting." + modname]
                if isinstance(smp, mod.DSconfig):
                    SamplesToUse[modname]["Configs"].append(smp)
            logging.info('Having found the following inputs in %s:' % modname)
            for smp in AllInputDS[modname]:
                try:
                    mod = sys.modules[modname]
                except:
                    mod = sys.modules["XAMPPplotting." + modname]
                if isinstance(smp, mod.DSconfig):
                    logging.info('  --- %s' % smp.Name)
    return SamplesToUse


#This function creates from SampleHisto objects ratio TH1 histograms
def CreateRatioHisto(Numerator=None, Denominator=None, Scale=1.):
    if not Numerator or not Denominator: return None
    try:
        Num = Numerator.GetHistogram().Clone("Ratio_" + Numerator.GetAnalysis() + "_" + Numerator.GetRegion() + "_" +
                                             Numerator.GetVariableName())
    except:
        Num = Numerator.Clone("Ratio_" + Numerator.GetName())

    try:
        Num.Divide(Denominator.GetHistogram().get())
    except:
        Num.Divide(Denominator)
    if Scale != 1.:
        Num.Scale(Scale)
    Num.SetDirectory(0)
    return Num


#
def CreateRatioHistos(Numerators, Denominator, StoreRatios=False, Path=""):
    Ratios = []
    for N in Numerators:
        if N == Denominator: continue
        Ratios.append(CreateRatioHisto(N, Denominator))
    return Ratios


def GetHorizontalLine(Template, yValue, Name, LineStyle):
    Histo = None
    try:
        Histo = Template.GetHistogram().Clone(Name + Template.GetHistogram().GetName())
    except:
        Histo = Template.Clone(Name + Template.GetName())
    Histo.Reset()
    Histo.SetLineWidth(1)
    Histo.SetLineColor(1)
    Histo.SetLineStyle(LineStyle)
    Histo.SetFillStyle(0)
    for bin in range(0, Histo.GetNbinsX() + 2):
        Histo.SetBinContent(bin, yValue)
    return Histo


def GetSystematicList(File, Analysis, Nominal, Exclude=[]):
    Syst = []
    for key in File.GetListOfKeys():
        KeyName = key.GetName()
        if Analysis not in KeyName or Nominal in KeyName:
            continue
        SysSplit = KeyName.split('%s_' % Analysis)
        if not SysSplit[1] in Exclude:
            Syst.append(SysSplit[1])
    return Syst


def longestSubstringFinder(string1, string2):
    answer = ""
    len1, len2 = len(string1), len(string2)
    for i in range(len1):
        match = ""
        for j in range(len2):
            if (i + j < len1 and string1[i + j] == string2[j]):
                match += string2[j]
            else:
                if (len(match) > len(answer)): answer = match
                match = ""
    return answer


def CheckHisto(histo):
    if histo == None:
        return False
    if math.isnan(histo.Integral()):
        return False
    if histo.Integral() == 0.:
        logging.info('Histogram %s has 0 entries, skipping...' % (histo.GetName()))
        return False
    elif histo.Integral() < 0.:
        logging.info('Histogram %s has %i entries, skipping...' % (histo.GetName(), histo.Integral()))
        return False
    return True


def IntegrateAndErrorTH1(Histo, BinStart=0, BinEnd=-1):
    return IntegrateAndError(Histo, xStart=BinStart, xEnd=BinEnd)


def IntegrateAndError(Histo, xStart=1, xEnd=-1, yStart=1, yEnd=-1, zStart=1, zEnd=-1):
    if not Histo:
        return 1.e25, 1.e25
    integral = 0.
    error = 0.
    if xEnd == -1: xEnd = Histo.GetNbinsX() + 2
    if yEnd == -1 and Histo.GetDimension() >= 2: yEnd = Histo.GetNbinsY() + 2
    if zEnd == -1 and Histo.GetDimension() == 3: zEnd = Histo.GetNbinsZ() + 2

    for x in range(xStart, xEnd):
        if Histo.GetDimension() == 1:
            norm = GetInverseBinNormalization(Histo, x)
            integral += Histo.GetBinContent(x) * norm
            error += math.pow(Histo.GetBinError(x) * norm, 2)
        else:
            for y in range(yStart, yEnd):
                if Histo.GetDimension() == 2:
                    norm = GetInverseBinNormalization(Histo, Histo.GetBin(x, y))
                    integral += Histo.GetBinContent(x, y) * norm
                    error += math.pow(Histo.GetBinError(x, y) * norm, 2)
                else:
                    for z in range(zStart, zEnd):
                        norm = GetInverseBinNormalization(Histo, Histo.GetBin(x, y, z))
                        integral += Histo.GetBinContent(x, y, z) * norm
                        error += math.pow(Histo.GetBinError(x, y, z) * norm, 2)
    return integral, math.sqrt(error)


def IntegrateTH1(Histo, BinStart=0, BinEnd=-1):
    return IntegrateAndErrorTH1(Histo, BinStart, BinEnd)[0]


def IntegrateTH2(Histo, StartX=0, EndX=-1, StartY=0, EndY=-1):
    return IntegrateAndError(Histo, xStart=StartX, xEnd=EndX, yStart=StartY, yEnd=EndY)[0]


def GetNbins(histo):
    nbins = 0
    if histo.InheritsFrom("TH2Poly"):
        nbins = histo.GetNumberOfBins() + 1
    elif histo.InheritsFrom("TGraphAsymmErrors"):
        nbins = histo.GetN()
    elif histo.GetDimension() == 3:
        nbins = (histo.GetNbinsX() + 2) * (histo.GetNbinsY() + 2) * (histo.GetNbinsZ() + 2)
    elif histo.GetDimension() == 2:
        nbins = (histo.GetNbinsX() + 2) * (histo.GetNbinsY() + 2)
    elif histo.GetDimension() == 1:
        nbins = histo.GetNbinsX() + 2
    else:
        logging.error("GetNbins(): no supported histogram type!")
        return -1

    return nbins


def DifferHistos(First, Second):
    BinsSecond = 0
    BinsFirst = 0
    try:
        BinsFirst = First.GetNbins()
    except:
        BinsFirst = GetNbins(First)
    try:
        BinsSecond = Second.GetNbins()
    except:
        BinsSecond = GetNbins(Second)
    if BinsFirst != BinsSecond:
        return True
    for i in range(0, BinsFirst):
        if abs(First.GetBinContent(i) - Second.GetBinContent(i)) > 1.e-12:
            return True
    return False


def GetMaximumWidth(str, Max):
    if len(str) > Max:
        return len(str)
    return Max


def GetOptimalSpacing(InSpacing, Desired, MinDist=4):
    if InSpacing + MinDist <= InSpacing:
        return InSpacing
    return InSpacing + MinDist


def PrintHistogram(Histo, Print=True):
    try:
        H = Histo.GetHistogram()
    except:
        H = Histo

    if not Print or H.InheritsFrom("TH3") or H.InheritsFrom("TH2"):
        return
    #Determine the maximum width
    MaxCenter = 0
    MaxContent = 0
    MaxError = 0
    ToPrint = {}
    for i in range(GetNbins(H)):
        ToPrint[i] = {}
        ToPrint[i]["BinCenter"] = "%.2f - %.2f" % (H.GetXaxis().GetBinLowEdge(i), H.GetXaxis().GetBinUpEdge(i))
        if H.GetXaxis().GetBinLabel(i) != "":
            ToPrint[i]["BinCenter"] = "%s (%s)" % (ToPrint[i], ToPrint[i]["BinCenter"])
        MaxCenter = GetMaximumWidth(ToPrint[i]["BinCenter"], MaxCenter)
        ToPrint[i]["BinContent"] = "%.3f" % (H.GetBinContent(i))
        MaxContent = GetMaximumWidth(ToPrint[i]["BinContent"], MaxContent)
        ToPrint[i]["BinError"] = "%.3f" % (H.GetBinError(i))
        MaxError = GetMaximumWidth(ToPrint[i]["BinError"], MaxError)

    #### Getting of the content is finished lets try to print the thing
    Spacing = 32
    MaxCenter = GetOptimalSpacing(MaxCenter, Spacing)
    MaxContent = GetOptimalSpacing(MaxContent, Spacing)
    MaxError = GetOptimalSpacing(MaxError, Spacing)

    print FillWhiteSpaces(150, "+")
    try:
        print "Sample: %s    Region: %s   Variable: %s" % (Histo.GetName(), Histo.GetRegion(), Histo.GetVariableName())
    except:
        pass
    print FillWhiteSpaces(150, "+")

    for i in range(GetNbins(H)):
        print "%s%s|  %s%s|  %s%s%s" % (ToPrint[i]["BinCenter"], FillWhiteSpaces(MaxCenter - len(ToPrint[i]["BinCenter"])),
                                        ToPrint[i]["BinContent"], FillWhiteSpaces(MaxContent - len(ToPrint[i]["BinContent"])),
                                        ToPrint[i]["BinError"], FillWhiteSpaces(MaxError - len(ToPrint[i]["BinError"])),
                                        "" if i != H.GetXaxis().GetNbins() + 1 else "Over flow")
    print FillWhiteSpaces(150, "#")
    print "Total yield: %2f " % (Histo.Integral())
    print FillWhiteSpaces(150, "#")


## Some utility methods for algebra in root
def PolyDivide(num, den, ratio):
    nbins = GetNbins(num)
    for i in range(0, nbins):
        if 0. != den.GetBinContent(i):
            #print 'num: %s pm %s den: %s pm %s'%(num.GetBinContent(i),num.GetBinError(i),den.GetBinContent(i),den.GetBinError(i))
            #print 'ratio %s pm %s'%(num.GetBinContent(i)/den.GetBinContent(i),math.sqrt((num.GetBinError(i)/den.GetBinContent(i))**2.+(den.GetBinError(i)*num.GetBinContent(i)/den.GetBinContent(i)**2.)**2.))
            ratio.SetBinContent(i, num.GetBinContent(i) / den.GetBinContent(i))
            ratio.SetBinError(
                i,
                math.sqrt((num.GetBinError(i) / den.GetBinContent(i))**2. +
                          (den.GetBinError(i) * num.GetBinContent(i) / den.GetBinContent(i)**2.)**2.))
        else:
            ratio.SetBinContent(i, 1.)
            ratio.SetBinError(i, 1.)


def PolyMultiply(fac1, fac2, res):
    #print fac1, fac2
    nbins = GetNbins(fac1)
    for i in range(0, nbins):
        #print fac1.GetBinContent(i), fac1.GetBinError(i)
        #print fac2.GetBinContent(i), fac2.GetBinError(i)
        #print math.sqrt((fac2.GetBinError(i)*fac1.GetBinContent(i))**2.+(fac1.GetBinError(i)*fac2.GetBinContent(i))**2.)
        res.SetBinContent(i, fac1.GetBinContent(i) * fac2.GetBinContent(i))
        res.SetBinError(i, math.sqrt((fac2.GetBinError(i) * fac1.GetBinContent(i))**2. + (fac1.GetBinError(i) * fac2.GetBinContent(i))**2.))


def PolyAdd(add1, add2, total, weight=1.):
    nbins = GetNbins(add1)
    for i in range(0, nbins):
        total.SetBinContent(i, add1.GetBinContent(i) + weight * add2.GetBinContent(i))
        total.SetBinError(i, math.sqrt(add1.GetBinError(i)**2 + weight * add2.GetBinError(i)**2))


def PolySubtract(subfrom, subthis, difference):
    nbins = GetNbins(subfrom)
    for i in range(0, nbins):
        difference.SetBinContent(i, subfrom.GetBinContent(i) - subthis.GetBinContent(i))
        difference.SetBinError(i, math.sqrt(subfrom.GetBinError(i)**2 + subthis.GetBinError(i)**2))


def PolyScale(histo, scale):
    nbins = GetNbins(histo)
    for i in range(0, nbins):
        histo.SetBinContent(i, histo.GetBinContent(i) * scale)
        histo.SetBinError(i, histo.GetBinError(i) * scale)


def SetPolyErrors(histo):
    nbins = GetNbins(histo)
    for i in range(0, nbins):
        histo.SetBinError(i, math.sqrt(histo.GetBinContent(i)))


def AddHistos(histo1, histo2):
    if GetNbins(histo1) != GetNbins(histo2):
        logging.error('Trying to add 2 histograms with different numbers of bins, exiting...')
        sys.exit(1)
    if histo1.InheritsFrom("TH2Poly"):
        PolyAdd(histo1, histo2, histo1)
    elif histo1.InheritsFrom("TH1"):
        histo1.Add(histo2)


def CreateCloneAndAdd(ToClone, ToAdd):
    Clone = ToClone.Clone("CloneOf_" + ToClone.GetName())
    AddHistos(Clone, ToAdd)
    return Clone


def RemoveSpecialChars(In):
    Out = In
    NotWantedChars = ["{", "}", "[", "]", "#", "^", ")", "(", "&", "^"]
    ReplaceChars = [".", ","]
    for C in NotWantedChars:
        Out = Out.replace(C, "")
    for C in ReplaceChars:
        Out = Out.replace(C, "_")
    return Out


def CreateCulumativeHisto(Histo, Clone=False):
    try:
        H = Histo.GetHistogram()
    except:
        H = Histo
    if Clone: H = H.Clone("Culumative_%s" % (H.GetName()))
    for x in range(GetNbins(H)):
        Int, Err = IntegrateAndErrorTH1(H, x)
        H.SetBinContent(x, Int)
        H.SetBinError(x, Err)
    if Clone: return H


def SameContent(List1, List2):
    if len(List1) != len(List2):
        return False
    for item in List1:
        if item not in List2:
            return False
    for item in List2:
        if item not in List1:
            return False
    return True


def HasCommonElement(List1, List2):
    for item in List1:
        if item in List2:
            return True
    for item in List2:
        if item in List1:
            return True
    return False


def SameAxisBinning(Axis1, Axis2):
    if Axis1.GetNbins() != Axis2.GetNbins():
        return False
    for i in range(Axis1.GetNbins()):
        if Axis1.GetBinCenter(i) != Axis2.GetBinCenter(i):
            return False
        if Axis1.GetBinLowEdge(i) != Axis2.GetBinLowEdge(i):
            return False
    return True


def ExtractMasses(ModelPoint):
    if ModelPoint.find(".") != -1: ModelPoint = ModelPoint[:ModelPoint.rfind(".")]
    try:
        return (int(ModelPoint.split("_")[1]), int(ModelPoint.split("_")[2]))
    except:
        return (int(ModelPoint.split("_")[1]), -1)


def ReplaceStringInHistoAxis(histo, stringToReplace='#TMT', stringToInsert='\\'):
    histo.GetXaxis().SetTitle(histo.GetXaxis().GetTitle().replace('#TMT', '\\'))
    histo.GetYaxis().SetTitle(histo.GetYaxis().GetTitle().replace('#TMT', '\\'))
    histo.GetZaxis().SetTitle(histo.GetZaxis().GetTitle().replace('#TMT', '\\'))


def SoverBerror(sig, bkg, sigerror, bkgerror):
    """Error propagation from s / sqrt(b)"""
    return math.sqrt(4 * sigerror**2 * bkg**2 + bkgerror**2 * sig**2) / (2 * math.sqrt(bkg)**3)


def extendAxis(in_histo, axis=0, high_bin=1.e15, atEnd=False, low_bin=-1.e15, atBegin=False):
    ### Get the binning in form of std::vectors
    x_bins = ROOT.XAMPP.SampleHisto.ExtractBinningFromAxis(in_histo.GetXaxis())
    y_bins = None if in_histo.GetDimension() < 2 else ROOT.XAMPP.SampleHisto.ExtractBinningFromAxis(in_histo.GetYaxis())
    z_bins = None if in_histo.GetDimension() < 3 else ROOT.XAMPP.SampleHisto.ExtractBinningFromAxis(in_histo.GetZaxis())
    if not atEnd and not atBegin:
        logging.error("extendAxis(): Haeeeh, where shall I extend the bins")
        exit(1)
    if axis >= in_histo.GetDimension():
        logging.error("extendAxis():  Dafuq into which dimension shall I go")
        exit(1)

    to_extend = [x_bins, y_bins, z_bins][axis]
    ### Do not change the number of but rather the bin-widths of the first and the last bin
    ### There we can simply copy the content from one histogram to another but having an extended
    ### range along the desired direction
    if atEnd:
        if to_extend.at(to_extend.size() - 1) > high_bin: logging.warning("Dafuq your bin-edge you want to add is not high enough")
        to_extend.data()[to_extend.size() - 1] = high_bin
    if atBegin and to_extend.at(0) < low_bin:
        logging.error("Your bin edge is probably to high")
        exit(1)
    elif atBegin:
        to_extend.data()[0] = low_bin

    ### Create the extended histogram
    extended_histo = None
    if in_histo.GetDimension() == 1:
        extended_histo = ROOT.TH1D("Extend" + in_histo.GetName(), "Extend", x_bins.size() - 1, x_bins.data())
    elif in_histo.GetDimension() == 2:
        extended_histo = ROOT.TH2D("Extend" + in_histo.GetName(), "Extend",
                                   x_bins.size() - 1, x_bins.data(),
                                   y_bins.size() - 1, y_bins.data())
    elif in_histo.GetDimension() == 3:
        extended_histo = ROOT.TH3D("Extend" + in_histo.GetName(), "Extend",
                                   x_bins.size() - 1, x_bins.data(),
                                   y_bins.size() - 1, y_bins.data(),
                                   z_bins.size() - 1, z_bins.data())

    ### Copy the axis titles
    extended_histo.GetXaxis().SetTitle(in_histo.GetXaxis().GetTitle())
    extended_histo.GetYaxis().SetTitle(in_histo.GetYaxis().GetTitle())
    extended_histo.GetZaxis().SetTitle(in_histo.GetZaxis().GetTitle())
    ### Copy the content from In to out
    for b in range(GetNbins(in_histo)):
        extended_histo.SetBinContent(b, in_histo.GetBinContent(b))
        extended_histo.SetBinError(b, in_histo.GetBinError(b))
    extended_histo.SetEntries(in_histo.GetEntries())
    extended_histo.SetName(in_histo.GetName())
    return extended_histo


def ProjectInto1D(H3, project_along=0, bin1_start=0, bin1_end=-1, bin2_start=0, bin2_end=-1):
    ### Determine first the axis to prject
    axis_to_project = None
    if project_along >= 3 or H3.GetDimension() < 2 or (project_along == 2 and H3.GetDimension() == 2):
        logging.error("ProjectInto1D() --  You're a very funny guy. Where is my %d dimension!?" % (project_along + 1))
        return None
    elif project_along == 2:
        axis_to_project = H3.GetZaxis()
    elif (project_along == 1):
        axis_to_project = H3.GetYaxis()
    else:
        axis_to_project = H3.GetXaxis()

    bins_x = ROOT.XAMPP.SampleHisto.ExtractBinningFromAxis(axis_to_project)
    new_histo = ROOT.TH1D(id_generator(36), "Projection", bins_x.size() - 1, bins_x.data())
    new_histo.GetXaxis().SetTitle(axis_to_project.GetTitle())
    for to in range(axis_to_project.GetNbins() + 1):
        if project_along == 2:
            integral, error = IntegrateAndError(H3, bin1_start, bin1_end + 1, bin2_start, bin2_end + 1, to, to + 1)
        elif project_along == 1:
            integral, error = IntegrateAndError(H3, bin1_start, bin1_end + 1, to, to + 1, bin2_start, bin2_end + 1)
        elif project_along == 0:
            integral, error = IntegrateAndError(H3, to, to + 1, bin1_start, bin1_end + 1, bin2_start, bin2_end + 1)
        new_histo.SetBinContent(to, integral * GetBinNormalization(new_histo, to))
        new_histo.SetBinError(to, error * GetBinNormalization(new_histo, to))
    return new_histo


def GlobalBinToLocalBin(histo, b):
    x = ROOT.Long()
    y = ROOT.Long()
    z = ROOT.Long()
    histo.GetBinXYZ(b, x, y, z)
    return int(x), int(y), int(z)


def GetBinNormalization(H1, Bin):
    x, y, z = GlobalBinToLocalBin(H1, Bin)
    norm = H1.GetXaxis().GetBinWidth(1) / H1.GetXaxis().GetBinWidth(x)
    if H1.GetDimension() >= 2: norm *= H1.GetYaxis().GetBinWidth(1) / H1.GetYaxis().GetBinWidth(y)
    if H1.GetDimension() == 3: norm *= H1.GetZaxis().GetBinWidth(1) / H1.GetZaxis().GetBinWidth(z)
    return norm


def GetBinArea(H1, Bin):
    x, y, z = GlobalBinToLocalBin(H1, Bin)
    area = H1.GetXaxis().GetBinWidth(x)
    if H1.GetDimension() >= 2: area *= H1.GetYaxis().GetBinWidth(y)
    if H1.GetDimension() == 3: area *= H1.GetZaxis().GetBinWidth(z)
    return area


def isOverFlowBin(H1, Bin):
    x, y, z = GlobalBinToLocalBin(H1, Bin)
    if x == 0 or H1.GetNbinsX() + 1 <= x: return True
    if H1.GetDimension() >= 2 and (y == 0 or H1.GetNbinsY() + 1 <= y): return True
    if H1.GetDimension() == 3 and (z == 0 or H1.GetNbinsZ() + 1 <= z): return True
    return False


def GetInverseBinNormalization(H1, Bin):
    return 1. / GetBinNormalization(H1, Bin)


def mkdir(root_file, path=""):
    to_make = path[:path.find("/") + 1]
    if not root_file.GetDirectory(to_make):
        root_file.mkdir(to_make)
    if len(path[path.find("/") + 1:]) > 0:
        return mkdir(root_file=root_file.GetDirectory(to_make), path=path[path.find("/") + 1:])
    return root_file.GetDirectory(to_make)
