#! /usr/bin/env python
from ClusterSubmission.PeriodRunConverter import PeriodRunConverter, SetupArgParser
from ClusterSubmission.Utils import prettyPrint
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateLumiFromRuns, SetupPRWTool
import argparse

m_PeriodRunConverter = None


class XAMPP_PeriodRunConverter(PeriodRunConverter):
    def CalculateLumiFromPeriod(self, year, period):
        FirstRun, LastRun = self.GetFirstRunLastRun(year, period)
        return CalculateLumiFromRuns(FirstRun, LastRun)


def GetPeriodRunConverter():
    global m_PeriodRunConverter
    if not m_PeriodRunConverter:
        m_PeriodRunConverter = XAMPP_PeriodRunConverter()
        for i in range(15, 19):
            GetPeriodRunConverter().GetPeriods(i)
    return m_PeriodRunConverter


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='This script returns the runs for a given period or vice versa. For more help type \"python PeriodRunConverter.py -h\"',
        prog='PeriodRunConverter',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = SetupArgParser(parser)
    Options = parser.parse_args()
    SetupPRWTool()
    for Y in GetPeriodRunConverter().GetYears():
        prettyPrint("", "###############################################################")
        prettyPrint("Periods in year ", str(Y))
        prettyPrint("", "###############################################################")
        for P in GetPeriodRunConverter().GetPeriods(Y):
            FirstRun, LastRun = GetPeriodRunConverter().GetFirstRunLastRun(Y, P, project=Options.project)
            if FirstRun != LastRun:
                prettyPrint(P, "%d - %d (%.3f fb^{-1})" % (FirstRun, LastRun, GetPeriodRunConverter().CalculateLumiFromPeriod(Y, P)))
    if len(Options.runNumber) > 0 and len(Options.period) > 0:
        print 'Both runNumber (%s) and period (%s) were given, checking if given runs are included in given periods...' % (
            Options.runNumber, Options.period)
        print GetPeriodRunConverter().RunInPeriod(Options.runNumber, Options.year, Options.period)
    elif len(Options.runNumber) > 0 and len(Options.period) == 0:
        print 'A runNumber (%s) was given, checking corresponding period...' % (Options.runNumber[0])
        print GetPeriodRunConverter().GetPeriodFromRun(Options.runNumber[0])
    elif len(Options.runNumber) == 0 and len(Options.period) > 0:
        for Y in Options.year:
            for P in Options.period:
                print 'A period (%s) was given, checking corresponding runs...' % P
                print GetPeriodRunConverter().GetRunsFromPeriod(Y, P)
    else:
        print 'Please specify at least one runNumber or one period. For help use "python PeriodRunConverter.py -h"'
