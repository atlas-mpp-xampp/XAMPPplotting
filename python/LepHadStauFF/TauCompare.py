from XAMPPplotting.QuickPlots import GetHistoPaths
from XAMPPplotting.Utils import PrintHistogram, ProjectInto1D, extendAxis, GetPlotRangeX, GetNbins
from XAMPPplotting.PlotUtils import PlotUtils
from ClusterSubmission.Utils import id_generator, CreateDirectory
import ROOT, argparse, logging


def setupComparisonParser():
    ### This script compares the two
    parser = argparse.ArgumentParser(description='This script compares two different fake-factor files with each other',
                                     prog='TauCompare',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--firstFile", required=True, help="First fake factor file")
    parser.add_argument("--secondFile", help="Second fake factor file", default="")
    parser.add_argument("--thirdFile", help="Second fake factor file", default="")

    parser.add_argument("--firstLabel", help="Optional legend label of the first file", default="")
    parser.add_argument("--secondLabel", help="Optional legend label of the second file", default="")
    parser.add_argument("--thirdLabel", help="Optional legend label of the third file", default="")

    parser.add_argument("--outDir", help="Directory in which the files are going to be stored", default="FakeFactorComparisons/")
    return parser


def get_property(name_str, prop):
    tokens = name_str.split("_")
    if prop not in tokens:
        logging.warning("Invalid prpoerty %s" % (prop))
        return ""
    return tokens[tokens.index(prop) + 1]


def load_from_file(t_file, h_to_load, ignore_tau_def=True):
    H = t_file.Get(h_to_load)
    if H: return H
    if ignore_tau_def:
        for Key in t_file.GetListOfKeys():
            key_name = Key.GetName()
            if key_name.replace(get_property(key_name, "TauDef"), "") == h_to_load.replace(get_property(h_to_load, "TauDef"), ""):
                return Key.ReadObj()
    logging.error("Failed to load ", h_to_load)
    return None


def draw1DComparison(options, fake_factors, projected_1, projected_2, projected_3, bonus_str="", extra_label=""):
    tokens = fake_factors.split("_")
    chan = tokens[tokens.index("nominal") + 1]
    SR = tokens[tokens.index("SR") + 1]
    CR = tokens[tokens.index("CR") + 1]
    Prongs = tokens[tokens.index("Prong") + 1]
    Observable = tokens[tokens.index("Observable") + 1]

    pu = PlotUtils(lumi=139.)
    projected_1.SetLineColor(ROOT.kOrange)
    projected_1.SetMarkerColor(ROOT.kOrange)
    projected_1.SetMarkerStyle(ROOT.kFullDoubleDiamond)
    projected_1.SetTitle(options.firstFile[options.firstFile.rfind("/") +
                                           1:options.firstFile.rfind(".")] if len(options.firstLabel) == 0 else options.firstLabel)
    to_draw = [projected_1]
    if projected_2:
        to_draw += [projected_2]
        projected_2.SetLineColor(ROOT.kBlue)
        projected_2.SetMarkerColor(ROOT.kBlue)
        projected_2.SetMarkerStyle(ROOT.kFullFourTrianglesPlus)
        projected_2.SetTitle(options.secondFile[options.secondFile.rfind("/") +
                                                1:options.secondFile.rfind(".")] if len(options.secondLabel) == 0 else options.secondLabel)
    if projected_3:
        to_draw += [projected_3]
        projected_3.SetLineColor(ROOT.kRed)
        projected_3.SetMarkerColor(ROOT.kRed)
        projected_3.SetMarkerStyle(ROOT.kFullCircle)
        projected_3.SetTitle(options.thirdFile[options.thirdFile.rfind("/") +
                                               1:options.thirdFile.rfind(".")] if len(options.thirdLabel) == 0 else options.thirdLabel)

    pu.Prepare1PadCanvas(id_generator(10))
    pu.GetCanvas().cd()

    pu.drawStyling(Template=projected_1, ymin=1.e-3, ymax=max([p.GetMaximum() for p in to_draw]) * 1.35, TopPad=False)

    for p in to_draw:
        p.Draw("histSAME")
        p.Draw("SAME ")

    pu.CreateLegend(0.7, 0.7, 0.97, 0.9)
    pu.AddToLegend(to_draw, Style="PL")
    pu.DrawLegend()
    pu.DrawAtlas(0.18, 0.86)
    pu.DrawLumiSqrtS(0.18, 0.81)
    if len(extra_label) > 0: pu.DrawTLatex(0.18, 0.76, extra_label)

    pu.DrawTLatex(0.55, 0.88, "Flavour: %s" % (tokens[0]))
    pu.DrawTLatex(0.45, 0.83, "Channel: %s" % (chan))
    pu.DrawTLatex(0.45, 0.78, "CR: %s" % (CR))

    pu.DrawTLatex(0.6, 0.78, "SR: %s" % (SR))
    pu.DrawTLatex(0.6, 0.83, "Prongs: %s" % (Prongs))

    pu.saveHisto("%s/AllComparisons" % (options.outDir), ["pdf"])
    pu.saveHisto("%s/FFComparison_%s%s" % (options.outDir, fake_factors, "_" + bonus_str if len(bonus_str) > 0 else ""), ["pdf"])


if __name__ == "__main__":
    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    options = setupComparisonParser().parse_args()

    first_file = ROOT.TFile.Open(options.firstFile)
    if not first_file or not first_file.IsOpen():
        logging.error("No such file or directory %s" % (options.firstFile))
        exit(1)
    second_file = None
    if len(options.secondFile) > 0:
        second_file = ROOT.TFile.Open(options.secondFile)
        if not second_file or not second_file.IsOpen():
            logging.error("No such file or directory %s" % (options.secondFile))
            exit(1)
    third_file = None
    if len(options.thirdFile) > 0:
        third_file = ROOT.TFile.Open(options.thirdFile)
        if not third_file or not third_file.IsOpen():
            logging.error("No such file or directory %s" % (options.thirdFile))
            exit(1)

    CreateDirectory(options.outDir, False)
    DummyCanvas = ROOT.TCanvas("Dummy", "Dummy", 800, 600)
    DummyCanvas.SaveAs("%s/AllComparisons.pdf[" % (options.outDir))

    for fake_factors in GetHistoPaths(first_file):
        if not "nominal" in fake_factors: continue
        #if fake_factors[0] != "R": continue
        ff_histo_1 = first_file.Get(fake_factors)
        ff_histo_2 = load_from_file(second_file, fake_factors) if second_file else None
        ff_histo_3 = load_from_file(third_file, fake_factors) if third_file else None

        tokens = fake_factors.split("_")
        chan = tokens[tokens.index("nominal") + 1]
        SR = tokens[tokens.index("SR") + 1]
        Prongs = tokens[tokens.index("Prong") + 1]
        Observable = tokens[tokens.index("Observable") + 1]
        CR = tokens[tokens.index("CR") + 1]
        if CR == "Wjets": continue
        if ff_histo_1.GetDimension() == 2:
            ### Break down in each slice
            for y in range(1, ff_histo_1.GetNbinsY() + 1):
                projected_1 = extendAxis(in_histo=ProjectInto1D(ff_histo_1, project_along=0, bin1_start=y, bin1_end=y + 1),
                                         atEnd=True,
                                         high_bin=250)
                projected_2 = extendAxis(
                    in_histo=ProjectInto1D(ff_histo_2, project_along=0, bin1_start=y, bin1_end=y +
                                           1), atEnd=True, high_bin=250) if ff_histo_2 else None
                projected_3 = extendAxis(
                    in_histo=ProjectInto1D(ff_histo_3, project_along=0, bin1_start=y, bin1_end=y +
                                           1), atEnd=True, high_bin=250) if ff_histo_3 else None

                draw1DComparison(
                    options=options,
                    fake_factors=fake_factors,
                    projected_1=projected_1,
                    projected_2=projected_2,
                    projected_3=projected_3,
                    bonus_str=str(y),
                    extra_label="%.2f<%s<%.2f" %
                    (ff_histo_1.GetYaxis().GetBinLowEdge(y), ff_histo_1.GetYaxis().GetTitle(), ff_histo_1.GetYaxis().GetBinUpEdge(y)))

            ### Plot them one by one
            pu = PlotUtils(lumi=139.)

            ff_histos = [ff_histo_1]
            if ff_histo_2: ff_histos += [ff_histo_2]
            if ff_histo_3: ff_histos += [ff_histo_3]

            n_f = len(ff_histos)

            pu.Prepare1PadCanvas(id_generator(10), 800 * n_f)

            ## make sure to have the same axis range
            max_z = max([f.GetMaximum() for f in ff_histos])

            pad_x_width = 1. / n_f
            pad_x_min = 0.
            pad_x_max = pad_x_width
            pads = []

            for i in range(n_f):
                ff_histos[i].SetMaximum(max_z)
                ff_histos[i] = extendAxis(in_histo=ff_histos[i], atEnd=True, high_bin=200)
                ff_histos[i].GetZaxis().SetTitle("Fake factor")
                pad1 = ROOT.TPad(id_generator(10), "LeftPad", pad_x_min, 0., pad_x_max, 1)
                pad1.cd()
                ff_histos[i].Draw("colz")
                pads += [pad1]

                pad_x_min += pad_x_width
                pad_x_max += pad_x_width
                if i != n_f - 1: pad1.SetRightMargin(0)
                else: pad1.SetRightMargin(0.15)

                if i > 0: pad1.SetLeftMargin(0)
                pad1.SetTopMargin(0.15)

            pu.GetCanvas().cd()
            for p in pads:
                p.Draw()

            pu.DrawAtlas(pads[0].GetLeftMargin() / 2, 0.92)
            pu.DrawLumiSqrtS(pads[0].GetLeftMargin() / 2, 0.87)

            pu.DrawTLatex(0.47, 0.95, "Channel: %s" % (chan))
            pu.DrawTLatex(0.47, 0.91, "SR: %s" % (SR))
            pu.DrawTLatex(0.47, 0.87, "Prongs: %s" % (Prongs))

            pu.DrawTLatex(
                0.25, 0.92, options.firstFile[options.firstFile.rfind("/") +
                                              1:options.firstFile.rfind(".")] if len(options.firstLabel) == 0 else options.firstLabel)

            pu.DrawTLatex(
                0.75, 0.92, options.secondFile[options.secondFile.rfind("/") +
                                               1:options.secondFile.rfind(".")] if len(options.secondLabel) == 0 else options.secondLabel)

            pu.saveHisto("%s/AllComparisons" % (options.outDir), ["pdf"])
            pu.saveHisto("%s/FFComparison_2D_%s" % (options.outDir, fake_factors), ["pdf"])
        else:
            projected_1 = extendAxis(in_histo=ff_histo_1, atEnd=True, high_bin=250) if GetPlotRangeX(ff_histo_1) > 250 else ff_histo_1
            projected_2 = extendAxis(in_histo=ff_histo_2, atEnd=True,
                                     high_bin=250) if ff_histo_2 and GetPlotRangeX(ff_histo_2) > 250 else ff_histo_2
            projected_3 = extendAxis(in_histo=ff_histo_3, atEnd=True,
                                     high_bin=250) if ff_histo_3 and GetPlotRangeX(ff_histo_3) > 250 else ff_histo_3
            draw1DComparison(options=options,
                             fake_factors=fake_factors,
                             projected_1=projected_1,
                             projected_2=projected_2,
                             projected_3=projected_3)
    DummyCanvas.SaveAs("%s/AllComparisons.pdf]" % (options.outDir))
