""" 
@package TauHistoLoader
@author zenon@cern.ch
@date January 2018

Calculation of combined tau fake factors.

"""

import ROOT
import XAMPPplotting.FileStructureHandler
import XAMPPplotting.PlottingHistos

from XAMPPplotting import TauLogger as TL
from XAMPPplotting import TauInfo as TI
from XAMPPplotting import TauHisto as THI
from XAMPPplotting import TauHelper as TH


##
# @class TauHistoLoader
##
class TauHistoLoader(object):
    def __init__(self, name="histo loader", options=None, file_structure=None):
        "Load histos in transient memory"
        #parsed options
        self.__name = name
        self.__options = options
        self.__file_structure = file_structure
        #message service
        self.__msg = TL.msg(2)
        self.__msg.info("TauHistoLoader", "'%s' loaded ..." % (self.__name))
        #local properties
        self.__allow_negative_bins = True
        #loaded methods
        self.__create_log_report = True  #keep loaded/analyzed histos in log
        self.__tau_holder = THI.TauHistoHolder()
        self.__scan_file_structure = self.__scan_structure()
        self.__ready = self.__finalize()

    @property
    def ready(self):
        return self.__ready

    @property
    def tau_holder_size(self):
        return self.__tau_holder.size()

    @property
    def tau_holder(self):
        return self.__tau_holder

    def __allow(self, tau_info=None):
        if not tau_info:
            self.__msg.error("TauHistoLoader", "TaunIfo object is null: %s" % (tau_info.whatami))

        if tau_info.is_data and tau_info.is_truth_info():
            self.__msg.debug("TauHistoLoader", "Skip (data, truth) %s" % (tau_info.whatami))
            return False
        if not tau_info.is_data and not tau_info.is_needed_mc_template():
            return False
        if tau_info.is_SR() and tau_info.is_anti_iso_lepton():
            self.__msg.debug("TauHistoLoader", "Skip (SR, anti-iso lep) %s" % (tau_info.whatami))
            return False

        if tau_info.is_IsoSS_CR() and tau_info.is_anti_iso_lepton():
            self.__msg.debug("TauHistoLoader", "Skip (IsoSS CR, anti-iso lep) %s" % (tau_info.whatami))
            return False

        if tau_info.is_Top_CR():
            self.__msg.debug("TauHistoLoader", "Skip (Top CR, anti-iso lep) %s" % (tau_info.whatami))
            return False

        if tau_info.is_Wjets_CR() and tau_info.is_anti_iso_lepton():
            self.__msg.debug("TauHistoLoader", "Skip (W CR, anti-iso lep) %s" % (tau_info.whatami))
            return False

        if tau_info.is_Dilepton_CR():
            self.__msg.debug("TauHistoLoader", "Skip (Dilepton CR, anti-iso lep) %s" % (tau_info.whatami))
            return False

        if tau_info.is_prong_incl():
            self.__msg.debug("TauHistoLoader", "Skip (incl prongs) %s" % (tau_info.whatami))
            return False
        if tau_info.observable not in [self.__options.fake_ratio_obs, self.__options.rfactor_obs, self.__options.isolation_obs]:
            return False
        if tau_info.definition != self.__options.tau_definition:
            return False
        return True

    def __get_histo_sets(self, analysis="", region="", variable="", use_data=False):
        return XAMPPplotting.PlottingHistos.CreateHistoSets(Options=self.__options,
                                                            analysis=analysis,
                                                            region=region,
                                                            var=variable,
                                                            UseData=use_data,
                                                            skip_consistency=self.__allow_negative_bins,
                                                            break_down_bkg=False)

    def __get_histo_set(self, tau_info=None):
        hs = self.__get_histo_sets(tau_info.analysis, tau_info.region, tau_info.variable, tau_info.is_data_friendly)
        if hs:
            return hs[0]
        else:
            self.__msg.warning("TauHistoLoader", "Unable to find histo set for %s" % (tau_info.whatami))

        return None

    def __get_all_mc_histos(self, histoset=None):
        if not histoset:
            self.__msg.error("TauHistoLoader", "Empty histo set - cannot retrieve mc histos...")
            return []
        histos = [histoset.GetSummedBackground()]
        histos += histoset.GetBackgrounds()
        histos += histoset.GetSignals()
        return histos

    def __get_sum_bkg_mc_histos(self, histoset=None):
        if not histoset:
            self.__msg.error("TauHistoLoader", "Empty histo set - cannot retrieve total background...")
            return []

        return [histoset.GetSummedBackground()]

    def __scan_structure(self):

        if not self.__file_structure:
            self.__msg.fatal("TauHistoLoader", "Invalid file structure...")

        if not issubclass(type(self.__file_structure), XAMPPplotting.FileStructureHandler.FileStructHandler):
            self.__msg.fatal("TauHistoLoader", "Histograms are contained in a FileStructure but '{}'...".format(type(self.file_structure)))

        if not self.__tau_holder:
            self.__msg.warning("TauHistoLoader", "Invalid or null tau holder -- cannot proceed with scanning the file structure...")
            return False

        #loop over analyses
        self.__msg.info("TauHistoLoader", "Analyzing histo structure...")

        for analysis in self.__file_structure.GetConfigSet().GetAnalyses():
            #loop over regions
            for region in self.__file_structure.GetConfigSet().GetAnalysisRegions(analysis):
                #loop over variables
                for variable in self.__file_structure.GetConfigSet().GetVariables(analysis, region):
                    #cache info
                    tau_info = TI.TauInfo(analysis, region, variable, "mc")
                    if not tau_info.is_ff_ready: continue
                    tau_info_dt = TI.TauInfo(analysis, region, variable, "data")
                    if not tau_info_dt.is_ff_ready: continue
                    #allow this histo
                    if not self.__allow(tau_info):
                        self.__msg.debug("TauHistoLoader", "Skipping %s" % (tau_info.whatami))
                        continue
                    #histo set

                    histoset = self.__get_histo_set(tau_info=tau_info)

                    if not histoset:
                        self.__msg.warning("TauHistoLoader", "Skipping histo, analysis=%s %s/%s" % (analysis, region, variable))
                        #mc
                    if tau_info.is_mc_friendly:
                        sample_histo_list_mc = self.__get_sum_bkg_mc_histos(
                            histoset) if self.__options.only_sum_bkg else self.__get_all_mc_histos(histoset)
                        for tau_sample_histo_mc in sample_histo_list_mc:
                            tau_histo_mc = THI.TauHisto(tau_info=tau_info, tau_sample_histo=tau_sample_histo_mc)
                            self.__tau_holder.add(tau_histo_mc)
                            #tau_info.display()

                    #data
                    if tau_info_dt.is_data_friendly:
                        # Note: 0 element of XAMPPplotting.PlottingHistos.SampleHisto
                        tau_sample_histo_data = histoset.GetData()[0] if histoset.GetData() else None
                        if tau_sample_histo_data:
                            tau_histo_data = THI.TauHisto(tau_info=tau_info_dt, tau_sample_histo=tau_sample_histo_data)
                            #tau_histo_data.info.display()
                            self.__tau_holder.add(tau_histo_data)
                            #tau_histo_data.info.display()

        return True

    def __finalize(self):

        if not self.__scan_file_structure:
            self.__msg.error("TauHistoLoader", "Problem in scanning input histogram structure, cannot finalize job...")
            return False

        if self.__create_log_report and not self.__tau_holder.create_report():
            self.__msg.error("TauHistoLoader", "Unable to create report log for registered inputs...")
            return False

        self.__msg.info("TauHistoLoader", "Done!")

        return True
