##
# @class TauFlatFF
##
class TauFlatFF(object):
    def __init__(self, prongs=[], eta=[], ff=0.):
        self.__prongs = prongs
        self.__eta = eta
        self.__ff = ff

    @property
    def prongs(self):
        return self.__prongs

    @property
    def eta(self):
        return self.__eta

    @property
    def ff(self):
        return self.__ff
