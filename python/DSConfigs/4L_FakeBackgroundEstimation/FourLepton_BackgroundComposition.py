#! /usr/bin/env python
import ROOT, os, sys
from XAMPPplotting.Defs import *
from XAMPPplotting.CheckMetaData import GetNormalizationDB
#from XAMPPplotting.PeriodRunConverter import GetPeriodRunConverter
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi
from XAMPPplotting.FileUtils import ResolvePath, ReadInputConfig

#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2018-10-31/4L_Histos/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2018-12-06/4L_MTChecks/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2018-12-11/4L_MT_Checks/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-01-22/4L_SensitivityStudy/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-01-24/4L_MT_Checks/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-01-24/4L_ProcFrac/"

#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-03-05/ExcessFollowUp/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-05-20/4L_Sensitivty/"

Path = "/ptmp/mpp/maren/Cluster/OUTPUT/2019-07-11/4L_fakecomposition_combined/"

mc_period = ""

ZZ = DSconfig(colour=ROOT.TColor.GetColor(111, 224, 5),
              label="ZZ",
              name="ZZ",
              filepath=[
                  "%s/Sherpa222_VV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                  "%s/Sherpa222_ggZZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
              ],
              sampletype=SampleTypes.Irreducible,
              TheoUncert=0.1)
Higgs = DSconfig(
    colour=ROOT.TColor.GetColor(162, 145, 94),
    label="Higgs",
    name="VH/H",
    filepath=[
        "%s/PowHegPy8_ZH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        #            "%s/PowHegPy8_WH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        "%s/PowHegPy8_ggH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        "%s/PowHegPy8_VBFH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        "%s/aMcAtNloPy8_ttH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
    ],
    sampletype=SampleTypes.Irreducible)
VVV = DSconfig(colour=ROOT.TColor.GetColor(225, 230, 22),
               label="VVV",
               name="VVV",
               filepath=["%s/Sherpa221_VVV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period)],
               sampletype=SampleTypes.Irreducible)
ZJets = DSconfig(colour=ROOT.TColor.GetColor(210, 104, 23),
                 label="V+jets",
                 name="Vjets",
                 filepath=[
                     "%s/PowHegPy8_Zee%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Zmumu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Ztautau%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wenu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wmunu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wtaunu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                 ],
                 sampletype=SampleTypes.Reducible)
ttbar = DSconfig(
    colour=ROOT.TColor.GetColor(102, 252, 245),
    label="t#bar{t}",
    name="ttbar",
    filepath=[
        "%s/PowHegPy8_ttbar_incl%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        #    "%s/PowhegPy_top%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
    ],
    sampletype=SampleTypes.Reducible)
ttV = DSconfig(colour=ROOT.TColor.GetColor(245, 82, 253),
               label="t#bar{t}Z(WW)",
               name="ttV",
               filepath=[
                   "%s/aMCatNLOPy8_ttZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/aMcAtNlo_tWZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_4t%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_ttWW%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_ttWZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
               ],
               sampletype=SampleTypes.Irreducible)

OneFakeRed = DSconfig(colour=ROOT.TColor.GetColor(20, 54, 208),
                      label="1-fakes",
                      name="one-fakes",
                      filepath=[
                          "%s/Sherpa221_VV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                          "%s/aMCatNLOPy8_ttW%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                      ],
                      sampletype=SampleTypes.Reducible)
