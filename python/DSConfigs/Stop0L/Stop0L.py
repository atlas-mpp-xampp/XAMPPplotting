#! /usr/bin/env python
from XAMPPplotting.Stop0LColors import *

BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-07-18/stop_SRSys/'
# BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-07/stop0L_SRATTCheck/' # there was a bug in the dR(bb) calculation -> higher indes MUST be first!!
# BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-07/stop0L_SRDCheck/' # for SRD, the bjetpt0+1 cut is using pt-sorted b-jets!!! not weight-sorted
# BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-07/stop_SRs/'

# Data
# Data = DSconfig(lumi=36.1,colour=ROOT.kBlack,name="Data",label="Data",filepath=BasePath+"Data_Period_Run2_Input.root",sampletype=SampleTypes.Data)

# Signal MC
# TT_directTT_1000_1 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_1000_1",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(1000,1) GeV",filepath=BasePath+"TT_directTT_1000_1_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_600_300 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_600_300",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,300) GeV",filepath=BasePath+"TT_directTT_600_300_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_700_400 = DSconfig(colour=ROOT.kRed-4,name="TT_directTT_700_400",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(700,400) GeV",filepath=BasePath+"TT_directTT_700_400_a821_r7676_Input.root",sampletype=SampleTypes.Signal)

# TT_directTT_250_77 = DSconfig(colour=sigColors['450_277'],name="TT_directTT_250_77",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(250,77) GeV",filepath=BasePath+"TT_directTT_250_77_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_300_127 = DSconfig(colour=sigColors['500_327'],name="TT_directTT_300_127",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(300,127) GeV",filepath=BasePath+"TT_directTT_300_127_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_350_177 = DSconfig(colour=sigColors['350_177'],name="TT_directTT_350_177",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(350,177) GeV",filepath=BasePath+"TT_directTT_350_177_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_450_277 = DSconfig(colour=sigColors['450_277'],name="TT_directTT_450_277",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(450,277) GeV",filepath=BasePath+"TT_directTT_450_277_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_600_427 = DSconfig(colour=sigColors['600_427'],name="TT_directTT_600_427",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,427) GeV",filepath=BasePath+"TT_directTT_600_427_a821_r7676_Input.root",sampletype=SampleTypes.Signal)

# GG_1700_400 = DSconfig(colour=ROOT.kMagenta,name="GG_1700_400",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(1700,400) GeV",filepath=BasePath+"GG_directGtc5_1700_400_a821_r7676_Input.root",sampletype=SampleTypes.Signal)

# TT_onestepBB_400_100_50 = DSconfig(colour=ROOT.kGreen-2,name="TT_onestepBB_400_100_50",label="#splitline{(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})}{=(400,100,50)GeV}",filepath=BasePath+"TT_onestepBB_400_100_50_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_onestepBB_700_100_50 = DSconfig(colour=ROOT.kGreen-2,name="TT_onestepBB_700_100_50",label="#splitline{(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})}{=(700,100,50)GeV}",filepath=BasePath+"TT_onestepBB_700_100_50_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_mixedBT_700_100_50 = DSconfig(colour=ROOT.kGreen-2,name="TT_mixedBT_700_100_50",label="#splitline{(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})}{=(700,100,50)GeV mixed}",filepath=BasePath+"TT_mixedBTMGPy8EG_700_100_50_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_onestepBB_700_200_100 = DSconfig(colour=ROOT.kGreen-2,name="TT_onestepBB_700_200_100",label="#splitline{(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})}{=(700,200,100)GeV}",filepath=BasePath+"TT_onestepBB_700_200_100_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_mixedBT_700_200_100 = DSconfig(colour=ROOT.kGreen-2,name="TT_mixedBT_700_200_100",label="#splitline{(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})}{=(700,200,100)GeV mixed}",filepath=BasePath+"TT_mixedBTMGPy8EG_700_200_100_a821_r7676_Input.root",sampletype=SampleTypes.Signal)

DE_tt_c1_M400 = DSconfig(colour=ROOT.kRed,
                         name="DE_tt_c1_M400",
                         label="(m_{M},m_{#varphi})=(400,0.1) GeV (c_{1}=1)",
                         filepath=BasePath + "DE_tt_c1_M400_Input.root",
                         sampletype=SampleTypes.Signal,
                         markerstyle=ROOT.kOpenCircle)
DE_tt_c2_M600 = DSconfig(colour=ROOT.kRed,
                         name="DE_tt_c2_M600",
                         label="(m_{M},m_{#varphi})=(600,0.1) GeV (c_{2}=1)",
                         filepath=BasePath + "DE_tt_c2_M600_Input.root",
                         sampletype=SampleTypes.Signal,
                         markerstyle=ROOT.kOpenCircle)

# Background MC
ttV = DSconfig(colour=bkgColors['ttV'],
               name="ttV",
               label="t#bar{t}+V",
               filepath=BasePath + "ttV_Input.root",
               sampletype=SampleTypes.Irreducible)
# ttgamma = DSconfig(colour=bkgColors['ttGamma'],name="ttgamma",label="t#bar{t}+#gamma",filepath=BasePath+"ttGamma_a821_r7676_Input.root",sampletype=SampleTypes.Irreducible)
Diboson = DSconfig(colour=bkgColors['dibosons'],
                   name="Diboson",
                   label="Diboson",
                   filepath=BasePath + "DibosonNew_Input.root",
                   sampletype=SampleTypes.Reducible)
SingleTop = DSconfig(colour=bkgColors['singleTop'],
                     name="SingleTop",
                     label="Single Top",
                     filepath=BasePath + "singleTop_Input.root",
                     sampletype=SampleTypes.Reducible)
ttbar = DSconfig(colour=bkgColors['ttbar'],
                 name="ttbar",
                 label="t#bar{t}",
                 filepath=BasePath + "ttbar_Input.root",
                 sampletype=SampleTypes.Reducible)
Wjets = DSconfig(colour=bkgColors['W'],
                 name="Wjets",
                 label="W+jets",
                 filepath=BasePath + "Wjets_Sherpa221_Input.root",
                 sampletype=SampleTypes.Reducible)
Zjets = DSconfig(colour=bkgColors['Z'],
                 name="Zjets",
                 label="Z+jets",
                 filepath=BasePath + "Zjets_Sherpa221_Input.root",
                 sampletype=SampleTypes.Reducible)
# SinglePhoton = DSconfig(colour=ROOT.kViolet+5,name="SinglePhoton",label="#gamma+jets",filepath=BasePath+"SinglePhoton_Input.root",sampletype=SampleTypes.Reducible)
