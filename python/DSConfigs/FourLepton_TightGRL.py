#! /usr/bin/env python
import ROOT, os, sys
from XAMPPplotting.Defs import *
from XAMPPplotting.CheckMetaData import GetNormalizationDB
#from XAMPPplotting.PeriodRunConverter import GetPeriodRunConverter
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi
from XAMPPplotting.FileUtils import ResolvePath, ReadInputConfig

#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2018-10-31/4L_Histos/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2018-12-06/4L_MTChecks/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2018-12-11/4L_MT_Checks/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-01-22/4L_SensitivityStudy/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-01-24/4L_MT_Checks/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-01-24/4L_ProcFrac/"

#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-03-05/ExcessFollowUp/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-05-20/4L_Sensitivty/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-06-06/FourLep_Syst/"

#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-08-30/4LRNN_Analysis/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-09-02/4LRNN_Analysis/"

Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-10-17/4L_Main/"

C_Name = DSConfigName("Tight GRL")
SignalPath = Path
DataPath = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-10-17/4L_TightGRL/"

CONFIGPATH = ResolvePath("XAMPPmultilep/InputConf/MPI/v025/Data/")
mc_period = ""
Periods = [
    Cfg.replace(".conf", "") for Cfg in os.listdir(CONFIGPATH)
    if (Cfg.find("data") != -1 and (len(mc_period) == 0 or (
        (Cfg.find("data15") != -1 or Cfg.find("data16") != -1) and mc_period.find("a") != -1) or
                                    (Cfg.find("data17") != -1 and mc_period.find("d") != -1) or
                                    (Cfg.find("data18") != -1 and mc_period.find("e") != -1))) and Cfg.find("debugrec_hlt") == -1
]
mc_period = ""
Files = []
LUMI = 36.1
#LUMI = 80
LUMI = 139
for P in Periods:
    Files += ReadInputConfig("%s/%s.conf" % (CONFIGPATH, P))

#for R in GetNormalizationDB(Files).GetRunNumbers():
#    LUMI += CalculateRecordedLumi(R)

#LUMI = CalculateLumiFromPeriod([], [2015, 2016]) / 1000.
#print LUMI

# Samples

Data = DSconfig(lumi=LUMI,
                colour=ROOT.kBlack,
                label="data",
                name="data",
                filepath=["%s/%s.root" % (DataPath, P) for P in Periods],
                sampletype=SampleTypes.Data)

ZZ = DSconfig(colour=ROOT.TColor.GetColor(111, 224, 5),
              label="ZZ",
              name="ZZ",
              filepath=[
                  "%s/Sherpa222_VV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                  "%s/Sherpa222_ggZZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
              ],
              sampletype=SampleTypes.Irreducible,
              TheoUncert=0.1)
Higgs = DSconfig(
    colour=ROOT.TColor.GetColor(162, 145, 94),
    label="Higgs",
    name="VH/H",
    filepath=[
        "%s/PowHegPy8_ZH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        #            "%s/PowHegPy8_WH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        "%s/PowHegPy8_ggH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        "%s/PowHegPy8_VBFH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        "%s/aMcAtNloPy8_ttH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
    ],
    sampletype=SampleTypes.Irreducible)
VVV = DSconfig(colour=ROOT.TColor.GetColor(225, 230, 22),
               label="VVV",
               name="VVV",
               filepath=["%s/Sherpa221_VVV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period)],
               sampletype=SampleTypes.Irreducible)
ZJets = DSconfig(colour=ROOT.TColor.GetColor(210, 104, 23),
                 label="V+jets",
                 name="Vjets",
                 filepath=[
                     "%s/PowHegPy8_Zee%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Zmumu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Ztautau%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wenu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wmunu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wtaunu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                 ],
                 sampletype=SampleTypes.Reducible)
ttbar = DSconfig(
    colour=ROOT.TColor.GetColor(102, 252, 245),
    label="t#bar{t}",
    name="ttbar",
    filepath=[
        "%s/PowHegPy8_ttbar_incl%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        #    "%s/PowhegPy_top%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
    ],
    sampletype=SampleTypes.Reducible)
ttV = DSconfig(colour=ROOT.TColor.GetColor(245, 82, 253),
               label="t#bar{t}Z(WW)",
               name="ttV",
               filepath=[
                   "%s/aMCatNLOPy8_ttZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/aMcAtNlo_tWZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_4t%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_ttWW%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_ttWZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
               ],
               sampletype=SampleTypes.Irreducible)

OneFakeRed = DSconfig(colour=ROOT.TColor.GetColor(20, 54, 208),
                      label="1-fakes",
                      name="one-fakes",
                      filepath=[
                          "%s/Sherpa221_VV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                          "%s/aMCatNLOPy8_ttW%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                      ],
                      sampletype=SampleTypes.Reducible)

#C1C1_800_200_LLE12k = DSconfig (colour=ROOT.kMagenta , label="m_{#tilde{#chi}^{0}_{1}}=800 GeV" , name="C1C1_800_200_LLE12k" , filepath=SignalPath+"C1C1_800_200_LLE12k.root", sampletype=SampleTypes.Signal)
#C1C1_1000_200_LLE12k = DSconfig (colour=ROOT.kBlack , label="m_{#tilde{#chi}^{#pm}_{1}}=1 TeV, #lambda_{12k}" , name="C1C1_1000_200_LLE12k" , filepath=SignalPath+"C1C1_1000_200_LLE12k.root", sampletype=SampleTypes.Signal)

C1C1_1000_100_LLEi33 = DSconfig(colour=ROOT.kBlue + 2,
                                label="m_{#tilde{#chi}^{#pm}_{1}}=1200 GeV, #lambda_{i33}",
                                name="Wino_1200_600_LLEi33",
                                filepath=SignalPath + "Wino_1200_600_LLEi33.root",
                                sampletype=SampleTypes.Signal)

#GGM = DSconfig(
#    colour=ROOT.kBlue + 2,
#    label="GGM",
#    name="GGM",
#    filepath=[
#        "%s/GGMHinoZh50_200%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
#    ],
#    sampletype=SampleTypes.Signal)

#
