import ROOT, os, sys
from XAMPPplotting.Defs import *
from XAMPPplotting.CheckMetaData import GetNormalizationDB
#from XAMPPplotting.PeriodRunConverter import GetPeriodRunConverter
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi

from XAMPPplotting.FileUtils import ResolvePath, ReadInputConfig

PATH = "/ptmp/mpp/junggjo9/Cluster/Output/2017-05-28/FourLep_SF/"
PATH = "/ptmp/mpp/junggjo9/Cluster/Output/2018-06-30/4LepSF/"
PATH = "/ptmp/mpp/junggjo9/Cluster/Output/2018-09-11/4LSF_Top/"
PATH = "/ptmp/mpp/zenon/Cluster/OUTPUT/2018-11-12/TauFakes_2018_11_12_21_57_36/"

CONFIGPATH = ResolvePath("XAMPPplotting/InputConf/FourLepton/SFAnalysis/")

#Periods = [Cfg.replace(".conf", "") for Cfg in os.listdir(CONFIGPATH) if Cfg.find("data") != -1 and Cfg.find("17") == -1]
Periods = [Cfg.replace(".conf", "") for Cfg in os.listdir(CONFIGPATH) if Cfg.find("data") != -1]

print "Data periods considered", Periods

#Samples = GetFilesPerSample( ["%s/%s.conf"%(CONFIGPATH,P) for P in Periods ])
Files = []

CalcLumi = False

if CalcLumi:
    # for S in Samples.itervalues():
    for P in Periods:
        Files += ReadInputConfig("%s/%s.conf" % (CONFIGPATH, P))
    LUMI = 0.
    for R in GetNormalizationDB(Files).GetRunNumbers():
        LUMI += CalculateRecordedLumi(R)
    print LUMI
    exit(1)

LUMI = 80.2

#mcperiod = "mc16a"
mcperiod = ""

Data = DSconfig(lumi=LUMI,
                colour=ROOT.kBlack,
                label="Data",
                name="Data",
                filepath=["%s/%s.root" % (PATH, P) for P in Periods],
                sampletype=SampleTypes.Data)

top = DSconfig(colour=ROOT.kBlue - 2,
               name="top",
               label="top",
               filepath=[
                   "%s/PowHegPy8_ttbar_incl%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
               ],
               sampletype=SampleTypes.Irreducible)

Zjets = DSconfig(colour=ROOT.kViolet - 9,
                 name="Zjets",
                 label="Z+Jets",
                 filepath=[
                     "%s/PowHegPy8_Zee%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
                     "%s/PowHegPy8_Zmumu%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
                     "%s/PowHegPy8_Ztautau%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod)
                 ],
                 sampletype=SampleTypes.Irreducible)

Wjets = DSconfig(colour=ROOT.kViolet - 7,
                 name="Wjets",
                 label="W+Jets",
                 filepath=[
                     "%s/PowHegPy8_Wenu%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
                     "%s/PowHegPy8_Wmunu%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
                     "%s/PowHegPy8_Wtaunu%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
                 ],
                 sampletype=SampleTypes.Irreducible)

VV = DSconfig(colour=ROOT.kGreen - 3,
              name="VV",
              label="Dibosons",
              filepath=[
                  "%s/PowHegPy8_VV%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
              ],
              sampletype=SampleTypes.Irreducible)
