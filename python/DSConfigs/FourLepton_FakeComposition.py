#! /usr/bin/env python
import ROOT, os, sys
from XAMPPplotting.Defs import *

Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-10-02/FourLepton_Fakes/"
### v2 are the signal regions and v3 the control regions
Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-10-02/FourLepton_Fakes_v2/"
### Using v025 of the SUSY4L n-tuples implemeting RNN taus
Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-10-17/4L_FakeComposition/"
### Using v026 of the SUSY4L n-tuples implementing the IFF classifier
Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-11-15/FF_composition_4L/"
### Using v028 of the SUSY4L n-tuples kicking the Quark jets
Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-13/4L_TruthTypes/"

C_Name = DSConfigName("Fake composition", ReferenceSample="Real")
#        Fake.root       Gluon.root             Jet.root

Real = DSconfig(colour=ROOT.TColor.GetColor(111, 224, 5),
                label="Prompt",
                name="Real",
                filepath=[Path + "/Real.root"],
                sampletype=SampleTypes.Irreducible,
                TheoUncert=0.1)
UnMatched = DSconfig(colour=ROOT.TColor.GetColor(162, 145, 94),
                     label="UN",
                     name="UnMatched",
                     filepath=[Path + "Unmatched.root"],
                     sampletype=SampleTypes.Irreducible)
HF = DSconfig(colour=ROOT.TColor.GetColor(225, 230, 22),
              label="HF",
              name="HF",
              filepath=[Path + "/HF.root"],
              sampletype=SampleTypes.Irreducible)
LF = DSconfig(colour=ROOT.TColor.GetColor(210, 104, 23),
              label="LF",
              name="LF",
              filepath=[Path + "/LF.root"],
              sampletype=SampleTypes.Irreducible)

### Convserion fakes have different naming schemes for lightleptons and taus
ConvTaus = DSconfig(colour=ROOT.TColor.GetColor(102, 252, 245),
                    label="CO",
                    name="Conv",
                    filepath=[Path + "/Conv.root"],
                    sampletype=SampleTypes.Irreducible)

#ConvLightLep = DSconfig(colour=ROOT.TColor.GetColor(102, 252, 245),
#                        label="CO",
#                        name="CONV_tau",
#                        filepath=[Path + "Conv.root"],
#                        sampletype=SampleTypes.Irreducible)

#LightLep = DSconfig(colour=ROOT.TColor.GetColor(245, 82, 253),
#                    label="leptons",
#                    name="LightLep",
#                    filepath=[
#                        Path + "Elec.root",
#                        Path + "Muon.root",
#                    ],
#                    sampletype=SampleTypes.Irreducible)

Jets = DSconfig(
    colour=ROOT.TColor.GetColor(20, 54, 208),
    label="GJ",
    name="jets",
    filepath=[Path + "Gluon.root"],  #, Path + "Jet.root"],
    sampletype=SampleTypes.Irreducible)
