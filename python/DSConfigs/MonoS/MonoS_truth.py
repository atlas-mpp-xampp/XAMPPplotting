#!/usr/bin/env python
import os
from XAMPPplotting.Defs import *
ROOT.gStyle.SetOptStat(0)
BasePath = '/nfs/dust/atlas/user/pgadow/monoS/plotting/truth/'

# Specify samples
# Signal MC
ds_signal = os.path.join(BasePath, "monoSWW_zp1000_dm200_hs160.root")
monoSWW_zp1000_dm200_hs160 = DSconfig(name="monoSWW_zp1000_dm200_hs160",
                                      label="(m_{Z'},m_{hs})=(1000,160)",
                                      colour=ROOT.kRed,
                                      filepath=ds_signal,
                                      sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "monoSWW_zp1500_dm200_hs160.root")
monoSWW_zp1500_dm200_hs160 = DSconfig(name="monoSWW_zp1500_dm200_hs160",
                                      label="(m_{Z'},m_{hs})=(1500,160)",
                                      colour=ROOT.kViolet,
                                      filepath=ds_signal,
                                      sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "monoSWW_zp2000_dm200_hs160.root")
monoSWW_zp2000_dm200_hs160 = DSconfig(name="monoSWW_zp2000_dm200_hs160",
                                      label="(m_{Z'},m_{hs})=(2000,160)",
                                      colour=ROOT.kRed - 7,
                                      filepath=ds_signal,
                                      sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "monoSWW_zp2500_dm200_hs160.root")
monoSWW_zp2500_dm200_hs160 = DSconfig(name="monoSWW_zp2500_dm200_hs160",
                                      label="(m_{Z'},m_{hs})=(2500,160)",
                                      colour=ROOT.kRed + 4,
                                      filepath=ds_signal,
                                      sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "monoSWW_zp3000_dm200_hs160.root")
monoSWW_zp3000_dm200_hs160 = DSconfig(name="monoSWW_zp3000_dm200_hs160",
                                      label="(m_{Z'},m_{hs})=(3000,160)",
                                      colour=ROOT.kGreen + 2,
                                      filepath=ds_signal,
                                      sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "monoSWW_zp3500_dm200_hs160.root")
monoSWW_zp3500_dm200_hs160 = DSconfig(name="monoSWW_zp3500_dm200_hs160",
                                      label="(m_{Z'},m_{hs})=(3500,160)",
                                      colour=ROOT.kBlue,
                                      filepath=ds_signal,
                                      sampletype=SampleTypes.Signal)
# ds_signal = os.path.join(BasePath, "monoSWW_zp4000_dm200_hs160.root")
# monoSWW_zp4000_dm200_hs160 = DSconfig(name="monoSWW_zp4000_dm200_hs160",
#                                label="(m_{Z'},m_{hs})=(4000,160)",
#                                colour=ROOT.kTeal     ,
#                                filepath=ds_signal,
#                                sampletype=SampleTypes.Signal)
