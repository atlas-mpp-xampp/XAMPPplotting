#! /usr/bin/env python
import ROOT, os, sys
from XAMPPplotting.Defs import *
from XAMPPplotting.CheckMetaData import GetNormalizationDB
#from XAMPPplotting.PeriodRunConverter import GetPeriodRunConverter
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi
from XAMPPplotting.FileUtils import ResolvePath, ReadInputConfig

Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-21/FourLep_Histos/"
Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-20/4LnoZ_bonly/"

C_Name = DSConfigName("stanard GRL")
SignalPath = Path
DataPath = Path
CONFIGPATH = ResolvePath("XAMPPmultilep/InputConf/MPI/v028/Data/")
mc_period = ""
Periods = [
    Cfg.replace(".conf", "") for Cfg in os.listdir(CONFIGPATH)
    if (Cfg.find("data") != -1 and (len(mc_period) == 0 or (
        (Cfg.find("data15") != -1 or Cfg.find("data16") != -1) and mc_period.find("a") != -1) or
                                    (Cfg.find("data17") != -1 and mc_period.find("d") != -1) or
                                    (Cfg.find("data18") != -1 and mc_period.find("e") != -1))) and Cfg.find("debugrec_hlt") == -1
]
mc_period = ""
Files = []
LUMI = 139
for P in Periods:
    Files += ReadInputConfig("%s/%s.conf" % (CONFIGPATH, P))

#for R in GetNormalizationDB(Files).GetRunNumbers():
#    LUMI += CalculateRecordedLumi(R)

#LUMI = CalculateLumiFromPeriod([], [2015, 2016]) / 1000.
#print LUMI

# Samples

Data = DSconfig(lumi=LUMI,
                colour=ROOT.kBlack,
                label="data",
                name="data",
                filepath=["%s/%s.root" % (DataPath, P) for P in Periods],
                sampletype=SampleTypes.Data)

ZZ = DSconfig(colour=ROOT.kAzure - 4,
              label="ZZ",
              name="ZZ",
              filepath=[
                  "%s/Sherpa222_VV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                  "%s/Sherpa222_ggZZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
              ],
              sampletype=SampleTypes.Irreducible,
              TheoUncert=0.1)
Higgs = DSconfig(
    colour=ROOT.kViolet - 9,
    label="Higgs",
    name="VH/H",
    filepath=[
        "%s/PowHegPy8_ZH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        "%s/PowHegPy8_WH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        #"%s/PowHegPy8_ggH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        #"%s/PowHegPy8_VBFH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        "%s/PowHegPy8_ttH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
    ],
    sampletype=SampleTypes.Irreducible)
VVV = DSconfig(colour=ROOT.kMagenta - 9,
               label="VVV",
               name="VVV",
               filepath=["%s/Sherpa221_VVV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period)],
               sampletype=SampleTypes.Irreducible)
ZJets = DSconfig(colour=ROOT.kYellow,
                 label="V+jets",
                 name="Vjets",
                 filepath=[
                     "%s/PowHegPy8_Zee%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Zmumu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Ztautau%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wenu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wmunu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wtaunu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                 ],
                 sampletype=SampleTypes.Reducible)
ttbar = DSconfig(colour=ROOT.kYellow + 1,
                 label="t#bar{t}",
                 name="ttbar",
                 filepath=[
                     "%s/PowHegPy8_ttbar_incl%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowhegPy_top%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                 ],
                 sampletype=SampleTypes.Reducible)
ttV = DSconfig(colour=ROOT.kTeal - 5,
               label="t#bar{t}Z(WW)",
               name="ttV",
               filepath=[
                   "%s/aMCatNLOPy8_ttZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/aMcAtNlo_tWZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_4t%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_ttWW%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_ttWZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
               ],
               sampletype=SampleTypes.Irreducible)

OneFakeRed = DSconfig(colour=ROOT.TColor.GetColor(20, 54, 208),
                      label="1-fakes",
                      name="one-fakes",
                      filepath=[
                          "%s/Sherpa221_VV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                          "%s/aMCatNLOPy8_ttW%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                      ],
                      sampletype=SampleTypes.Reducible)

SignalPath = '/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-21/FourLep_Histos/'
SignalPath = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-20/4LnoZ_bonly/"

GG_2400_2000_LLE12k = DSconfig(colour=ROOT.kAzure,
                               sampletype=SampleTypes.Signal,
                               name='GG_2400_2000_LLE12k',
                               label='#tilde{g} (2400,2000)',
                               filepath=SignalPath + '/GG_2400_2000_LLE12k.root')

Wino_1700_1200_LLE12k = DSconfig(colour=ROOT.kMagenta,
                                 sampletype=SampleTypes.Signal,
                                 name='Wino_1700_1200_LLE12k',
                                 label='#tilde{W} (1700,1200)',
                                 filepath=SignalPath + '/Wino_1700_1200_LLE12k.root')
Wino_1300_1290_LLE12k = DSconfig(colour=ROOT.kTeal,
                                 sampletype=SampleTypes.Signal,
                                 name='Wino_1300_1290_LLE12k',
                                 label='#tilde{W} (1300,1290)',
                                 filepath=SignalPath + '/Wino_1300_1290_LLE12k.root')
LV_1100_600_LLE12k = DSconfig(colour=ROOT.kOrange,
                              sampletype=SampleTypes.Signal,
                              name='LV_1100_600_LLE12k',
                              label='#tilde{#it{l}}/#tilde{#nu} (1100,600)',
                              filepath=SignalPath + '/LV_1100_600_LLE12k.root')
