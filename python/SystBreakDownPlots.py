#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import PlotUtils
from ClusterSubmission.Utils import CreateDirectory, id_generator
from XAMPPplotting.FileStructureHandler import GetSystPairer, GetStructure, ClearServices
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.Utils import CreateRatioHistos, RemoveSpecialChars, setupBaseParser
import logging


def is_used_variation(ref_list, syst_var):
    for ref in ref_list:
        if ref.name() == syst_var.name() and syst_var.is_symmetric(): return True

    return False


def drawHisto(Options, analysis, region, var, bonusstr=""):
    FullHistoSet = CreateHistoSets(Options, analysis, region, var, UseData=False)
    if not FullHistoSet or not FullHistoSet[0].CheckHistoSet():
        logging.warning("I cannot draw stacked background MC histogram without background samples, skipping...")
        return False

    DefaultSet = FullHistoSet[0]
    if DefaultSet.isTH2(): return False

    logstr = "" if not Options.doLogY else "_LogY"

    once_plotted = False
    for sample in [DefaultSet.GetSummedBackground()] + DefaultSet.GetBackgrounds() + DefaultSet.GetSignals():
        if not sample.isValid():
            logging.warning("Skip systematic break down of %s" % (sample.GetName()))
            continue
        if len(Options.samplesOnly) > 0 and sample.GetName() not in Options.samplesOnly:
            logging.info("Skip sample %s because you do not want to have its breakdown" % (sample.GetName()))
            continue
        pu = PlotUtils(status=Options.label, size=24, lumi=DefaultSet.GetLumi(), normalizedToUnity=False)
        pu.Prepare2PadCanvas(id_generator(30), 800, 600, Options.quadCanvas)
        pu.GetTopPad().cd()
        if Options.doLogY: pu.GetTopPad().SetLogy()
        pu.CreateLegend(Options.LegendXCoords[0], Options.LegendYCoords[0], Options.LegendXCoords[1], Options.LegendYCoords[1])
        #enum SystErrorSorting { Naming = 1, LargestUp, LargestDown, SmallestUp, SmallestDown };
        syst_list_up = sample.GetSystComponents(ROOT.XAMPP.SampleHisto.LargestUp)
        syst_list_dn = sample.GetSystComponents(ROOT.XAMPP.SampleHisto.LargestDown)
        for x in syst_list_up:
            print x.name(), x.total_up_dev(), x.total_dn_dev()
        entering_up = [syst_list_up[i] for i in range(min(Options.leadingNSyst, len(syst_list_up)))]
        entering_dn = []

        for i in range(len(syst_list_dn)):
            if is_used_variation(entering_up, syst_list_dn[i]): continue
            entering_dn += [syst_list_dn[i]]
            if len(entering_dn) >= Options.leadingNSyst: break
        hist_list = [e.final_dn_histo() for e in entering_dn] + [e.final_dn_histo() for e in entering_up if e.is_symmetric()
                                                                 ] + [sample.GetHistogram()] + [e.final_up_histo() for e in entering_up]

        ymin, ymax = pu.GetFancyAxisRanges(hist_list, Options)
        if math.fabs(ymin) <= 1.e-10 and math.fabs(ymax) <= 1.e-10:
            logging.info("Night time %f day time %f" % (ymin, ymax))
            continue

        ### format the nominal sample
        sample.SetFillStyle(0)
        sample.SetFillColor(ROOT.kWhite)
        sample.SetLineColor(ROOT.kBlack)
        sample.GetHistogram().SetTitle("Nominal")

        # try to use non-ugly colormap provided by colorbrewer
        # http://colorbrewer2.org/?type=qualitative&scheme=Paired&n=6#type=qualitative&scheme=Paired&n=8
        color_map = {
            0: [ROOT.TColor.GetColor("#a6cee3"), ROOT.TColor.GetColor("#1f78b4")],
            1: [ROOT.TColor.GetColor("#b2df8a"), ROOT.TColor.GetColor("#33a02c")],
            2: [ROOT.TColor.GetColor("#fb9a99"), ROOT.TColor.GetColor("#e31a1c")],
            3: [ROOT.TColor.GetColor("#fdbf6f"), ROOT.TColor.GetColor("#ff7f00")]
        }

        ### Format the systematic histograms
        for i, syst in enumerate(entering_dn):
            syst.final_dn_histo().SetLineStyle(2)
            syst.final_dn_histo().SetLineWidth(3)
            syst.final_dn_histo().SetLineColor(color_map[i][0])
            syst.final_dn_histo().SetFillStyle(0)
            pu.AddToLegend(syst.final_dn_histo().get(), Style="FL")
        pu.AddToLegend(sample, Style="FL")

        ### Format the systematic histograms
        for i, syst in enumerate(entering_up):
            syst.final_up_histo().SetLineStyle(7)
            syst.final_up_histo().SetLineWidth(3)

            syst.final_up_histo().SetLineColor(color_map[i][1])
            syst.final_up_histo().SetFillStyle(0)
            if syst.is_symmetric():
                syst.final_dn_histo().SetLineColor(color_map[i][1])
                syst.final_dn_histo().SetFillStyle(0)
                syst.final_dn_histo().SetLineStyle(7)
                syst.final_dn_histo().SetLineWidth(3)

            pu.AddToLegend(syst.final_up_histo().get(), Style="FL")

        pu.drawStyling(sample, ymin, ymax, RemoveLabel=True, TopPad=True)
        pu.drawSumBg(sample, DrawSyst=True, DrawOnStack=False)
        for h in hist_list:
            h.Draw("histSAME")

        region_label = region if len(Options.regionLabel) == 0 else Options.regionLabel
        ### Draw the styling and some information
        pu.DrawPlotLabels(0.195, 0.83, "%s -- %s" % (region_label, sample.GetTitle()), analysis, Options.noATLAS)
        pu.DrawLegend(NperCol=Options.EntriesPerLegendColumn)

        pu.GetTopPad().RedrawAxis()

        ### now let's draw the ratio panel
        pu.GetBottomPad().cd()
        pu.GetBottomPad().cd()
        pu.GetBottomPad().SetGridy()

        ratio_histos = CreateRatioHistos([e.final_dn_histo()
                                          for e in entering_dn] + [e.final_dn_histo() for e in entering_up if e.is_symmetric()] +
                                         [e.final_up_histo() for e in entering_up], sample)

        ymin, ymax = pu.GetFancyAxisRanges(ratio_histos, Options, doRatio=True)
        pu.drawRatioStyling(sample, ymin, ymax, "Relative deviation")
        pu.drawRatioErrors(sample, DrawSyst=True, DrawSysLegend=False)
        for r in ratio_histos:
            r.Draw("histSAME")

        ROOT.gPad.RedrawAxis()
        ### Save the plot to disk
        plot_name = "SystBreakDown_%s_%s_%s_%s%s" % (sample.GetAnalysis(), sample.GetRegion(), sample.GetVariableName(), sample.GetName(),
                                                     logstr)
        if not Options.summaryPlotOnly:
            pu.saveHisto("%s/%s" % (Options.outputDir, plot_name), Options.OutFileType)

        pu.GetTopPad().cd()
        pu.DrawTLatex(0.5, 0.94, "%s%s in %s (%s analysis)" % (var, logstr, region, analysis), 0.04, 52, 22)
        pu.GetCanvas().SaveAs("%s/AllBreakDownPlots%s.pdf" % (Options.outputDir, bonusstr))
        once_plotted = True
    return once_plotted


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='This script produces the systematic break down plots for each MC component \"python SystBreakDownPlots.py -h\"',
        prog='MCPlots',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.add_argument("--samplesOnly", help="Restrict the systematic break down only on the samples with name x", default=[], nargs="+")
    parser.add_argument("--leadingNSyst", help="Plot the leading N systematics", default=4, type=int)
    parser.set_defaults(EntriesPerLegendColumn=5)
    parser.set_defaults(lumi=139)
    parser.set_defaults(OutFileType=["pdf"])

    parser.set_defaults(LegendXCoords=[0.55, 0.94])
    parser.set_defaults(LegendYCoords=[0.5, 0.9])

    PlottingOptions = parser.parse_args()
    CreateDirectory(PlottingOptions.outputDir, False)

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = GetStructure(PlottingOptions)
    if PlottingOptions.noSyst or len(GetSystPairer().get_systematics()) < 2:
        logging.error(
            "This is systematic break down plots. Could you explain how this script can fulfil its destiny if no systematics are present?")
        exit(1)

    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")
    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)

    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            bonusstr = '_%s_%s' % (ana, region)
            if PlottingOptions.doLogY:
                bonusstr += '_LogY'
            bonusstr = RemoveSpecialChars(bonusstr)
            dummy.SaveAs("%s/AllBreakDownPlots%s.pdf[" % (PlottingOptions.outputDir, bonusstr))
            Drawn = False
            for var in FileStructure.GetConfigSet().GetVariables(ana, region):
                Drawn = drawHisto(PlottingOptions, ana, region, var, bonusstr=bonusstr) or Drawn
            if Drawn:
                dummy.SaveAs("%s/AllBreakDownPlots%s.pdf]" % (PlottingOptions.outputDir, bonusstr))
            else:
                os.system("rm %s/AllBreakDownPlots%s.pdf" % (PlottingOptions.outputDir, bonusstr))
