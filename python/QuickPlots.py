#! /usr/bin/env python
import ROOT, os, argparse
from ClusterSubmission.Utils import CreateDirectory
objects = []


def setupArgParser():
    parser = argparse.ArgumentParser(description='This scripts casts all TH1 in a ROOT file into a single pdf',
                                     prog='QuickPlots',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--inFile", required=True, help="The path of the ROOT file you want to plot")
    parser.add_argument("--outFile", required=True, help="Where shall the output be casted to")
    return parser


def DrawTLatex(x, y, text, size=18, font=43, align=11):
    tex = ROOT.TLatex()
    tex.SetTextAlign(align)
    tex.SetTextSize(size)
    tex.SetTextFont(font)
    tex.SetNDC()
    tex.DrawLatex(x, y, text)
    objects.append(tex)


def GetHistoPaths(TFile, Directory=""):
    Paths = []
    if Directory.startswith("/"): Directory = Directory[1:len(Directory)]
    TDir = TFile if len(Directory) == 0 else TFile.Get(Directory)
    for Key in TDir.GetListOfKeys():
        KeyName = Key.GetName()
        if TDir.Get(KeyName).InheritsFrom("TDirectory"):
            Paths.extend(GetHistoPaths(TFile, "%s/%s" % (Directory, KeyName)))
        elif TDir.Get(KeyName).InheritsFrom("TH1"):
            if len(Directory) > 0: Paths.append("%s/%s" % (Directory, KeyName))
            else: Paths.append(KeyName)
    return Paths


if __name__ == "__main__":

    Options = setupArgParser().parse_args()
    if not os.path.isfile(Options.inFile):
        print "ERROR: No such file or directory " + Options.inFile
        exit(1)

    TFile = ROOT.TFile(Options.inFile, "READ")
    if not TFile.IsOpen():
        print "ERROR: Could not open " + Options.inFile
        exit(1)

    DummyCanvas = ROOT.TCanvas("dummy", "dummy", 800, 600)
    if Options.outFile.find("/") != -1:
        CreateDirectory(Options.outFile[:Options.outFile.find("/")], False)

    DummyCanvas.SaveAs("%s.pdf[" % (Options.outFile))
    ######################################################################
    #         Loop over all keys of the file and see what's in there     #
    ######################################################################
    ROOT.gStyle.SetPaintTextFormat(".3f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetPalette(55)
    ROOT.gROOT.SetBatch()
    ROOT.gStyle.SetOptStat(0)
    PlotDrawn = False
    for Key in GetHistoPaths(TFile):
        ##################################################
        # We've found a TH1Histogram. So lets plot it    #
        ##################################################
        Histo = TFile.Get(Key)
        if Histo.GetEntries() == 0: continue

        Canvas = ROOT.TCanvas("C1" + Key.replace("/", "_"), Key, 800, 600)
        Canvas.cd()
        if Histo.GetDimension() == 1: Histo.Draw("HIST")
        elif Histo.GetDimension() == 2: Histo.Draw("COLZl")
        else: continue
        DrawTLatex(0.45, 0.94, "%s in %s " % (Key, Options.inFile), 0.04, 52, 22)

        Canvas.SaveAs("%s.pdf" % (Options.outFile))
        PlotDrawn = True

    DummyCanvas.SaveAs("%s.pdf]" % (Options.outFile))
    if not PlotDrawn: os.system("rm %s.pdf" % (Options.outFile))
