#ifndef XAMPPPLOTTING_IFDEFHELPERS_H
#define XAMPPPLOTTING_IFDEFHELPERS_H
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace XAMPP {
    typedef std::vector<std::string> StringVector;
    typedef std::shared_ptr<StringVector> StringVector_Ptr;

    class IfDefVariable;
    typedef std::shared_ptr<IfDefVariable> Variable_Ptr;

    class IfDefVariable {
    public:
        IfDefVariable(const std::string& name, const StringVector& values);

        std::string name() const;
        bool Next(std::string& line);
        bool isValid() const;
        unsigned int nLines() const;
        std::string getFirstElement() const;
        const StringVector& values() const;
        void clearRegister();
        void Print() const;

    private:
        std::string m_name;
        StringVector m_lines;
        Variable_Ptr m_cross_ref;
        bool m_isValid;
        size_t m_LineToRead;
    };

    // Helper class to read ifdef/endif statements
    class IfDefFlags {
    public:
        static IfDefFlags* Instance();
        void AddFlag(const std::string& Flag);
        bool IsFlagDefined(const std::string& Flag) const;
        bool IsVariableDefined(const std::string& variable) const;
        bool DefineVariable(const std::string& varname, const std::vector<std::string>& values);

        Variable_Ptr RetrieveVariable(const std::string& varName) const;
        ~IfDefFlags();
        bool HasVariableCached(std::string& line);
        bool CacheVariable(const std::string& name);
        bool allVarsValid() const;

    protected:
        static IfDefFlags* m_Inst;
        IfDefFlags();
        IfDefFlags(const IfDefFlags&) = delete;
        void operator=(const IfDefFlags&) = delete;

    private:
        StringVector m_Flags;
        std::vector<Variable_Ptr> m_Variables;
        Variable_Ptr m_VarToRead;
    };

    class IfDefLineParser {
    public:
        IfDefLineParser();
        enum LineStatus { NewProperty, EndOfFile, ReadError };
        LineStatus operator()(std::ifstream& inf, std::string& line);
        LineStatus read(std::ifstream& inf, std::string& line);
        void AddFlag(const std::string& Flag);

    private:
        bool SkipLines(std::ifstream& inf, std::string& line);
        /// Returns 0 if the lines does not need to be skipped
        /// 1 if there is a skip statement
        /// and -1 if an endif statement is found
        int SkipLine(const std::string& line);

        bool m_InErrorState;
        bool m_ResolveVars;
        bool m_GlobalSkip;
    };
}  // namespace XAMPP

#endif
