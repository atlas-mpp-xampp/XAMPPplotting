#ifndef XAMPPPLOTTING_LUMICALCULATOR_H
#define XAMPPPLOTTING_LUMICALCULATOR_H

#include <memory>
#include <string>
#include <vector>
namespace CP {
    class PileupReweightingTool;
}
namespace XAMPP {
    // Helper class to calculate the luminosity from the prwTool in python
    class LumiCalculator {
    public:
        static LumiCalculator* getCalculator();
        // Functions to calculate the Luminosity using the prwTool maintained by the Weighter
        double getExpectedLumiFromRun(unsigned int runNumber) const;
        double getExpectedLumiFromRun(unsigned int run, unsigned int block) const;
        double getRecordedLumiFromRun(unsigned int runNumber) const;

        double getExpectedLumi(unsigned int begin, unsigned int end) const;
        double getRecordedLumi(unsigned int begin, unsigned int end) const;
        bool isGoodLumiBlock(unsigned int run, unsigned int block) const;
        std::vector<unsigned int> good_runs() const;
        // Helper functinos to initialize  the pileup tool -> wrapper of the Weighter
        bool InitPileUpTool();

        void SetPRWlumi(const std::vector<std::string>& lumi);
        void SetPRWconfig(const std::vector<std::string>& config);
        // Retrieves the IlumiCalc/MC config files used for the PRW tool
        const std::vector<std::string>& GetPRWconfig() const;
        const std::vector<std::string>& GetPRWlumi() const;
        std::shared_ptr<CP::PileupReweightingTool> GetPileUpTool() const;

        unsigned int GetNLumiBlocks(unsigned int run) const;
        ~LumiCalculator();

    private:
        static LumiCalculator* m_Inst;
        typedef std::pair<unsigned int, unsigned int> Run_Nblocks;

        std::vector<Run_Nblocks> m_recorded_runs;

        std::shared_ptr<CP::PileupReweightingTool> m_prwTool;
        std::vector<std::string> m_PRWlumiFiles;
        std::vector<std::string> m_PRWconfigFiles;

        LumiCalculator();
        LumiCalculator(const LumiCalculator&) = delete;
        void operator=(const LumiCalculator&) = delete;

        void ReadLumiCalcFile(const std::string& path, std::vector<LumiCalculator::Run_Nblocks>& runset) const;
    };

    // Helper class to slice the total recorded luminosity in
    // blocks of equal luminosity
    class LumiInterval;
    class LumiSlicer {
    public:
        // Size of each lumiInterval in pb^{-1}
        LumiSlicer(double slice_size);
        // Slice the luminosity per run
        LumiSlicer();
        unsigned int find_bin(unsigned int run, unsigned int lumi_block) const;
        std::shared_ptr<LumiInterval> get_block(unsigned int bin) const;

        size_t size() const;

    private:
        std::vector<std::shared_ptr<LumiInterval>> m_blocks;
    };
    class LumiInterval {
    public:
        LumiInterval(unsigned int bin);

        void set_start(unsigned int run, unsigned int lumi_block);
        void set_end(unsigned int run, unsigned int lumi_block);

        unsigned int bin_number() const;
        bool in_interval(unsigned int run, unsigned int lumi_block) const;

        unsigned int start_run() const;
        unsigned int end_run() const;

        unsigned int start_block() const;
        unsigned int end_block() const;

        void set_lumi(double l);
        double lumi() const;

    private:
        unsigned int m_start_run;
        unsigned int m_start_block;
        unsigned int m_end_run;
        unsigned int m_end_block;
        double m_lumi;
        unsigned int m_bin;
    };

}  // namespace XAMPP
#endif
