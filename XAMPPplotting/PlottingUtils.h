#ifndef XAMPPPLOTTING_PLOTTINGUTILS_H
#define XAMPPPLOTTING_PLOTTINGUTILS_H

#include <XAMPPplotting/Cuts.h>

#include <cfloat>
#include <climits>
#include <cmath>
#include <fstream>
#include <future>
#include <iostream>
#include <memory>
#include <set>
#include <string>
#include <thread>
#include <vector>

class TH1;
class TAxis;
class TFile;
class TDirectory;
namespace XAMPP {

    typedef std::pair<int, int> Ranges;
    typedef std::pair<Ranges, Ranges> Cube;
    // Assign a constant to -1 casted to size_t
    constexpr size_t reset_idx = -1;

    class Condition;
    class ITreeVarWriter;
    class Histo;

    void PrintHeadLine(const std::string &Title);
    void PrintFooter();

    /// returns whether a given element is part of the vector
    template <typename T> bool IsElementInList(const std::vector<T> &List, const T &Element);

    ///     From: source vector
    ///     To: destination vector
    ///     func: Decider when the element should be appended
    ///     Clear: Clear the vector beforehand
    template <typename T>
    void CopyVector(const std::vector<T> &From, std::vector<T> &To, std::function<bool(const T &)> func, bool Clear = false);

    /// Creates a copy of the vector excluding duplicate entries
    template <typename T> void CopyVector(const std::vector<T> &From, std::vector<T> &To, bool Clear = false);

    template <typename T> void CopyVector(const std::vector<T> &From, std::set<T> &To, bool Clear = false);

    /// Removes all Duplicate entries from a vector
    template <typename T> void ClearFromDuplicates(std::vector<T> &toClear);

    /// Counts how many elements in a given iterator range are satisfying the
    /// functional condition
    template <typename T>
    unsigned int count(typename std::vector<T>::const_iterator begin, typename std::vector<T>::const_iterator end,
                       std::function<bool(const T &)> func);
    /// Counts how many elements in the vector are satisfying the condition
    template <typename T> unsigned int count(const std::vector<T> &vector, std::function<bool(const T &)> func);

    /// Returns the number of active threads
    template <typename T> unsigned int count_active(const std::vector<std::future<T>> &threads);

    /// Removes elements from a vector satisfying the selection function conditions
    template <typename T> void EraseFromVector(std::vector<T> &vec, std::function<bool(const T &)> func);
    /// Removes the given element from the list
    template <typename T> void RemoveElement(std::vector<T> &Vec, const T &Ele);

    /// std::cout operator for vectors
    template <class T> std::ostream &operator<<(std::ostream &os, const std::vector<T> &v);
    /// std::cout operator for pairs
    template <class T, class U> std::ostream &operator<<(std::ostream &os, const std::pair<T, U> &e);
    /// Two vectors are the same if each element is the same
    template <class T> bool operator==(const std::vector<T> &v1, const std::vector<T> &v2);

    /// Returns the most left bit set
    template <typename T> int max_bit(const T &number);
    /// Returns the most right bit set
    template <typename T> int min_bit(const T &number);

    constexpr int MsgMethodChars = 35;
    template <class... Args> void Info(const std::string &method, const std::string &info_str, Args &&...args);
    template <class... Args> void Warning(const std::string &method, const std::string &info_str, Args &&...args);
    template <class... Args> void Error(const std::string &method, const std::string &info_str, Args &&...args);

    /// Pipes the next word from a stringstream into a string.
    /// Reset: The internal stringstream pointer is set to the beginning of the current word
    std::string GetWordFromStream(std::stringstream &sstr, bool Reset = false);
    /// Interprets the next word as int
    int GetIntegerFromStream(std::stringstream &sstr, bool Reset = false);
    /// Interprets the next word as float
    float GetFloatFromStream(std::stringstream &sstr, bool Reset = false);
    /// Interprets the next word as double
    double GetDoubleFromStream(std::stringstream &sstr, bool Reset = false);

    /// Truncate the double to a given decimal precision
    double truncate(const double &dbl, unsigned int prec);
    /// Checks if the next word in the string stream is the keyword.
    /// If yes the stream readout pointer is set to the next element in the stream
    bool IsKeyWordSatisfied(std::stringstream &sstr, const std::string &word);
    /// Checks if in the next word read by the stringstream contains the is_in
    bool IsInNextWord(std::stringstream &sstr, const std::string &is_in);

    /// Reads out the stringstreams into the end and pipes it into vectors
    void ExtractListFromStream(std::stringstream &sstr, std::vector<std::string> &List, bool ClearList = false);
    void ExtractListFromStream(std::stringstream &sstr, std::vector<int> &List, bool ClearList = false);
    void ExtractListFromStream(std::stringstream &sstr, std::vector<float> &List, bool ClearList = false);

    // File opening functions
    std::shared_ptr<TFile> Open(const std::string &Path);

    void PrintHistogram(TH1 *H);

    // Histogram manipulation
    unsigned int GetNbins(const std::shared_ptr<TH1> &Histo);
    unsigned int GetNbins(const TH1 *Histo);

    bool isOverflowBin(const std::shared_ptr<TH1> &Histo, int bin);
    bool isOverflowBin(const TH1 *Histo, int bin);
    bool isOverflowBin(const TAxis *a, int bin);

    bool isAlphaNumeric(std::shared_ptr<TH1> histo);
    bool isAlphaNumeric(TH1 *histo);

    double PlotRange(const TAxis *a);

    double GetBinNormalization(const TH1 *H1, int Bin);
    double GetBinNormalization(const std::shared_ptr<TH1> &H1, int Bin);

    // Integral functions which take into account that the histogram is normalized to the first bin
    double Integrate(const std::shared_ptr<TH1> &Histo, unsigned int xStart = 0, int xEnd = -1, unsigned int yStart = 0, int yEnd = -1,
                     unsigned int zStart = 0, int zEnd = -1);
    double Integrate(const TH1 *Histo, unsigned int xStart = 0, int xEnd = -1, unsigned int yStart = 0, int yEnd = -1,
                     unsigned int zStart = 0, int zEnd = -1);

    std::pair<double, double> IntegrateAndError(const std::shared_ptr<TH1> &Histo, unsigned int xStart = 0, int xEnd = -1,
                                                unsigned int yStart = 0, int yEnd = -1, unsigned int zStart = 0, int zEnd = -1);
    std::pair<double, double> IntegrateAndError(const TH1 *Histo, unsigned int xStart = 0, int xEnd = -1, unsigned int yStart = 0,
                                                int yEnd = -1, unsigned int zStart = 0, int zEnd = -1);

    double GetMinimum(const std::shared_ptr<TH1> &histo, double min_value = -DBL_MAX);
    double GetMinimum(const TH1 *histo, double min_value = DBL_MIN);
    double GetMaximum(const std::shared_ptr<TH1> &histo, double max_value = DBL_MAX);
    double GetMaximum(const TH1 *histo, double max_value = DBL_MAX);

    int GetMinimumBin(const std::shared_ptr<TH1> &histo, double min_value = -DBL_MAX);
    int GetMinimumBin(const TH1 *histo, double min_value = DBL_MIN);
    int GetMaximumBin(const std::shared_ptr<TH1> &histo, double max_value = DBL_MAX);
    int GetMaximumBin(const TH1 *histo, double max_value = DBL_MAX);

    std::shared_ptr<TH1> clone(const std::shared_ptr<TH1> &histo, bool reset = false);
    std::shared_ptr<TH1> clone(const TH1 *histo, bool reset = false);

    std::shared_ptr<TH1> ProjectInto1D(const std::shared_ptr<TH1> &H3, unsigned int project_along, int bin1_start = 0, int bin1_end = -1,
                                       int bin2_start = 0, int bin2_end = -1);
    std::shared_ptr<TH1> ProjectInto1D(const TH1 *H3, unsigned int project_along, int bin1_start = 0, int bin1_end = -1, int bin2_start = 0,
                                       int bin2_end = -1);

    std::shared_ptr<TH1> ProjectInto2D(const std::shared_ptr<TH1> &H3, unsigned int project_along, int bin_start, int bin_end);
    std::shared_ptr<TH1> ProjectInto2D(const TH1 *H3, unsigned int project_along, int bin_start, int bin_end);

    std::shared_ptr<TH1> MakeTH1(std::vector<double> BinEdgesX, std::vector<double> BinEdgesY = std::vector<double>(),
                                 std::vector<double> BinEdgesZ = std::vector<double>());

    /// Rebins the histogram such that each bin has a relative error less than max_stat_error
    std::shared_ptr<TH1> ReBinHisto(const TH1 *H, double max_stat_error);
    std::shared_ptr<TH1> ReBinHisto(const std::shared_ptr<TH1> &H, double max_stat_error);

    void CopyStyling(const TH1 *from, TH1 *to);
    /// Tansmutes the binning from one shape into another
    bool TransformToBinning(const TH1 *from, TH1 *to);

    /// Extracts the binning from the Axis including high and low edges
    std::vector<double> ExtractBinning(const TAxis *axis);

    bool IsFinite(const std::shared_ptr<TH1> &histo);
    bool IsFinite(const TH1 *histo);

    std::string TimeHMS(float t);
    std::string SeparationLine();
    std::string WhiteSpaces(int N = 0, std::string space = " ");
    std::string EraseWhiteSpaces(std::string str);
    std::string LongestCommonString(const std::string &str1, const std::string &str2);
    std::string RandomString(size_t s);

    std::shared_ptr<Condition> CreateCut(ITreeVarReader *Var, std::stringstream &sstr);
    std::shared_ptr<Condition> CreateCut(const std::string &line, std::vector<ITreeVarReader *> &Readers);

    Condition::Mode GetCombineModeFromLine(const std::string &line);
    std::shared_ptr<Condition> CreateCombinedCut(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers, const std::string &line);
    std::shared_ptr<Condition> CreateCombinedCut(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers, Condition::Mode M);

    std::shared_ptr<Histo> CreateHisto(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers);
    ITreeVarWriter *CreateOutBranch(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers);

    std::vector<std::string> ListDirectory(std::string Path, std::string WildCard);
    std::vector<std::string> GetPathResolvedFileList(const std::vector<std::string> &Files);
    std::string ResolvePath(const std::string &path);
    bool DoesFileExist(const std::string &Path);
    bool DoesDirectoryExist(const std::string &Path);

    bool GetLine(std::ifstream &inf, std::string &line);

    bool IsFinite(double N);
    int gcd(int a, int b);

    std::string ReplaceExpInString(std::string str, const std::string &exp, const std::string &rep);
    std::string ResolveEnviromentVariables(std::string str);

    Ranges GetRangesFromStr(std::string str);
    Cube GetMatrixRangesToRead(std::string &Instr);
    std::string GetMatrixRangesFromCube(Cube C);

    unsigned int binomial(unsigned int n, unsigned int m);

    std::vector<size_t> sequence(size_t to, size_t start = 0);

    TDirectory *mkdir(const std::string &name, TDirectory *in_dir);
    std::shared_ptr<TH1> fetch_from_file(const std::string &path, TDirectory *in_dir);
    std::vector<std::string> recursive_ls(TDirectory *d, const std::string &pre_path = "");
    bool isElementSubstr(const std::vector<std::string> &vec, const std::string &str);

    bool same_binning(const std::shared_ptr<TH1> &H1, const std::shared_ptr<TH1> &H2);

}  // namespace XAMPP
#include <XAMPPplotting/PlottingUtils.ixx>
#endif
