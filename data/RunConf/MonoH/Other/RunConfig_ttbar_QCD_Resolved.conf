### This file is the RunConfig for the mono-H(bb) analysis for the XAMPPplotting package
###

## --------------------------------------------------------------------------
## Basic definitions
## --------------------------------------------------------------------------

### Lets tell the code the name of the tree in the input files i.e. SUSYAnalysisConfg_******
Tree MonoH

### This option defines the name of the top directories in the output histogram files.
### For the Tree files the name is taken from the SampleName property in the InputConfig

Analysis MonoH

# Create CutFlow histogram (needed for yields+significances!!)
doCutFlow

# Disable pile-up reweighting
#noPRW

# Disable systematics
noSyst

# Cross-section and cross-section files
# disableXS
xSecDir XAMPPplotting/xSecFiles/MonoH

# The weights which should be read from the trees. DO NOT add the PileUp weight to the list in any case
Weights GenWeight EleWeight MuoWeight JetWeight
#EleWeight MuoWeight JetWeight 

# Possible weights
# Weights GenWeight EleWeight MuoWeight JetWeight TrackJetWeight


## --------------------------------------------------------------------------
## Define Readers
## --------------------------------------------------------------------------

New_ParticleM JetReader Jet 
    Var char bjet
End_Particle

# New_ParticleM BJetReader Jet 
#     Cut char bjet = 1
# End_Particle

# New_ParticleM LightJetReader Jet 
#     Cut char bjet = 0
# End_Particle

New_ParticleM ForwardJetReader ForwardJet 
End_Particle

New_ParticleM FatJetReader FatJet 
    Var int n_matchedasstrkjets
    Var int n_asstrkjets
End_Particle

New_ParticleM TrackJetReader TrackJet 
End_Particle
 
New_ParticleM BaselineMuons Muon 
    Var float charge
End_Particle

New_ParticleM BaselineElectrons Electron 
    Var float charge
End_Particle

New_ParticleM SignalTaus Tau
End_Particle


## --------------------------------------------------------------------------
## Define regions
## --------------------------------------------------------------------------


## Baseline resolved event requirements
## --------------------------------------------------------------------------

## Cuts already implemented in XAMPPmonoH on skimming level
# Event requirements
# EvCut char PassGRL = 1
# EvCut char HasVtx = 1

# MET trigger
# CombCut AND
#    EvCut char IsMETTrigPassed = 1
#    EvCut char Trigger = 1
# End_CombCut

# EvCut int BadJet = 0
# EvCut int CosmicMuon = 0
# EvCut int BadMuon = 0

# Lepton Veto
# EvCut int N_SignalLeptons = 0

# Minimal MET cut
# EvCut floatGeV MetTST_met > 150.

# At least 2 small-R jets
# EvCut int N_Jets04 >= 2

# Resolved selection MET cut
EvCut floatGeV MetTST_met <= 500.

# Anti-QCD cuts

EvCut float DeltaPhiJJ  |<=| 2.44346095279  # not present in the table in the paper but in the event selection and also used in the 2016 multijet estimate
# The remaining anti-QCD cuts are not implemented here but in each region individually. They are listed as comments for the sake of completenes.
# EvCut float DeltaPhiMin3 |>=| 0.34906585039
# EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
# EvCut float DeltaRJJ < 1.8

# Resolved event selection
CombCut OR
    EvCut floatGeV Jet_BLight1BPt > 45.
    EvCut floatGeV Jet_BLight2BPt > 45.
End_CombCut

CombCut OR
    CombCut AND
        EvCut int N_Jets04 = 2
        EvCut floatGeV sigjet012ptsum > 120.
    End_CombCut    
    CombCut AND
        EvCut int N_Jets04 > 2
        EvCut floatGeV sigjet012ptsum > 150.
    End_CombCut
End_CombCut


EvCut int N_SignalTaus = 0
EvCut int N_TausExtended_Resolved = 0
EvCut int N_BJets_04 <= 2
EvCut float HtRatioResolved <= 0.37


# Signal Regions (few QCD, with anti-QCD cut)
# -> these are used in the final fit
# --------------------------------------------------------------------------

# Resolved SR 0tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region SR_150_200_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirements
EvCut float DeltaPhiMin3 |>=| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8

# Resolved SR 0tag 200 - 350 GeV
# --------------------------------------------------------------------------
Region SR_200_350_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirements
EvCut float DeltaPhiMin3 |>=| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


#  Resolved SR 0tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region SR_350_500_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirements
EvCut float DeltaPhiMin3 |>=| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved SR 1tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region SR_150_200_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 1

# anti-QCD requirements
EvCut float DeltaPhiMin3 |>=| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved SR 1tag 200 - 350 GeV 
# --------------------------------------------------------------------------
Region SR_200_350_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirements
EvCut float DeltaPhiMin3 |>=| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved SR 1tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region SR_350_500_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 1

# anti-QCD requirements
EvCut float DeltaPhiMin3 |>=| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved SR 2tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region SR_150_200_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirements
EvCut float DeltaPhiMin3 |>=| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved SR 2tag 200 - 350 GeV
# --------------------------------------------------------------------------
Region SR_200_350_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirements
EvCut float DeltaPhiMin3 |>=| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved SR 2tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region SR_350_500_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirements
EvCut float DeltaPhiMin3 |>=| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Control Regions (much QCD, reversed DeltaPhiMin3 cut but still other anti-QCD cuts)
# -> these are used to get the template shape
# --------------------------------------------------------------------------

# Resolved CR 0tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region CR_150_200_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirements
EvCut float DeltaPhiMin3 |<| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8

# Resolved CR 0tag 200 - 350 GeV
# --------------------------------------------------------------------------
Region CR_200_350_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirements
EvCut float DeltaPhiMin3 |<| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


#  Resolved CR 0tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region CR_350_500_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirements
EvCut float DeltaPhiMin3 |<| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved CR 1tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region CR_150_200_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 1

# anti-QCD requirements
EvCut float DeltaPhiMin3 |<| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved CR 1tag 200 - 350 GeV 
# --------------------------------------------------------------------------
Region CR_200_350_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirements
EvCut float DeltaPhiMin3 |<| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved CR 1tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region CR_350_500_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 1

# anti-QCD requirements
EvCut float DeltaPhiMin3 |<| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved CR 2tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region CR_150_200_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirements
EvCut float DeltaPhiMin3 |<| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved CR 2tag 200 - 350 GeV
# --------------------------------------------------------------------------
Region CR_200_350_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirements
EvCut float DeltaPhiMin3 |<| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8


# Resolved CR 2tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region CR_350_500_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirements
EvCut float DeltaPhiMin3 |<| 0.34906585039
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239
EvCut float DeltaRJJ < 1.8




# QCD Signal Regions (few QCD, with relaxed anti-QCD cuts, blinded)
# -> these are used for getting the template 
#    in the process of determining the template normalisation
# --------------------------------------------------------------------------

# Resolved QCDSR 0tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region QCDSR_150_200_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDSR 0tag 200 - 350 GeV
# --------------------------------------------------------------------------
Region QCDSR_200_350_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

#  Resolved QCDSR 0tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region QCDSR_350_500_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDSR 1tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region QCDSR_150_200_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 1

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDSR 1tag 200 - 350 GeV 
# --------------------------------------------------------------------------
Region QCDSR_200_350_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDSR 1tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region QCDSR_350_500_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 1

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDSR 2tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region QCDSR_150_200_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDSR 2tag 200 - 350 GeV
# --------------------------------------------------------------------------
Region QCDSR_200_350_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDSR 2tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region QCDSR_350_500_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# QCD Control Regions (much QCD, reversed and relaxed anti-QCD cuts, blinded)
# -> these are used for doing the template normalisation fit
#    in the process of determining the template normalisation
# --------------------------------------------------------------------------

# Resolved QCDCR 0tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region QCDCR_150_200_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDCR 0tag 200 - 350 GeV
# --------------------------------------------------------------------------
Region QCDCR_200_350_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

#  Resolved QCDCR 0tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region QCDCR_350_500_0tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 0

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDCR 1tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region QCDCR_150_200_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 1

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDCR 1tag 200 - 350 GeV 
# --------------------------------------------------------------------------
Region QCDCR_200_350_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDCR 1tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region QCDCR_350_500_1tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 1

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDCR 2tag 150 - 200 GeV 
# --------------------------------------------------------------------------
Region QCDCR_150_200_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 150.
  EvCut floatGeV MetTST_met < 200.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut

# Resolved QCDCR 2tag 200 - 350 GeV
# --------------------------------------------------------------------------
Region QCDCR_200_350_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 200.
  EvCut floatGeV MetTST_met < 350.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut


# Resolved QCDCR 2tag 350 - 500 GeV
# --------------------------------------------------------------------------
Region QCDCR_350_500_2tag

# MET cut
CombCut AND
  EvCut floatGeV MetTST_met >= 350.
  EvCut floatGeV MetTST_met < 500.
End_CombCut

# b-tags
EvCut int N_BJets_04 = 2

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039

# blinding
CombCut OR
  EvCut floatGeV m_jj >= 140
  EvCut floatGeV m_jj < 70.
End_CombCut
