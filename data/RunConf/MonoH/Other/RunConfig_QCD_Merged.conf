## This file is the RunConfig for the mono-h(bb) analysis
## for the merged QCD estimation regions. It extends RunConfig.conf
## where all the common options are already defined.

Import XAMPPplotting/RunConf/MonoH/RunConfig.conf

##############################################################################

## --------------------------------------------------------------------------
## Define regions
## --------------------------------------------------------------------------

## Note: this file does not make use of the common resolved region definitions
##       because of some loosened cuts for the QCD estimation.

## Baseline merged event requirements
## --------------------------------------------------------------------------

## Cuts already implemented in XAMPPmonoH on skimming level
# Event requirements
# EvCut char PassGRL = 1
# EvCut char HasVtx = 1

# MET trigger
# CombCut AND
#     EvCut char IsMETTrigPassed = 1
#     EvCut char Trigger = 1
# End_CombCut

# EvCut int BadJet = 0
# EvCut int CosmicMuon = 0
# EvCut int BadMuon = 0

# Lepton Veto
# EvCut int N_SignalLeptons = 0

# Minimal MET cut (redundant)
# EvCut floatGeV MetTST_met > 150.

# at least 1 large-R jet
# EvCut int N_Jets10 >= 1


## Cuts implemented in XAMPP plotting
# MET cut
EvCut floatGeV MetTST_met > 500.

# Anti-QCD cuts

# DeltaPhiMin3 cut missing here, is used below
# EvCut float DeltaPhiMin3 |>=| 0.34906585039

# Merged event selection
EvCut int N_SignalTaus = 0
EvCut int N_TausExtended_Merged = 0
EvCut int N_BTags_not_associated_02 = 0
EvCut float HtRatioMerged <= 0.57


# Signal Regions (few QCD, with anti-QCD cut)
# -> these are used in the final fit
# --------------------------------------------------------------------------

# Merged SR 0tag
# --------------------------------------------------------------------------
Region SR_Merged_0btags

# b-tags
EvCut int N_BTags_associated_02 = 0

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039


# Merged SR 1tag
# --------------------------------------------------------------------------
Region SR_Merged_1btags

# b-tags
EvCut int N_BTags_associated_02 = 1

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039


# Merged SR 2tag
# --------------------------------------------------------------------------
Region SR_Merged_2btags

# b-tags
EvCut int N_BTags_associated_02 = 2

# anti-QCD requirement
EvCut float DeltaPhiMin3 |>=| 0.34906585039



# Control Regions (much QCD, reversed DeltaPhiMin3 cut but still other anti-QCD cuts)
# -> these are used to get the template shape
# --------------------------------------------------------------------------

# Merged CR 0tag
# --------------------------------------------------------------------------
Region CR_Merged_0btags

# b-tags
EvCut int N_BTags_associated_02 = 0

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039


# Merged CR 1tag
# --------------------------------------------------------------------------
Region CR_Merged_1btags

# b-tags
EvCut int N_BTags_associated_02 = 1

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039


# Merged CR 2tag
# --------------------------------------------------------------------------
Region CR_Merged_2btags

# b-tags
EvCut int N_BTags_associated_02 = 2

# anti-QCD requirement
EvCut float DeltaPhiMin3 |<| 0.34906585039


# # QCD Signal Regions (few QCD, with relaxed anti-QCD cuts, blinded)
# # -> these are used for getting the template 
# #    in the process of determining the template normalisation
# # --------------------------------------------------------------------------

# # Merged QCDSR 0tag
# # --------------------------------------------------------------------------
# Region QCDSR_Merged_0btags

# # b-tags
# EvCut int N_BTags_associated_02 = 0

# # anti-QCD requirement
# EvCut float DeltaPhiMin3 |>=| 0.34906585039

# # blinding
# CombCut OR
#   EvCut floatGeV fatjets_m >= 140
#   EvCut floatGeV fatjets_m < 70.
# End_CombCut

# # Merged QCDSR 1tag
# # --------------------------------------------------------------------------
# Region QCDSR_Merged_1btags

# # b-tags
# EvCut int N_BTags_associated_02 = 1

# # anti-QCD requirement
# EvCut float DeltaPhiMin3 |>=| 0.34906585039

# # blinding
# CombCut OR
#   EvCut floatGeV fatjets_m >= 140
#   EvCut floatGeV fatjets_m < 70.
# End_CombCut

# # Merged QCDSR 2tag
# # --------------------------------------------------------------------------
# Region QCDSR_Merged_2btags

# # b-tags
# EvCut int N_BTags_associated_02 = 2

# # anti-QCD requirement
# EvCut float DeltaPhiMin3 |>=| 0.34906585039

# # blinding
# CombCut OR
#   EvCut floatGeV fatjets_m >= 140
#   EvCut floatGeV fatjets_m < 70.
# End_CombCut


# # QCD Control Regions (much QCD, reversed and relaxed anti-QCD cuts, blinded)
# # -> these are used for doing the template normalisation fit
# #    in the process of determining the template normalisation
# # --------------------------------------------------------------------------

# # Merged QCDCR 0tag
# # --------------------------------------------------------------------------
# Region QCDCR_Merged_0btags

# # b-tags
# EvCut int N_BTags_associated_02 = 0

# # anti-QCD requirement
# EvCut float DeltaPhiMin3 |<| 0.34906585039

# # blinding
# CombCut OR
#   EvCut floatGeV fatjets_m >= 140
#   EvCut floatGeV fatjets_m < 70.
# End_CombCut

# # Merged QCDCR 1tag
# # --------------------------------------------------------------------------
# Region QCDCR_Merged_1btags

# # b-tags
# EvCut int N_BTags_associated_02 = 1

# # anti-QCD requirement
# EvCut float DeltaPhiMin3 |<| 0.34906585039

# # blinding
# CombCut OR
#   EvCut floatGeV fatjets_m >= 140
#   EvCut floatGeV fatjets_m < 70.
# End_CombCut

# # Merged QCDCR 2tag
# # --------------------------------------------------------------------------
# Region QCDCR_Merged_2btags

# # b-tags
# EvCut int N_BTags_associated_02 = 2

# # anti-QCD requirement
# EvCut float DeltaPhiMin3 |<| 0.34906585039

# # blinding
# CombCut OR
#   EvCut floatGeV fatjets_m >= 140
#   EvCut floatGeV fatjets_m < 70.
# End_CombCut
