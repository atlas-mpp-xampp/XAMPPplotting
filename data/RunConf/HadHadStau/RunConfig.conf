###This file is an example RunConfig for the XAMPPplotting package
###
#Weights GenWeight EleWeight MuoWeight TauWeight MuoWeightIsol MuoWeightReco MuoWeightTTVA TauWeightEleOREle TauWeightId TauWeightReco
Weights GenWeight EleWeight MuoWeight TauWeight 
### Lets tell the code the name of the tree in the input files i.e. SUSYAnalysisConfg_******
Tree Staus
### This option defines the name of the top directories in the output histogram files.
### For the Tree files the name is taken from the SampleName property in the InputConfig
Analysis StauHadHad

#Systematics which you want to run over. If you do not specify it XAMPPplotting runs over all systematics stored
#Systematic Nominal

#NoSUSYpidWeight
#For testing it is quite usefull to switch off the systematics. Then XAMPPplotting only runs over the Nominal tree
noSyst
#noSystW
#noSystK

#Event weights for MonteCarlo to use. Please never add the MuWeight as the PileUp weight is treated separately in the tool
# Weights are included in the trigger conf files

#You can switch off the PileUpReweighting by uncommenting this line. Useful for MC comparisons only
#noPRW
#Or redo the prw with updated lumi calc files
#reDoPRW

#Disable the cros-section to get the efficiencies plotted
#disableXS
#Lumi 0.001
# Flag for giving a custom directory with xSection files (if MetaData information is outdated)
xSecDir XAMPPplotting/xSecFiles/

#Disable the reweighting of SumW to  each SUSYproc Id -> unneccessary with the coming derivations
#NoSUSYpidWeight
# Luminosity in fb
#Lumi 1

# Create CutFlow histogram
doCutFlow


# Rename the systematics
RenameSyst JET_PunchThrough_MC15__1down JET_PunchThrough__1down
RenameSyst JET_PunchThrough_AFII__1down JET_PunchThrough__1down

RenameSyst JET_PunchThrough_MC15__1up JET_PunchThrough__1up
RenameSyst JET_PunchThrough_AFII__1up JET_PunchThrough__1up


#########################################################################################################
#                                   Signal particles                                                    #
#########################################################################################################

#New_Particle <Name>  <Name of the InputBranches>
New_Particle SignalMuons muons 
   Cut char signal = 1    
   Cut char isol = 1   
   Var float charge
   Var float z0sinTheta
   Var float d0sig
   Var floatGeV MT
   ifdef isMC
       Var int truthOrigin
       Var int truthType
   endif 
End_Particle 

New_Particle SignalElectrons electrons
   Cut char signal = 1    
   Cut char isol = 1
   Var float charge
   Var float z0sinTheta
   Var float d0sig
   Var floatGeV MT
     
   ifdef isMC
        Var int truthOrigin
        Var int truthType
    endif
End_Particle 

New_Particle SignalTaus taus  
#   Cut char passOR = 1
   #Cut char signalID = 1
   Cut int Quality >= 2 #medium WP
   Var floatGeV MT
   Var floatGeV pt
   Var float charge
   Var int NTrks
   Var int NTrksJet
   Var float Width
   Var float BDTJetScore
End_Particle 

New_Particle AllTaus taus  
#   Cut char passOR = 1
   #Cut char signalID = 1
   #Cut int Quality >= 2 #medium WP
   Var floatGeV MT
   Var floatGeV pt
   Var float charge
   Var int NTrks
   Var int NTrksJet
   Var float Width
   Var float BDTJetScore
End_Particle

New_ParticleM SignalJets jets     
    Cut char signal = 1  
End_Particle 

New_ParticleM SignalHardJets jets
    Cut char signal = 1
    Cut floatGeV pt > 100.0
End_Particle 

New_ParticleM SignalBJets jets     
    Cut char signal = 1
    Cut char bjet = 1  
End_Particle 

DiParticle LightLep SignalElectrons SignalMuons
DiParticle Leptons LightLep SignalTaus

#########################################################################################################
#                                   Loose Objects                                                       #
#########################################################################################################
New_Particle LooseElectrons electrons
    Var float charge
    Var float z0sinTheta
    Var float d0sig
	Var floatGeV MT
    CombCut OR
        Cut char signal = 0
        Cut char isol = 0
   End_CombCut
End_Particle

New_Particle LooseMuons muons
    Var float charge
    Var float z0sinTheta
    Var float d0sig
	Var floatGeV MT
    CombCut OR
        Cut char signal = 0
        Cut char isol = 0
   End_CombCut
End_Particle

New_Particle LooseTaus taus
#   Cut char passOR = 1
#   Cut char signalID = 0
    Cut	char Quality >= 1
    Var floatGeV pt
End_Particle 

DiParticle LooseLightLep LooseElectrons LooseMuons
DiParticle LooseLep LooseLightLep LooseTaus

New_ParticleM LooseJets jets     
    Cut char signal = 0
End_Particle 

#########################################################################################################
#                                   AntiIso Particles                                                   #
#########################################################################################################

#New_Particle <Name>  <Name of the InputBranches>
New_Particle AntiIsoMuons muons 
   Cut char signal = 1 
   Cut char isol = 0 
	Var floatGeV MT
End_Particle 

New_Particle AntiIsoElectrons electrons
   Cut char signal = 1
   Cut char isol = 0
	Var floatGeV MT
End_Particle 

#########################################################################################################
#                                   Baseline leptons                                                    #
#########################################################################################################
DiParticle BaseElectrons SignalElectrons LooseElectrons
DiParticle BaseMuons SignalMuons LooseMuons
DiParticle BaseTaus SignalTaus LooseTaus
########################################################################################################
#                                   DiLepton candidates                                                #
########################################################################################################
New_ParticleM DiLep dilepton
    Var float charge
    Var int pdgId
End_Particle

New_ParticleM ZCand dilepton
    Cut float charge = 0
    Cut floatGeV m >= 61
    Cut floatGeV m <= 101
    CombCut OR
        Cut int pdgId = 121 #ee
        Cut int pdgId = 169 #mumu
    End_CombCut
End_Particle
    
########################################################################################################
#                       Include truth-classified leptons                                               #
########################################################################################################
#Import XAMPPplotting/data/RunConf/FourLepton/LeptonTypes/LeptonTypes.conf

