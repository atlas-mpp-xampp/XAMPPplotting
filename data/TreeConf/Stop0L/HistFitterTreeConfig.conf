
RenameSyst Nominal 
# add prefix for the kinetic systematics
SystPrefix _SYST_

##########
# Weights
##########
RenameWeight EleWeight ElecSF
RenameWeight MuonWeight MuonSF
RenameWeight JetWeightBTag BTagSF
RenameWeight JetWeightJVT JVTSF
RenameWeight xSection FinalXSecWeight
RenameWeight prwWeight PileupWeight

# remove nominal weight name for weight variations
SystReplace ElecSF_SYST_ SYST__
SystReplace MuonSF_SYST_ SYST__
SystReplace BTagSF_SYST_ SYST__
SystReplace JVTSF_SYST_ SYST__
SystReplace PileupWeight_SYST_ SYST__


ifdef isMC
New_Branch
    Name dsid
    Type int
    EvReader int mcChannelNumber
End_Branch
New_Branch
    Name ThreeBodyWeightNum
    Type double
    PseudoReader ThreeBodyWeightNum 1
End_Branch
New_Branch
    Name sherpaNJetsWeight
    Type double
    EvReader double SherpaVjetsNjetsWeight
End_Branch
endif

New_Branch
    Name TreatAsYear
    Type int
    EvReader int treatAsYear
End_Branch




##########
# Lepton multiplicity
##########
New_Branch
    Name NElectrons 
    Type int
    NumParReader SignalElectrons
End_Branch
New_Branch
    Name NMuons  
    Type int
    NumParReader SignalMuons
End_Branch
New_Branch
    Name NSigElectrons 
    Type int
    NumParReader SignalElectrons
End_Branch
New_Branch
    Name NSigMuons  
    Type int
    NumParReader SignalMuons
End_Branch
New_Branch
    Name NSigLep  
    Type int
    NumParReader SignalLeps
End_Branch

##########
# FatJets + TopCandidates
##########

New_Branch
    Name TopDRMinB0M
    Type float
    ParReader TopCand1 m[0]
End_Branch
New_Branch
    Name TopDRMinB1M
    Type float
    ParReader TopCand2 m[0]
End_Branch
New_Branch
    Name Top0Chi2M
    Type float
    ParReader TopCand1Chi2 m[0]
End_Branch
New_Branch
    Name Top1Chi2M
    Type float
    ParReader TopCand2Chi2 m[0]
End_Branch

New_Branch
    Name NAntiKt8  
    Type int
    NumParReader FatJetsR8
End_Branch
New_Branch
    Name AntiKt8Pt
    Type floatVec
    ParReader FatJetsR8 pt
End_Branch
New_Branch
    Name AntiKt8Phi
    Type floatVec
    ParReader FatJetsR8 phi
End_Branch
New_Branch
    Name AntiKt8Eta
    Type floatVec
    ParReader FatJetsR8 eta
End_Branch
New_Branch
    Name AntiKt8M
    Type floatVec
    ParReader FatJetsR8 m
End_Branch
New_Branch
    Name AntiKt8NConst
    Type intVec
    ParReader FatJetsR8 constituents
End_Branch
New_Branch
    Name AntiKt8_DijetMassAsym
    Type float
    EvReader float AsymmetryMTopR08
End_Branch

New_Branch
    Name NAntiKt12  
    Type int
    NumParReader FatJetsR12
End_Branch
New_Branch
    Name AntiKt12Pt
    Type floatVec
    ParReader FatJetsR12 pt
End_Branch
New_Branch
    Name AntiKt12Phi
    Type floatVec
    ParReader FatJetsR12 phi
End_Branch
New_Branch
    Name AntiKt12Eta
    Type floatVec
    ParReader FatJetsR12 eta
End_Branch
New_Branch
    Name AntiKt12M
    Type floatVec
    ParReader FatJetsR12 m
End_Branch
New_Branch
    Name AntiKt12NConst
    Type intVec
    ParReader FatJetsR12 constituents
End_Branch
New_Branch
    Name AntiKt12_DijetMassAsym
    Type float
    EvReader float AsymmetryMTopR12
End_Branch



##########
# Met
##########
New_Branch
    Name Met
    Type float
    EvReader floatGeV MetTST_met
End_Branch
New_Branch
    Name MetTrack
    Type float
    EvReader floatGeV MetTrack_met
End_Branch



##########
# Others
##########
New_Branch
    Name chi2
    Type float
    EvReader floatGeV mt2_chi2
End_Branch
New_Branch
    Name MT2Chi2
    Type float
    EvReader floatGeV mt2
End_Branch

New_Branch
    Name DPhiMetTrackMet
    Type float
    |EvReader| float DPhiMetTrackMet
End_Branch
New_Branch
    Name JetDPhiMetMin2  
    Type float
    |EvReader| min DeltaPhiMin_2
End_Branch
New_Branch
    Name JetDPhiMetMin3  
    Type float
    |EvReader| min DeltaPhiMin_3
End_Branch

New_Branch
    Name HasTauCand
    Type bool
    EvReader char HasTauCand
End_Branch

New_Branch
    Name MtTauCand 
    Type float
    EvReader floatGeV MtTauCand
End_Branch

New_Branch
    Name MinMt
    Type float
    EvReader floatGeV MtJetMin
End_Branch
New_Branch
    Name HtSig
    Type float
    EvReader float HtSig
End_Branch
New_Branch
    Name MEff
    Type float
    EvReader floatGeV Meff
End_Branch
New_Branch
    Name MtNonBMin
    Type float
    EvReader floatGeV MtNonBMin
End_Branch
New_Branch
    Name MtNonBMax
    Type float
    EvReader floatGeV MtNonBMax
End_Branch
New_Branch
    Name MtBMin
    Type float
    EvReader floatGeV MtBMin
End_Branch
New_Branch
    Name MtBMax
    Type float
    EvReader floatGeV MtBMax
End_Branch
New_Branch
    Name DRBB
    Type float
    |dRReader| Bjets_ASC_MV2c10[1] Bjets_ASC_MV2c10[0]
End_Branch
New_Branch
    Name MBB
    Type float
    InvDiMReader Bjets_ASC_MV2c10[1] Bjets_ASC_MV2c10[0]
End_Branch


