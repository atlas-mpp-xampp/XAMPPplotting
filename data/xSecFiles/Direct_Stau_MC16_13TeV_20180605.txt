# grid   : Signal grid with direct pair-production of scalar tau leptons (staus). 13 TeV. MC16.
# author : mann@cern.ch
# date   : 05.06.2018
#
# Cross sections taken from / computed with Resummino (cf. also https://twiki.cern.ch/twiki/bin/view/LHCPhysics/SUSYCrossSections13TeVslepslep)
# BF*K combines branching fraction (1.0 as no specific decay is enforced) and K-factor
# Filter efficiencies FILTEREFF from AMI
# REL.UNCERT (relative uncertainty) given as fraction of xsec
# Last column gives mass parameters
#
# DSID      FS     CROSSSECTION[pb]    BF*K      FILTEREFF    REL.UNCERT
396100     206         0.009731        1.00       0.680650      0.049867  # (200.0, 1.0)
396100     207         0.009731        1.00       0.680650      0.049867  # (200.0, 1.0)
396100     208         0.010432        1.00       0.680650      0.041979  # (200.0, 1.0) # NOTE: includes x2 in xsec for stau1+stau2- and stau1-stau2+
#
396102     206         0.140621        1.00       0.502830      0.035995  # (120.0, 1.0)
396102     207         0.051904        1.00       0.502830      0.042179  # (120.0, 1.0)
396103     206         0.049677        1.00       0.606170      0.040351  # (160.0, 1.0)
396103     207         0.018698        1.00       0.606170      0.045608  # (160.0, 1.0)
396104     206         0.021653        1.00       0.676090      0.044677  # (200.0, 1.0)
396104     207         0.008243        1.00       0.676090      0.049500  # (200.0, 1.0)
396105     206         0.010745        1.00       0.726310      0.049585  # (240.0, 1.0)
396105     207         0.004121        1.00       0.726310      0.053583  # (240.0, 1.0)
396106     206         0.005827        1.00       0.762790      0.053917  # (280.0, 1.0)
396106     207         0.002246        1.00       0.762790      0.056098  # (280.0, 1.0)
396107     206         0.140621        1.00       0.467330      0.035995  # (120.0, 40.0)
396107     207         0.051904        1.00       0.467330      0.042179  # (120.0, 40.0)
396108     206         0.049677        1.00       0.589100      0.040351  # (160.0, 40.0)
396108     207         0.018698        1.00       0.589100      0.045608  # (160.0, 40.0)
396109     206         0.021653        1.00       0.667160      0.044677  # (200.0, 40.0)
396109     207         0.008243        1.00       0.667160      0.049500  # (200.0, 40.0)
396110     206         0.010745        1.00       0.719910      0.049585  # (240.0, 40.0)
396110     207         0.004121        1.00       0.719910      0.053583  # (240.0, 40.0)
396111     206         0.005827        1.00       0.758790      0.053917  # (280.0, 40.0)
396111     207         0.002246        1.00       0.758790      0.056098  # (280.0, 40.0)
396112     206         0.049677        1.00       0.523750      0.040351  # (160.0, 80.0)
396112     207         0.018698        1.00       0.523750      0.045608  # (160.0, 80.0)
396113     206         0.010745        1.00       0.703350      0.049585  # (240.0, 80.0)
396113     207         0.004121        1.00       0.703350      0.053583  # (240.0, 80.0)
396114     206         0.597622        1.00       0.347730      0.035043  # (80.0, 1.0)
396114     207         0.211387        1.00       0.347730      0.040516  # (80.0, 1.0)
