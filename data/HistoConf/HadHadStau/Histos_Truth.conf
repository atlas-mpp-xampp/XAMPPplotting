# axis definitions
Import XAMPPplotting/HistoConf/Stau/Histos_Defs.conf
Import XAMPPplotting/HistoConf/Stau/Histos_Event.conf
#########################################################
#                                                       #
#########################################################


NewVar 
    Type 1D
    Template phi
    Name met_phi
    EvReader floatGeV TruthMET_phi
    xLabel #phi(E_{T}^{miss}) [rad]
EndVar

NewVar 
    Type 1D
    Template metVar_et
    Name met    
    EvReader floatGeV TruthMET_met
    xLabel E_{T}^{miss} [GeV]
EndVar

NewVar 
    Type Cul1D
    Template met_et
    Name Cul_met    
    EvReader floatGeV TruthMET_met
    xLabel Culumative E_{T}^{miss} [GeV]
EndVar

NewVar 
    Type 1D
    Template metVar_sumet
    Name met_sumet
    EvReader floatGeV TruthMET_sumet
    xLabel #Sigma{E_{T}} [GeV]
EndVar

NewVar
    Type 1D
    Template dPhi
    Name met_dPhi_tau
    DPhiToMetReader SignalTaus[0] TruthMET
    xLabel #Delta#phi(E_{T}^{miss}, #tau)
EndVar

NewVar
    Type 1D
    Template dPhi
    Name met_dPhi_ele
    DPhiToMetReader SignalElectrons[0] TruthMET
    xLabel #Delta#phi(E_{T}^{miss}, #tau)
EndVar
NewVar
    Type 1D
    Template dPhi
    Name met_dPhi_muo
    DPhiToMetReader SignalMuons[0] TruthMET
    xLabel #Delta#phi(E_{T}^{miss}, #tau)
EndVar
#########################################################
#               Electrons                               #
#########################################################
NewVar  
    Type 1D
    Template lowVar_pt
    Name el_pt
    ParReader SignalElectrons pt
    xLabel p_{T}(e) [GeV]
EndVar
NewVar  
    Type 1D
    Template eta
    Name el_eta
    ParReader SignalElectrons eta
    xLabel #eta(e)
EndVar
NewVar  
    Type 1D
    Template phi
    Name el_phi
    ParReader SignalElectrons phi
    xLabel #phi(e)
EndVar

#########################################################
#               Muons                                   #
#########################################################
NewVar  
    Type 1D
    Template lowVar_pt
    Name mu_pt
    ParReader SignalMuons pt
    xLabel p_{T}(#mu) [GeV]
EndVar
NewVar  
    Type 1D
    Template eta
    Name mu_eta
    ParReader SignalMuons eta
    xLabel #eta(#mu)
EndVar
NewVar  
    Type 1D
    Template phi
    Name mu_phi
    ParReader SignalMuons phi
    xLabel #phi(#mu)
EndVar
############################################################
#                 Taus                                     #
############################################################
NewVar  
    Type 1D
    Template tauVar_pt
    Name tau_pt
    ParReader SignalTaus pt
    xLabel p_{T}(#tau) [GeV]
EndVar
NewVar  
    Type 1D
    Template eta
    Name tau_eta
    ParReader SignalTaus eta
    xLabel #eta(#tau)
EndVar
NewVar  
    Type 1D
    Template phi
    Name tau_phi
    ParReader SignalTaus phi
    xLabel #phi(#tau) [rad]
EndVar
################################################################
#          Staus                                               #
################################################################
NewVar  
    Type 1D
    Template Var_pTeff
    Name stau_pt
    ParReader TruthStaus pt
    xLabel p_{T}(#tilde{#tau}) [GeV]
EndVar

NewVar  
    Type 1D
    Template broad_eta
    Name stau_eta
    ParReader TruthStaus eta
    xLabel #eta(#tilde{#tau})
EndVar
NewVar  
    Type 1D
    Template phi
    Name stau_phi
    ParReader TruthStaus phi
    xLabel #phi(#tilde{#tau}) [rad]
EndVar

NewVar  
    Type 1D
    Template cos_theta
    Name stau_theta
    ParReader TruthStaus CosThetaStar
    xLabel cos(#theta^{*}) , #theta^{*} = #angle(#tilde{#tau} #dot #tilde{#chi}^{0}_{1})
EndVar

#~ Var_pTeff
