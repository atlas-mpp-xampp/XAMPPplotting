#include <TStopwatch.h>
#include <XAMPPplotting/HistFitterMetaReader.h>
#include <XAMPPplotting/HistFitterWeight.h>
#include <XAMPPplotting/ReaderProvider.h>

namespace XAMPP {

    //##############################################################
    //                        HistFitterNormDataBase
    //##############################################################
    HistFitterNormDataBase::HistFitterNormDataBase() : NormalizationDataBase(), m_isData(false), m_NominalName("Nominal"), m_file() {
        ReaderProvider::GetInstance(true);
    }
    void HistFitterNormDataBase::SetNominalName(const std::string& Nominal) { m_NominalName = Nominal; }
    bool HistFitterNormDataBase::init(const std::vector<std::shared_ptr<TFile>>&) {
        Error("HistFitterNormDataBase::init()", "The method with pointer is not implemented yet");
        return false;
    }
    bool HistFitterNormDataBase::init(const std::vector<std::string>& In) {
        if (isInitialized()) {
            Info("HistFitterNormDataBase::init()", "The database is already initialized");
            return true;
        }
        initialized();
        if (m_NominalName.empty()) {
            Warning("HistFitterNormDataBase::init()", "Set the thing to data");
            m_isData = true;
            HistFitterWeight::getWeighter(isData());
            return true;
        }
        unsigned int N = 0;
        for (const auto& Path : In) {
            TTree* tree = GetNominalTree(Path);
            if (!tree) { return false; }
            if (N == 0) {
                m_isData = isDataTree(tree);
            } else {
                if (m_isData != isDataTree(tree)) {
                    Error("HistFitterNormDataBase::init()",
                          "All loaded trees contain " + std::string(m_isData ? "data" : "MC ") + " events except for this file.");
                    Error("HistFitterNormDataBase::init()", Path);
                    return false;
                }
            }
            ++N;
        }
        HistFitterWeight::getWeighter(isData());
        m_file = std::shared_ptr<TFile>();
        return true;
    }

    NormalizationDataBase* HistFitterNormDataBase::getDataBase() {
        if (!m_Inst) m_Inst = new HistFitterNormDataBase();
        return m_Inst;
    }
    bool HistFitterNormDataBase::isData() const { return m_isData; }
    TTree* HistFitterNormDataBase::GetNominalTree(const std::string& Path) {
        m_file = Open(Path);
        TIter next(m_file->GetListOfKeys());
        while (TObject* obj = next()) {
            std::string ObjName = obj->GetName();
            if (ObjName.find(m_NominalName) == std::string::npos) { continue; }
            TTree* T = nullptr;
            m_file->GetObject(obj->GetName(), T);
            if (T == nullptr) continue;
            return T;
        }
        Error("HistFitterNormDataBase::init()", "Could not find 'Nominal' tree in " + Path);
        return nullptr;
    }
    bool HistFitterNormDataBase::isDataTree(TTree* tree) {
        if (tree->GetBranch("NormWeight")) { return false; }
        return true;
    }

    //##############################################################
    //                      HybridNormDataBase
    //##############################################################
    HybridNormDataBase::HybridNormDataBase() : NormalizationDataBase() {
        Info("HybridNormDataBase()", "Use this database instead of the ordinary one");
    }
    bool HybridNormDataBase::isData() const { return false; }
    bool HybridNormDataBase::init(const std::vector<std::shared_ptr<TFile>>&) {
        Error("HybridNormDataBase::init()", "The method with pointer is not implemented yet");
        return false;
    }
    bool HybridNormDataBase::init(const std::vector<std::string>& In) {
        if (isInitialized()) {
            Info("HybridNormDataBase::init()", "The database is already initialized");
            return true;
        }
        initialized();
        std::vector<std::string> FileList;
        CopyVector(In, FileList);
        unsigned int NFiles = FileList.size(), readN = 0, Prompt = NFiles / 10;
        TStopwatch tsw;
        tsw.Start();
        double t2 = 0;
        for (const auto File : FileList) {
            std::shared_ptr<TFile> F = Open(File);
            if (!F) { return false; }
            TTree* MetaDataTree = nullptr;
            F->GetObject("MetaDataTree", MetaDataTree);
            if (!ReadTree(MetaDataTree)) {
                Error("NormalizationDataBase::init()", "Could not read the metadata from " + File);
                return false;
            }
            delete MetaDataTree;
            ++readN;
            if (NFiles > 10 && readN % Prompt == 0) {
                t2 = tsw.RealTime();
                Info("NormaliztaionDataBase::init()", "Successully read in file " + std::to_string(readN) + "/" + std::to_string(NFiles) +
                                                          ". Needed time " + TimeHMS(t2) + ". Will be finished in " +
                                                          TimeHMS(t2 * ((float)NFiles / (float)readN - 1)) + ".");
                tsw.Continue();
            }
        }
        HybridWeight::getWeighter(isData());
        return true;
    }
    NormalizationDataBase* HybridNormDataBase::getDataBase() {
        if (m_Inst == nullptr) m_Inst = new HybridNormDataBase();
        return m_Inst;
    }
}  // namespace XAMPP
