Welcome to XAMPPplotting!
===

XAMPPplotting package can be used to write histogramms or small trees from standard [XAMPPbase](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPbase) output.

The XAMPPplotting code is provided as a  AthAnalysis package.

You can get more information at the [XAMPPplotting-Twiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XAMPPSoftwareFramework#XAMPPplotting).

Setup the package
---

We recommend working on your own branch an using Merge Requests through the gitlab web interface to push your changes to the master branch. 

You can create your branch by clicking on 'Branches' and choosing 'New branch'. 

If you prefer working in your own fork, feel free to create one from this repository.


* Using cmake and AthAnalysis in R21:

```sh
mkdir MyProject
cd MyProject
mkdir source build run
setupATLASUI
cd source && asetup AthAnalysis,21.2.45,here (or later)
git clone ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPplotting.git -b <your branch>
cd ../build && cmake ../source && make
cd ../ && source build/x86_64-slc6-gcc62-opt/setup.sh
```




Running the code
---

Now, you can run the code in order to produce either a histogram or a (slimmed) tree file. 

For producing (correctly weighted) histograms, use:
```C++
WriteDefaultHistos -i <input config, listing one or many XAMPPbase output files> -r <run config file, see below> -h <histogram config file, see below>
```

For creating trees, use:
```sh
WriteDefaultTrees -i <input config file, listing one or many XAMPPbase output files> -t <tree config file, see below>
```

The output file (which is called `output_histograms.root`/`output_trees.root` by default) will contain histograms/trees, respectively. 

The WriteDefaultHistos / WriteDefaultTrees executables have some useful options such as -o <...> for specifying an output file name, -n <...> for specifying a maximum number of events or -skip <...> for skipping a certain number of events. 

Configuration Files
---

All parts of the offline analysis are configured using configuration files (*.conf) usually located in the [XAMPPplotting/data/](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/tree/master/data) folder. 

There are four different types of config files: [InputConfig](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/tree/master/data/InputConf), [RunConfig](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/tree/master/data/RunConf), [HistoConfig](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/tree/master/data/HistoConf) or [TreeConfig](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/tree/master/data/TreeConf) from the later two you only need one, depending if you want to write histograms or trees, respectively.

InputConfig
---

The input config files tells the package which input ntuples to use, pileup reweighting and ilumicalc files to be used for MC when re-calculating pileup weights.

Known Keywords:

* SampleName: Specify a sample name which can be used for the tree production
* Input: Filepath to be added
* PRWlumi: Specify an ilumicalc file
* PRWconfig: Specify a pileup reweighting file You need to provide both PRWlumi and PRWconfig when running offline pileup reweighting! 

Example of an input configuration file for data:
```sh
# give a sample name which can be used when producing a slimmed tree
SampleName Data2015_PeriodF
# specify the input file to read from
Input <path to file>
```

Example of an input configuration file for MC:

```sh
# give a sample name which can be used when producing a slimmed tree
SampleName Diboson
# give 2 ilumicalc files
PRWlumi XAMPPbase/GRL/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.root
PRWlumi XAMPPbase/GRL/data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.root
# give a pileup reweighting file
PRWconfig dev/SUSYTools/merged_prw_mc15c_July27_afterFix.root
# specify the input file to read from
Input <path to file>
```

RunConfig
---
Run config files tell the package which selection shall be applied for the histogram/tree creation. 

Furthermore, you can specify the weights to be used, if you want to create a CutFlow histogram or you want to re-calculate the Pileup weights.

Known Keywords:

* Tree: Specify the input tree of the Ntuples used for plotting
* Analysis: Specify the analysis name in the output histogram file
* noSyst: Add this when you want to run on Nominal only
* Systematic: Specify this when you only want to run on certain systematic uncertainties (which affect the event kinematics)
* Weights: Specify the event weights to be applied in the histogram creation
* noPRW: Use this if you do not want to apply pileup weights
* NoSUSYpidWeight: Disable the reweighting of SumW for each each SUSYproc Id
* xSecDir: Specify an alternative directory for the SUSYTools cross section files
* Lumi: Only set this to a value different from 1 (\ifb) when you know what you are doing
* New_Particle < name >  < name of the input branch > \n ...\n End_Particle Create a new particle collection by giving it a name and telling the name prefix of the corresponding input branches
* New_ParticleM < name >  < name of the input branch > \n ...\n End_Particle Same as New_Particle but reading from an EtaPhiPtM TLorenztVector instead of EtaPhiPtE 

Example:
```sh
# Analysis name in input tree
Tree Stop0L
# Analysis name in output histogram file
Analysis Stop0L
# Name of region in output histogram file
Region SR
# Create CutFlow histogram
doCutFlow
# define a particle collection called 'SignalJets'
New_Particle SignalJets Jet 
End_Particle 
# do PRW in XAMPPplotting
reDoPRW 
# require to have at least 2 'SignalJets'
NumParCut SignalJets >= 2
# require the jets to have a pt > 80 GeV
ParCut SignalJets pt[0] >= 80
ParCut SignalJets pt[1] >= 80
```

HistoConfig
---

Histo config files tell the code which histograms to produce.

Example:

Create a 1D binning used for a plot of pt with 10 bins from 0 to 500 by typing:
```sh
1DHisto medium_pt 10 0 500
```

This way, a 1D template with the name `medium_pt` is created. 

You can now define a histogram using:

```sh
NewVar  
    Type 1D
    Template medium_pt
    Name jet1_pt
    ParReader SignalJets pt[0]
    xLabel Leading jet p_{T} [GeV]
EndVar
```
where you specify the template used, a name (e.g. `jet1_pt`) of the histogram as well as the variable reader to be plotted and the axis title(s). 
Here it is assumed, that you already have defined the ParReader `SignalJets` inside your RunConfig.

TreeConfig
---
Tree config files define which branches should be written to the output and with which content they should be filled. 

For examples, please have a look here : [some TreeConfigs as illustration](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/tree/master/data/TreeConf).

Creating a Cutflow
---
Make sure you set `doCutFlow` in your RunConfig beforhand.
```sh
python PrintCutFlow.py -i <input file, created by WriteDefaultHistos> -r <defined region> -a <your analysis>
```

Creating Final MC or Data/MC Plots
---

 As mentioned above, the output file (which is called `output_histograms.root` by default) produced by WriteDefaultHistos will contain histograms.
 
 These can be used to create plots / compute efficiencies / optimize yields / etc. 
 
 The python scripts located at [XAMPPplotting/python](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/tree/master/python) provide some pyROOT macros. 
 
 The general recommended workflow is as follows:

* The paths of the histogram files created by WriteDefaultHistos have to be specified using so called DSConfig files. 

You can have a look at [XAMPPplotting/python/](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/tree/master/python/DSConfigs) for some examples.

Inside this file, you specify the path to all histogram files you want to use as well as their plotting labels, colors, type of sample (signal, background, data) and integrated luminosity (for data).

* Use the XAMPPplotting/python/DataMCPlots.py or XAMPPplotting/python/MCPlots.py macro for plotting Data-MC comparions or MC-only plots, respectively. 

It has an argument parser, where you can specify the type of plot you want to create as well as the DSconfig.

Both the [MCPlots.py](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/blob/master/python/MCPlots.py) and the [DataMCPlots.py](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/blob/master/python/DataMCPlots.py) script will create a folder Plots containing the final plots in .pdf format. 

You can change the output directory by using the -o option, for choosing another filetype than .pdf, you can use the -f option. 

Example:

Plotting a MC-only distribution for a histogram called jet1_pt coming from an analysis called Stop0L in a region called SR:
```sh
python python/MCPlots.py -a Stop0L -r SR -v jet1_pt -c <your DSConfig file>
```
Plotting a Data-MC comparion for a histogram called mll coming from an analysis called Stop0L in a region called 2LCR:
```sh
python python/DataMCPlots.py -a Stop0L -r 2LCR -v mll -c <your DSConfig file>
```
For more help on the options available, type:
```sh
python python/DataMCPlots.py -h
```

Advisory regarding friendship failures
---
If your analysis uses the concept of `SystematicGroups` where common variables are written into separete trees it might happen that your jobs crash giving the following error message:
```
EventService::setupTree()           -- ERROR: The friend tree CommonTree_Staus has less events than Staus_Nominal (495577 vs. 591608). The file is corrupted!!!
```
These failures usually occur if many samples are merged into one file. After some investigations we found two possible reasons for this failure:
* You are merging AFII and FullSim samples together. The problem in here is that some recommendations like the EGamma or Jet uncertainties add extra systematics on AFII. The hadd command cannot deal with two different sets of branches and discards the fraction of events not matching the existing structure.
* You store the LHE weight variations inside your sample. Even across different slices of a given process generated by the same generator the set of LHE weights does not necessarily need to match amongst each other. XAMPP creates for each LHE weight an own branch in the CommonTree. Like in the case above the hadd command cannot deal with it.
* One of your input files is just corrupted. This case has already caused troubles in the past.

One should only merge files with the same content of SystematicTrees and the same number of LHEweights together. The [XAMPPplottingSample](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/blob/master/scripts/LepHadStau/CreateDataConfigs.py#L12-156) python class implements exactly this strategy.

 
Doxygen documentation
---

A doxygen documentation of the package can be found at:

https://xamppplotting-doxygen.web.cern.ch/ 

